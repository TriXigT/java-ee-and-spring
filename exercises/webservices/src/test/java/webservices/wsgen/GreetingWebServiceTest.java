package webservices.wsgen;

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Test;

/**
 * This test tests the wsgen tool. It builds the javaclasses the JAX-WS need for building a webservice. The generated
 * java classes contains JABX-annotations that is used to generate schemas and WSDL files.
 * 
 * The modern versions of JAX-WS don't require us to use wsgen, this is done automatically. But we run it here simply to
 * gain some understanding for how the process works.
 * 
 */
public class GreetingWebServiceTest {

	/**
	 * Generate the files needed to create the GreetingWebService. In the root directory of this project, run the
	 * following statement on the command line:
	 * 
	 * wsgen -keep -cp ./target/classes/ -verbose -d target/classes -s src/main/java 
	 * webservices.wsgen.GreetingWebServiceImpl -wsdl
	 */
	@Test
	public void test() {

		// Specify the path to the root directory of this project
		String pathToRootDir = "/home/coolio/Projects/school_projs/gbju13/enterprise_integration/code/enterprise-java/exercises/webservices";

		// Specify the filename of the xsd file that is generated in the target directory
		String xsdFileName = "GreetingWebServiceImplService_schema1.xsd";

		// Specify the filename of the wsdl file that is generated in the target directory
		String wsdlFileName = "GreetingWebServiceImplService.wsdl";

		assertTrue("Xsd", new File(pathToRootDir + "/target/classes/" + xsdFileName).exists());
		assertTrue("WSDL", new File(pathToRootDir + "/target/classes/" + wsdlFileName).exists());
		assertTrue("GreetSomeone.java", new File(pathToRootDir
				+ "/src/main/java/webservices/wsgen/jaxws/GreetSomeone.java").exists());
		assertTrue("GreetSomeoneResponse.java", new File(pathToRootDir
				+ "/src/main/java/webservices/wsgen/jaxws/GreetSomeoneResponse.java").exists());

	}

}
