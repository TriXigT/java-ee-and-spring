package se.plushogskolan.jee.utils.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;

public class DifferentArrivalAndDepartureAirportValidator implements
		ConstraintValidator<DifferentArrivalAndDepartureAirport, ArrivalAndDepartureAirportHolder> {

	@Override
	public void initialize(DifferentArrivalAndDepartureAirport a) {

	}

	@Override
	public boolean isValid(ArrivalAndDepartureAirportHolder airportHolder, ConstraintValidatorContext context) {

		boolean arrivalEmpty = StringUtils.isEmpty(airportHolder.getArrivalAirportCode());
		boolean departureEmpty = StringUtils.isEmpty(airportHolder.getArrivalAirportCode());

		if (arrivalEmpty || departureEmpty) {
			// Both values must exist
			return false;
		}

		if (airportHolder.getArrivalAirportCode().toLowerCase()
				.equals(airportHolder.getDepartureAirportCode().toLowerCase())) {
			return false;
		}

		return true;

	}

}
