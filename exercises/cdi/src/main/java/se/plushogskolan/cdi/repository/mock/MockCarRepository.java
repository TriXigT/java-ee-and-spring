package se.plushogskolan.cdi.repository.mock;

import se.plushogskolan.cdi.annotations.Environment;
import se.plushogskolan.cdi.annotations.InvocationCounter;
import se.plushogskolan.cdi.annotations.Environment.EnvironmentType;
import se.plushogskolan.cdi.model.Car;
import se.plushogskolan.cdi.repository.CarRepository;

/**
 * A mocked repository
 * 
 */
@Environment(EnvironmentType.MOCK)
public class MockCarRepository implements CarRepository {

	public MockCarRepository() {
	}
	
	@InvocationCounter
	public Car getNormalCar() {
		return new Car("Mocked normal car");
	}

	@InvocationCounter
	public Car getSportsCar() {
		return new Car("Mocked sports car");
	}

	public void saveCar(Car car) {
		System.out.println("Save car in mock: " + car.getName());
	}

}
