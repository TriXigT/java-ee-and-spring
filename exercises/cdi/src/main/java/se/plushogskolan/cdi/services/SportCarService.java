package se.plushogskolan.cdi.services;

import javax.inject.Inject;

import se.plushogskolan.cdi.annotations.Environment;
import se.plushogskolan.cdi.annotations.InvocationCounter;
import se.plushogskolan.cdi.annotations.Environment.EnvironmentType;
import se.plushogskolan.cdi.annotations.Income;
import se.plushogskolan.cdi.annotations.Income.IncomeType;
import se.plushogskolan.cdi.model.Car;
import se.plushogskolan.cdi.model.Owner;
import se.plushogskolan.cdi.repository.CarRepository;

@Income(IncomeType.HIGH)
public class SportCarService implements CarService {

	/**
	 * Injecting a high income owner. The Owner is created using the @Producer
	 * method in Owner.java.
	 */
	@Inject @Income(IncomeType.HIGH)
	Owner owner;

	@Inject @Environment(EnvironmentType.PROD)
	private CarRepository carRepository;

	@InvocationCounter
	public Car getCar() {
		Car car = carRepository.getSportsCar();
		car.setOwner(owner);
		return car;
	}

	public void saveCar(Car car) {
		carRepository.saveCar(car);

	}

	@Override
	public String toString() {
		return "SportCarService, owner=" + owner;
	}

}
