package se.plushogskolan.jetbroker.plane.repository;

import java.util.List;

import org.joda.time.DateTime;

import se.plushogskolan.jee.utils.repository.BaseRepository;
import se.plushogskolan.jetbroker.plane.domain.Airport;

public interface AirportRepository extends BaseRepository<Airport> {

	List<Airport> getAllAirports();

	Airport getAirportByCode(String code);

	int deleteAirportsOlderThan(DateTime date);

}
