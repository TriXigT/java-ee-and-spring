package se.plushogskolan.jee.utils.validation;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = ArrivalAndDepartureAirportValidator.class)
@Documented
public @interface ArrivalAndDepartureAirport{

	String message() default "{validation.airport.equals}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
