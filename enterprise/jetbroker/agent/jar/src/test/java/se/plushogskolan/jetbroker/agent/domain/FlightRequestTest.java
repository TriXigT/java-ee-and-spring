package se.plushogskolan.jetbroker.agent.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import javax.validation.ConstraintViolation;

import org.junit.Test;

import se.plushogskolan.jetbroker.TestFixture;

public class FlightRequestTest {

	@Test
	public void testStatusIsCreated() {
		FlightRequest req = new FlightRequest();
		
		assertEquals("", req.getStatus(), Status.CREATED);
	}
	
	@Test
	public void testValidationCustomerNull() {
		FlightRequest request = TestFixture.getValidFlightRequest();
		request.setCustomer(null);
		
		Set<ConstraintViolation<FlightRequest>> violations = TestFixture.getValidator().validate(request);
		TestFixture.assertPropertyIsInvalid("customer", violations);
	}
	
	@Test
	public void testValidationPassengersBelow1() {
		FlightRequest request = TestFixture.getValidFlightRequest();
		request.setPassengers(-1);
		
		Set<ConstraintViolation<FlightRequest>> violations = TestFixture.getValidator().validate(request);
		TestFixture.assertPropertyIsInvalid("passengers", violations);
	}
	
	@Test
	public void testValidationPassengersOver500() {
		FlightRequest request = TestFixture.getValidFlightRequest();
		request.setPassengers(501);
		
		Set<ConstraintViolation<FlightRequest>> violations = TestFixture.getValidator().validate(request);
		TestFixture.assertPropertyIsInvalid("passengers", violations);
	}
	
	@Test
	public void testValidationPassengersOk() {
		FlightRequest request = TestFixture.getValidFlightRequest();
		request.setPassengers(499);
		
		Set<ConstraintViolation<FlightRequest>> violations = TestFixture.getValidator().validate(request);
		assertTrue("passengers ok", violations.isEmpty());
	}
	
	@Test
	public void testValidationAirportCodesActive() {
		FlightRequest request = TestFixture.getValidFlightRequest();
		
		request.setArrivalAirportCode("GBG");
		request.setDepartureAirportCode("GBG");
		
		Set<ConstraintViolation<FlightRequest>> violations = TestFixture.getValidator().validate(request);
		assertFalse("airport codes equal", violations.isEmpty());
	}

}
