package se.plushogskolan.jetbroker.agent.repository.jpa;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.Status;
import se.plushogskolan.jetbroker.agent.repository.AbstractRepositoryTest;
import se.plushogskolan.jetbroker.agent.service.CustomerService;

@RunWith(Arquillian.class)
@Transactional(value = TransactionMode.ROLLBACK)
public class JpaFlightRequestRepositoryIntegrationTest extends AbstractRepositoryTest<FlightRequest, JpaFlightRequestRepository>{

	private static Logger log = Logger.getLogger(JpaFlightRequestRepositoryIntegrationTest.class);
	
	@Inject
	JpaFlightRequestRepository repo;
	@Inject
	CustomerService customerService;

	@Test
	public void testDeleteOldFlightRequests() {
		long id1 = repo.persist(getEntity1());
		
		FlightRequest flightRequest2 = getEntity2();
		repo.persist(flightRequest2);
		
		assertEquals("Customers in DB", 2, repo.getAllFlightRequests().size());
		
		repo.remove(flightRequest2);
		
		assertEquals("Number of FlightRequests after delete", 1, repo.getAllFlightRequests().size());
		assertEquals("Correct FlightRequest is in DB", id1, repo.getAllFlightRequests().iterator().next().getId());
	}
	
	@Test
	public void testGetAllFlightRequests() {
		repo.persist(getEntity1());
		repo.persist(getEntity2());
		
		assertTrue("No. of FlightRequestss", repo.getAllFlightRequests().size() == 2);
	}
	
	@Test
	public void testGetByStatus() {
		repo.persist(getEntity1());
		repo.persist(getEntity2());
		
		assertEquals("get FlightRequests by status", 2, repo.getFlightRequestsByStatus(Status.CREATED).size());
	}
	
	@Test
	public void testPersist() {
		FlightRequest req = getEntity2();
		req.setStatus(Status.REJECTED);
		DateTime date = DateTime.now();
		req.setDate(date);
		repo.persist(req);
		
		assertEquals("status", Status.REJECTED, req.getStatus());
		assertEquals("customer email", "nean@amsen.se", req.getCustomer().getEmail());
		assertEquals("date and time", date, req.getDate());
	}
	
	@Test(expected = Exception.class)
	public void testValidationInvalidState() {
		FlightRequest req = getEntity1();
		req.setPassengers(9999);
		
		repo.persist(req);
	}

	@Override
	protected JpaFlightRequestRepository getRepository() {
		return repo;
	}

	@Override
	protected FlightRequest getEntity1() {
		Customer customer = customerService.createCustomer(TestFixture.getValidCustomer());
		return TestFixture.getValidFlightRequest(0, "GBG", "STM", DateTime.now(), customer, 32);
	}

	@Override
	protected FlightRequest getEntity2() {
		Customer customer = customerService.createCustomer(TestFixture.getValidCustomer(0,"Linnea","Amsen","nean@amsen.se"));
		return TestFixture.getValidFlightRequest(0, "STM", "KLM", DateTime.now(), customer, 44);
	}


}
