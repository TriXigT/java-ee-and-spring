package se.plushogskolan.jetbroker.order.domain;


public class PlaneType {

	private String code;
	private String name;
	private int maxNoOfPassengers;
	private int maxRangeKm;
	private int maxSpeedKmH;
	private double fuelConsumptionPerKm;

	

	public PlaneType(String code, String name, int maxNoOfPassengers,
			int maxRangeKm, int maxSpeedKmH, double fuelConsumptionPerKm) {
		this.code = code;
		this.name = name;
		this.maxNoOfPassengers = maxNoOfPassengers;
		this.maxRangeKm = maxRangeKm;
		this.maxSpeedKmH = maxSpeedKmH;
		this.fuelConsumptionPerKm = fuelConsumptionPerKm;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getNiceName() {
		return String.format("%s (%s)", name, code);
	}

	public int getMaxNoOfPassengers() {
		return maxNoOfPassengers;
	}

	public void setMaxNoOfPassengers(int maxNoOfPassengers) {
		this.maxNoOfPassengers = maxNoOfPassengers;
	}

	public int getMaxRangeKm() {
		return maxRangeKm;
	}

	public void setMaxRangeKm(int maxRangeKm) {
		this.maxRangeKm = maxRangeKm;
	}

	public int getMaxSpeedKmH() {
		return maxSpeedKmH;
	}

	public void setMaxSpeedKmH(int maxSpeedKmH) {
		this.maxSpeedKmH = maxSpeedKmH;
	}

	public double getFuelConsumptionPerKm() {
		return fuelConsumptionPerKm;
	}

	public void setFuelConsumptionPerKm(double fuelConsumptionPerKm) {
		this.fuelConsumptionPerKm = fuelConsumptionPerKm;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlaneType other = (PlaneType) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		return true;
	}

	
}
