package se.plushogskolan.jetbroker.agent.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.agent.domain.Airport;
import se.plushogskolan.jetbroker.agent.repository.AirportRepository;
import se.plushogskolan.jetbroker.agent.service.AirportService;

@Stateless
public class AirportServiceImpl implements AirportService{

	@Inject
	AirportRepository repo;

	@Override
	public Airport getAirportByCode(String code) {
		return repo.getAirportByCode(code);
	}

	@Override
	public List<Airport> getAllAirports() {
		return repo.getAllAirports();
	}
}
