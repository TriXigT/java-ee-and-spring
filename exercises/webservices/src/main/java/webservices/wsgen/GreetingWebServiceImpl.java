package webservices.wsgen;

import javax.jws.WebService;
import javax.xml.ws.Endpoint;

@WebService(endpointInterface = "webservices.wsgen.GreetingWebService")
public class GreetingWebServiceImpl implements GreetingWebService {

	public static void main(String[] args) {
		Endpoint.publish("http://localhost:8180/ws/greetingWebService", new GreetingWebServiceImpl());
	}

	public String greetSomeone(String name) {
		return "Hello " + name;
	}

}
