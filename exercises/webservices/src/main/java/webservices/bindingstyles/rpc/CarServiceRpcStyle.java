package webservices.bindingstyles.rpc;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import webservices.bindingstyles.Car;
import webservices.bindingstyles.CarService;
import webservices.bindingstyles.RegNumber;

/**
 * Add annotations to make this an RPC style web service
 */
@WebService
@SOAPBinding(style = Style.RPC)
public interface CarServiceRpcStyle extends CarService {
	
	@WebMethod
	@Override
	public Car getCar(RegNumber regNo);
}
