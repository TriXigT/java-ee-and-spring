package se.plushogskolan.jetbroker.agent.mvc;

import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.format.datetime.joda.DateTimeFormatterFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.agent.domain.Airport;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.PlaneType;
import se.plushogskolan.jetbroker.agent.mvc.bean.EditFlightRequestBean;
import se.plushogskolan.jetbroker.agent.service.AirportService;
import se.plushogskolan.jetbroker.agent.service.CustomerService;
import se.plushogskolan.jetbroker.agent.service.FlightRequestService;
import se.plushogskolan.jetbroker.agent.service.PlaneTypeService;

@Controller
@RequestMapping("editFlightRequest/{id}.html")
public class EditFlightRequestController {
	private static Logger log = Logger.getLogger(EditFlightRequestController.class.getName());
	
	@Inject
	private FlightRequestService flightRequestService;
	@Inject
	private PlaneTypeService planeTypeService;
	@Inject
	private AirportService airportService;
	@Inject
	private CustomerService customerService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView get(@PathVariable long id) {
		log.fine("GET FlightRequest");
		
		EditFlightRequestBean editFlightRequestBean = new EditFlightRequestBean();

		if(id>0) {
			log.fine(String.format("get FlightRequest with id %d",id));
			FlightRequest flightRequest = flightRequestService.getFlightRequest(id);
			
			copyFlightRequestValuesIntoBean(editFlightRequestBean, flightRequest);
		}
		
		return getModelAndView(editFlightRequestBean);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView handleSubmit(@Valid EditFlightRequestBean editFlightRequestBean, BindingResult errors) {
		log.fine("handle submit");
		if(errors.hasErrors()) {
			log.fine("submit had errors");
			return getModelAndView(editFlightRequestBean);
		}
		
		FlightRequest flightRequest;
		
		if(editFlightRequestBean.getId() > 0) {
			log.fine("update flight request");
			flightRequest = flightRequestService.getFlightRequest(editFlightRequestBean.getId());
			copyValuesIntoFlightRequest(flightRequest, editFlightRequestBean);
			flightRequestService.updateFlightRequest(flightRequest);
		} else {
			log.fine("persist new flight request");
			flightRequest = new FlightRequest();
			copyValuesIntoFlightRequest(flightRequest, editFlightRequestBean);
			flightRequestService.createFlightRequest(flightRequest);
		}

		return new ModelAndView("redirect:/index.html");
	}
	
	public ModelAndView getModelAndView(EditFlightRequestBean editFlightRequestBean) {
		ModelAndView mav = new ModelAndView("editFlightRequest");
		
		List<Airport> airports = airportService.getAllAirports();
		List<Customer> customers = customerService.getAllCustomers();
		List<PlaneType> planeTypes = planeTypeService.getAllPlaneTypes();
		
		mav.addObject("editFlightRequestBean", editFlightRequestBean);
		mav.addObject("airports", airports);
		mav.addObject("customers", customers);
		mav.addObject("planeTypes", planeTypes);
		
		return mav;
	}
	
	public void copyFlightRequestValuesIntoBean(EditFlightRequestBean bean, FlightRequest flightRequest) {
		bean.setId(flightRequest.getId());
		bean.setDate(flightRequest.getDate().toString(new DateTimeFormatterFactory("dd-MM-yyyy").createDateTimeFormatter()));
		bean.setNumberOfPassengers(flightRequest.getPassengers());

		bean.setDepartureAirportCode(flightRequest.getDepartureAirportCode());
		bean.setArrivalAirportCode(flightRequest.getDepartureAirportCode());
		bean.setCustomerEmail(flightRequest.getCustomer().getEmail());
		
		bean.setHour(flightRequest.getDate().getHourOfDay());
		bean.setMinute(flightRequest.getDate().getMinuteOfHour());
	}
	
	public void copyValuesIntoFlightRequest(FlightRequest flightRequest, EditFlightRequestBean bean) {
		flightRequest.setId(bean.getId());
		
		DateTimeFormatter formatter = new DateTimeFormatterFactory("dd-MM-yyyy HH:mm").createDateTimeFormatter();
		DateTime date = formatter.parseDateTime(String.format("%s %d:%d", bean.getDate().split(" ")[0], bean.getHour(), bean.getMinute()));
		flightRequest.setDate(date);
		
		flightRequest.setPassengers(bean.getNumberOfPassengers());
		flightRequest.setDepartureAirportCode(bean.getDepartureAirportCode());
		flightRequest.setArrivalAirportCode(bean.getArrivalAirportCode());
		flightRequest.setCustomer(customerService.getCustomerByEmail(bean.getCustomerEmail()));
	}
}