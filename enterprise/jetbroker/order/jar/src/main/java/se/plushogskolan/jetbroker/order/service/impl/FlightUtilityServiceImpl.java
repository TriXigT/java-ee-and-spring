package se.plushogskolan.jetbroker.order.service.impl;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.map.HaversineDistance;
import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.service.FlightUtilityService;
import se.plushogskolan.jetbroker.order.service.FuelPriceService;

@Stateless
public class FlightUtilityServiceImpl implements FlightUtilityService{
	@Inject
	FuelPriceService fuelPriceService;

	@Override
	public double getTimeForFlightInMinutes(Airport a, Airport b, PlaneType plane) {
		return getDistanceForFlight(a, b) / plane.getMaxSpeedKmH() * 60;
	}

	@Override
	public double getFuelPriceForFlight(Airport a, Airport b, PlaneType plane) {
		return getDistanceForFlight(a, b) * plane.getFuelConsumptionPerKm() * fuelPriceService.getFuelPrice();
	}
	
	@Override
	public double getDistanceForFlight(Airport a, Airport b) {
		return HaversineDistance.getDistance(a.getLatitude(), a.getLongitude(), b.getLatitude(), b.getLongitude());
	}

	protected FuelPriceService getFuelPriceService() {
		return fuelPriceService;
	}

	protected void setFuelPriceService(FuelPriceService fuelPriceService) {
		this.fuelPriceService = fuelPriceService;
	}
	
}
