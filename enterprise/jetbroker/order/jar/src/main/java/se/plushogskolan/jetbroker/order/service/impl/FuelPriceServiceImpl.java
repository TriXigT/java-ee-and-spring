package se.plushogskolan.jetbroker.order.service.impl;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;

import se.plushogskolan.jetbroker.order.service.FuelPriceService;

@Singleton
public class FuelPriceServiceImpl implements FuelPriceService{

	private double fuelPrice = 1;

	@Lock(LockType.READ)
	public double getFuelPrice() {
		return fuelPrice;
	}
	
	@Lock(LockType.WRITE)
	public void setFuelPrice(double fuelPrice) {
		this.fuelPrice = fuelPrice;
	}

}
