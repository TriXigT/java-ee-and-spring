package se.plushogskolan.jetbroker.order.service.impl;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.service.FuelPriceService;

public class FlightUtilityServiceImplTest {

	private FlightUtilityServiceImpl flightUtilityService;
	private FuelPriceService fuelPriceService;
	
	private Airport airportA;
	private Airport airportB;
	private PlaneType planeType;
	
	private static final double CALCULATED_DISTANCE = 4459;
	private static final int MAX_SPEED = 888;
	private static final double FUEL_CONSUMPTION = 5.5;
	
	@Before
	public void setup() {
		flightUtilityService = new FlightUtilityServiceImpl();
		
		fuelPriceService = createMock(FuelPriceService.class);
		airportA = TestFixture.getValidAirport("","",10,10);
		airportB = TestFixture.getValidAirport("","",40,40);
		planeType = TestFixture.getValidPlaneType("","",MAX_SPEED,FUEL_CONSUMPTION);
		
		flightUtilityService.setFuelPriceService(fuelPriceService);
	}
	
	@Test
	public void getTimeForFlightInMinutesTest() {
		assertEquals("get time for flight", CALCULATED_DISTANCE/MAX_SPEED*60, flightUtilityService.getTimeForFlightInMinutes(airportA, airportB, planeType), 5);
	}
	
	@Test
	public void getFuelPriceForFlightTest() {
		double fuelPrice = 2d;
		expect(fuelPriceService.getFuelPrice()).andReturn(fuelPrice);
		replay(fuelPriceService);
		assertEquals("get fuel price", CALCULATED_DISTANCE*fuelPrice*FUEL_CONSUMPTION, flightUtilityService.getFuelPriceForFlight(airportA, airportB, planeType), 5);
		verify(fuelPriceService);
	}
	
	@Test
	public void getDistanceForFlightTest() {
		assertEquals("get distance for flight", CALCULATED_DISTANCE, flightUtilityService.getDistanceForFlight(airportA, airportB), 5);
	}
	
}
	
