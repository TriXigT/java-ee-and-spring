package se.plushogskolan.jetbroker.plane.rest.customer;

import java.util.logging.Logger;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import se.plushogskolan.jetbroker.agent.rest.OkOrErrorResponse;
import se.plushogskolan.jetbroker.agent.services.CustomerService;
import se.plushogskolan.jetbroker.plane.rest.customer.model.CreateCustomerRequest;
import se.plushogskolan.jetbroker.plane.rest.customer.model.CreateCustomerResponse;
import se.plushogskolan.jetbroker.plane.rest.customer.model.GetCustomerResponse;

@Controller
public class CustomerController {
	Logger log = Logger.getLogger(CustomerController.class.getName());
	
	@Inject
	CustomerService customerService;
	
	@RequestMapping(value="/createCustomer", method=RequestMethod.POST, produces="application/json")
	@ResponseBody
	public CreateCustomerResponse createCustomer(@RequestBody CreateCustomerRequest request) {
		return new CreateCustomerResponse(customerService.createCustomer(request.buildCustomer()).getId());
	}
	
	@RequestMapping(value="/getCustomer/{id}", method=RequestMethod.GET, produces="application/json") 
	@ResponseBody
	public GetCustomerResponse getCustomer(@PathVariable long id) {
		return new GetCustomerResponse(customerService.getCustomer(id));
	}
	
	@RequestMapping(value="/deleteCustomer/{id}", method=RequestMethod.DELETE, produces="application/json") 
	@ResponseBody
	public OkOrErrorResponse deleteCustomer(@PathVariable long id) {
		try {
			customerService.deleteCustomer(id);
			return OkOrErrorResponse.getOkResponse();
		} catch(Exception e) {
			return OkOrErrorResponse.getErrorResponse(e.getMessage());
		}
	}
}
