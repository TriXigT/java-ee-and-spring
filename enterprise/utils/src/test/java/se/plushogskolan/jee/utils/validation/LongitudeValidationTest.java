package se.plushogskolan.jee.utils.validation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class LongitudeValidationTest {
	@Test
	public void isValidTest() {
		LongitudeValidator validator = new LongitudeValidator();
		
		assertTrue("is valid integer", validator.isValid(180, null));
		assertTrue("is valid double", validator.isValid(9.99d, null));
		assertTrue("is valid string", validator.isValid("-66", null));
		
		assertFalse("is not valid integer", validator.isValid(999, null));
		assertFalse("is not valid double", validator.isValid(-180.1d, null));
		assertFalse("is not valid string", validator.isValid("0", null));
	}
}
