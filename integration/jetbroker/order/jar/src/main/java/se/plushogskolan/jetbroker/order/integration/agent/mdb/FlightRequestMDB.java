package se.plushogskolan.jetbroker.order.integration.agent.mdb;

import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.service.FlightRequestService;

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = JmsConstants.DESTINATION_TYPE_QUEUE),
		@ActivationConfigProperty(propertyName="destination", propertyValue=JmsConstants.QUEUE_FLIGHTREQUEST_REQUEST)
		})
public class FlightRequestMDB implements MessageListener{

	@Inject
	private Logger log;
	
	@Inject
	FlightRequestService flightRequestService;
	
	@Override
	public void onMessage(Message message) {
		log.fine("received message");
		
		MapMessage mapMessage = (MapMessage) message;
		FlightRequest request = new FlightRequest();
		
		String dateString;
		long agentId;
		String arrival;
		String departure;
		int passengers;
		
		try {
			dateString = mapMessage.getString("date");
			agentId = mapMessage.getLong("agentId");
			arrival = mapMessage.getString("arrivalAirportCode");
			departure = mapMessage.getString("departureAirportCode");
			passengers = mapMessage.getInt("noOfPassengers");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		DateTime date = DateTime.parse(dateString, 
				DateTimeFormat.forPattern(JmsConstants.DATE_FORMAT));
		
		request.setAgentRequestId(agentId);
		request.setArrivalAirportCode(arrival);
		request.setDepartureAirportCode(departure);
		request.setDepartureTime(date);
		request.setNoOfPassengers(passengers);

		log.fine(String.format("received flightRequest from agent: %s]", request.toString()));
		
		flightRequestService.handleIncomingFlightRequest(request);
	}

}