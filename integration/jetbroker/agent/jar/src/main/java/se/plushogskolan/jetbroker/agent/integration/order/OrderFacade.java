package se.plushogskolan.jetbroker.agent.integration.order;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;

@Local
public interface OrderFacade {

	public void sendFlightRequest(FlightRequest request) throws Exception;

}
