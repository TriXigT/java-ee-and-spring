package se.plushogskolan.jee.utils.repository.mock;

import java.util.List;

import se.plushogskolan.jee.utils.domain.IdHolder;
import se.plushogskolan.jee.utils.repository.BaseRepository;

public abstract class MockAbstractRepository <E extends IdHolder> implements BaseRepository<E>{

	protected MockIdGenerator idGenerator;

	public MockAbstractRepository () {
		idGenerator = new MockIdGenerator();
	}

	/**
	 * A MockRepository must supply a List implementation to be used as the source of mock
	 * objects. 
	 * @return a List of objects inheriting IdHolder
	 */
	protected abstract List<E> getMockList();

	@Override
	public long persist(E entity) {
		getMockList().add(entity);
		return idGenerator.assignNextId(entity);
	}

	@Override
	public void remove(E entity) {
		getMockList().remove(entity);
	}

	@Override
	public void update(E entity) {
		for(E tempEntity : getMockList()) {
			if(tempEntity.equals(entity)) {
				tempEntity = entity;
			}
		}
	}

	@Override
	public E findById(long id) {
		for(E tempEntity : getMockList()) {
			if(tempEntity.getId() == id) {
				return tempEntity;
			}
		}
		return null;
	}

	/**
	 * For testing purposes
	 * @return
	 */
	protected MockIdGenerator getIdGenerator() {
		return idGenerator;
	}
}
