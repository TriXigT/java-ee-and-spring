package se.plushogskolan.jetbroker.agent.service;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.agent.domain.PlaneType;

@Local
public interface PlaneTypeService {
	PlaneType getPlaneType(long id);

	PlaneType createPlaneType(PlaneType planeType);

	List<PlaneType> getAllPlaneTypes();
	
	PlaneType getPlaneTypeByCode(String code);
}
