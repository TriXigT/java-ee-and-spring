package se.plushogskolan.jee.utils.repository.mock;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jee.utils.domain.IdHolder;
import se.plushogskolan.jee.utils.repository.mock.MockTestFixture.IdHolderImpl;
import se.plushogskolan.jee.utils.repository.test.AbstractRepositoryTestNoRemoteServer;

/**
 * Unit test to verify that the MockAbstractRepository is functioning as expected
 * @author Pär
 *
 */
public class MockAbstractRepositoryTest extends AbstractRepositoryTestNoRemoteServer<IdHolder, MockAbstractRepository<IdHolder>>{

	private MockAbstractRepository<IdHolder> repo;
	
	@Before
	public void setup() {
		repo = MockTestFixture.getMockAbstractRepositoryImpl(10);
	}
	
	@Test
	public void testConstructor() {
		assertTrue("is generator set", repo.getIdGenerator() != null);
	}

	@Test
	public void testGetAllEntities() {
		assertTrue("number of entities in List", repo.getMockList().size() == 10);
	}

	@Override
	protected MockAbstractRepository<IdHolder> getRepository() {
		return repo;
	}

	@Override
	protected IdHolder getEntity1() {
		IdHolderImpl idHolderImpl = MockTestFixture.getIdHolderImpl();
		idHolderImpl.setId(1);
		
		return idHolderImpl;
	}

	@Override
	protected IdHolder getEntity2() {
		IdHolderImpl idHolderImpl = MockTestFixture.getIdHolderImpl();
		idHolderImpl.setId(2);
		
		return idHolderImpl;
	}

}
