package se.plushogskolan.jetbroker.agent.rest.exercises.http;

import java.util.logging.Logger;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * A controller that should be properly mapped using @RequestMapping. Run the
 * JMeter test and implement the request mapping so that the test is OK.
 * 
 */
@Controller
public class HttpTestController {

	Logger log = Logger.getLogger(HttpTestController.class.getName());

	@RequestMapping(value = "httpTest.html", method=RequestMethod.GET)
	@ResponseBody
	public String test_get_html() throws Exception {
		return "test.html GET html";
	}

	@RequestMapping(value = "httpTest.html", method=RequestMethod.POST)
	@ResponseBody
	public String test_post_html() throws Exception {
		return "test.html POST html";
	}

	@RequestMapping(value = "httpTest", method=RequestMethod.GET, consumes="text/plain")
	@ResponseBody
	public String test_get_text() throws Exception {
		return "test GET consumes=plain";
	}

	@RequestMapping(value = "httpTest", method=RequestMethod.GET, consumes="text/xml")
	@ResponseBody
	public String test_get_xml() throws Exception {
		return "test GET consumes=xml";
	}

	@RequestMapping(value = "httpTest", method=RequestMethod.GET, produces="text/xml")
	@ResponseBody
	public String test_get_produces_xml() throws Exception {
		return "test GET produces=xml";
	}

	@RequestMapping(value = "httpTest", method=RequestMethod.GET, produces="text/json")
	@ResponseBody
	public String test_get_json() throws Exception {
		return "test GET produces=json";
	}

	@RequestMapping(value = "httpTest", method=RequestMethod.PUT, produces="text/json")
	@ResponseBody
	public String test_put_json() throws Exception {
		return "test PUT produces=json";
	}

	@RequestMapping(value = "httpTest", method=RequestMethod.DELETE, produces="text/json")
	@ResponseBody
	public String test_delete_json() throws Exception {
		return "test DELETE produces=json";
	}

}
