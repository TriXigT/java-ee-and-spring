package se.plushogskolan.jetbroker.order.mvc.mock;

import java.util.Random;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.joda.time.DateTime;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.order.integration.simulator.SystemSimulator;

@Controller
@RequestMapping("createMockFlightRequest.html")
public class MockFlightRequestController {
	private static Logger log = Logger.getLogger(MockFlightRequestController.class.getName());
	
	@Inject
	private SystemSimulator simulator;
	
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView get() {
		log.fine("createMockFlightRequest");
		
		simulator.receiveFlightRequest(getRandomNumber(1, 10));
		
		return new ModelAndView("redirect:/index.html");
	}
	
	private int getRandomNumber(int min, int max) {
		return new Random().nextInt((max - min) + 1) + min;
	}
}
