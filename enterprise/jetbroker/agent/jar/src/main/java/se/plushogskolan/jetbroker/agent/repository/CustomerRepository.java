package se.plushogskolan.jetbroker.agent.repository;

import java.util.List;

import se.plushogskolan.jee.utils.repository.BaseRepository;
import se.plushogskolan.jetbroker.agent.domain.Customer;

public interface CustomerRepository  extends BaseRepository<Customer>{

	/**
	 * Retrieve all Customers from the datasource
	 * @return List of Customers
	 */
	List<Customer> getAllCustomers();
	
	Customer getCustomerByEmail(String email);
}
