package se.plushogskolan.jetbroker.plane.integration.plane;

import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicSession;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jee.utils.jms.JmsHelper;

@Prod
@Stateless
public class PlaneIntegrationFacadeImpl implements PlaneIntegrationFacade{

	@Inject
	private Logger log;
	
	@Resource(mappedName = JmsConstants.JNDI_CONNECTIONFACTORY)
	private TopicConnectionFactory connectionFactory;
	@Resource(mappedName = JmsConstants.TOPIC_PLANE_BROADCAST)
	private Topic topic;

	private TopicConnection connection;
	private TopicSession session;

	@PostConstruct
	private void init() {
		try {
			log.fine("initialize");
			
			connection = connectionFactory.createTopicConnection();
			session = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
			connection.start();
		} catch (JMSException e) {
			throw new RuntimeException(e);
		}
	}

	@PreDestroy
	public void cleanUp() {
		log.fine("destroy");
		JmsHelper.closeConnectionAndSession(connection, session);
	}

	@Override
	public void broadcastNewFuelPrice(double fuelPrice) {
		try {
			log.fine(String.format("broadcast fuel price [%f]", fuelPrice));
			
			BytesMessage msg = session.createBytesMessage();
			msg.writeDouble(fuelPrice);
			msg.setStringProperty("messageType", JmsConstants.MSGTYPE_PLANEBROADCAST_FUELPRICECHANGED);

			session.createPublisher(topic).publish(msg);
		} catch (JMSException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void broadcastAirportsChanged() {
		try {
			log.fine("broadcast airports changed");
			
			BytesMessage msg = session.createBytesMessage();
			msg.setStringProperty("messageType", JmsConstants.MSGTYPE_PLANEBROADCAST_AIRPORTSCHANGED);
			
			session.createPublisher(topic).publish(msg);
		} catch (JMSException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void broadcastPlaneTypesChanged() {
		try {
			log.fine("broadcast planetypes changed");
			
			BytesMessage msg = session.createBytesMessage();
			msg.setStringProperty("messageType", JmsConstants.MSGTYPE_PLANEBROADCAST_PLANETYPESCHANGED);
			
			session.createPublisher(topic).publish(msg);
		} catch (JMSException e) {
			throw new RuntimeException(e);
		}
	}
}
