package se.plushogskolan.jee.utils.repository.mock;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class MockIdGeneratorTest {
	MockIdGenerator generator;
	MockTestFixture.IdHolderImpl impl;
	
	@Before 
	public void setup() {
		impl = MockTestFixture.getIdHolderImpl();
	    generator = new MockIdGenerator();
	}
	
	@Test
	public void testAssignNextId() {
		assertTrue("first id is 1", generator.assignNextId(impl) == 1l);
		assertTrue("second id is 2", generator.assignNextId(impl) == 2l);
		assertTrue("ternary id is 3", generator.assignNextId(impl) == 3l);
	}

}
