<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>

<html>
<head>
<link href="<%=request.getContextPath()%>/style/common.css" type="text/css" rel="stylesheet" />
<link href="<%=request.getContextPath()%>/style/theme_stylish.css" type="text/css" rel="stylesheet" />
<c:set var="dateFormat"><spring:message code="index.flightRequest.dateFormat"/></c:set>
</head>
<body>
	<div class="container">
		<jsp:include page="header.jsp" />
		<div class="content">
			<div class="content_container" id="flight_request_container">
				<img src="<%=request.getContextPath()%>/images/flightrequest.png"/><h2><spring:message code="index.flightRequests.title"/></h2>
				<hr>
				<a href="<%=request.getContextPath()%>/editFlightRequest/0.html" class="button" id="create_flight_request_button"><spring:message code="index.flightRequests.addButton"/></a>
				<br>
				<h3><spring:message code="index.flightRequests.process.title"/></h3>
				<c:choose>
					<c:when test="${!empty createdFlightRequests}">
					<table class="datatable">
						<tr>
							<th><spring:message code="global.id"/></th>
							<th><spring:message code="flightRequest.status"/></th>
							<th><spring:message code="flightRequest.departureAirport"/></th>
							<th><spring:message code="flightRequest.arrivalAirport"/></th>
							<th><spring:message code="flightRequest.departureDate"/></th>
							<th><spring:message code="flightRequest.customer"/></th>
							<th><spring:message code="flightRequest.noOfPassengers"/></th>
							<th><spring:message code="global.edit"/></th>
						</tr>
						<c:forEach items="${createdFlightRequests}" var="created">
						<tr>
							<td>${created.id}</td>
							<td>${created.status.niceName}</td>
							<td>${created.departureAirportCode}</td>
							<td>${created.arrivalAirportCode}</td>
							<td><joda:format value="${created.date}" pattern="${dateFormat}"/></td>
							<td>${created.customer.niceName}</td>
							<td>${created.passengers}</td>
							<td><a href="<%=request.getContextPath()%>/editFlightRequest/${created.id}.html"><img alt="<spring:message code="global.edit"/>" src="<%=request.getContextPath()%>/images/edit.png"/></a></td>
						</tr>
						</c:forEach>
					</table>
				</c:when>
				<c:otherwise>
					<span><spring:message code="index.flightRequests.noCreated"></spring:message></span>
				</c:otherwise>
				</c:choose>
				<h3><spring:message code="index.flightRequests.offer.title"/></h3>
				<c:choose>
					<c:when test="${!empty processedFlightRequests}">
						<table class="datatable">
							<tr>
								<th><spring:message code="global.id"/></th>
								<th><spring:message code="flightRequest.status"/></th>
								<th><spring:message code="flightRequest.departureAirport"/></th>
								<th><spring:message code="flightRequest.arrivalAirport"/></th>
								<th><spring:message code="flightRequest.departureDate"/></th>
								<th><spring:message code="flightRequest.customer"/></th>
								<th><spring:message code="flightRequest.noOfPassengers"/></th>
								<th><spring:message code="flightRequest.planeType"/></th>
								<th><spring:message code="flightRequest.offeredPrice"/></th>
							</tr>
							<c:forEach items="${processedFlightRequests}" var="processed">
							<tr>
								<td>${processed.id}</td>
								<td>${processed.status.niceName}</td>
								<td>${processed.departureAirportCode}</td>
								<td>${processed.arrivalAirportCode}</td>
								<td><joda:format value="${processed.date}" pattern="${dateFormat}"/></td>
								<td>${processed.customer.niceName}</td>
								<td>${processed.passengers}</td>
								<td>${processed.planeTypeCode}</td>
								<td>${processed.offeredPrice} SEK</td>
							</tr>
							</c:forEach>
						</table>
					</c:when>
					<c:otherwise>
						<span><spring:message code="index.flightRequests.noProcessed"></spring:message></span>
					</c:otherwise>
				</c:choose>
				<h3><spring:message code="index.flightRequests.rejected.title"/></h3>
				<c:choose>
					<c:when test="${!empty rejectedFlightRequests}">
					<table class="datatable">
						<tr>
							<th><spring:message code="global.id"/></th>
							<th><spring:message code="flightRequest.status"/></th>
							<th><spring:message code="flightRequest.departureAirport"/></th>
							<th><spring:message code="flightRequest.arrivalAirport"/></th>
							<th><spring:message code="flightRequest.departureDate"/></th>
							<th><spring:message code="flightRequest.customer"/></th>
							<th><spring:message code="flightRequest.noOfPassengers"/></th>
						</tr>
						<c:forEach items="${rejectedFlightRequests}" var="rejected">
						<tr>
							<td>${rejected.id}</td>
							<td>${rejected.status.niceName}</td>
							<td>${rejected.departureAirportCode}</td>
							<td>${rejected.arrivalAirportCode}</td>
							<td><joda:format value="${rejected.date}" pattern="${dateFormat}"/></td>
							<td>${rejected.customer.niceName}</td>
							<td>${rejected.passengers}</td>
						</tr>
						</c:forEach>
					</table>
					</c:when>
					<c:otherwise>
						<span><spring:message code="index.flightRequests.noRejected"></spring:message></span>
					</c:otherwise>
				</c:choose>
			</div>
			<div class="content_container" id="generic_container">
			<img src="<%=request.getContextPath()%>/images/customer.png"/><h2><spring:message code="index.Customers.title"/></h2>
				<hr>
				<a href="<%=request.getContextPath()%>/editCustomer/0.html" class="button" id="create_customer_button"><spring:message code="index.Customers.addButton"/></a>
				<br>
				<c:choose>
					<c:when test="${!empty customers}">
					<table class="datatable">
						<tr>
							<th><spring:message code="global.id"/></th>
							<th><spring:message code="global.customer.firstName"/></th>
							<th><spring:message code="global.customer.lastName"/></th>
							<th><spring:message code="global.customer.email"/></th>
							<th><spring:message code="global.customer.company"/></th>
							<th><spring:message code="global.edit"/></th>
						</tr>
						
						<c:forEach items="${customers}" var="customer">
						<tr>
							<td>${customer.id}</td>
							<td>${customer.firstName}</td>
							<td>${customer.lastName}</td>
							<td>${customer.email}</td>
							<td>${customer.company}</td>
							<td><a href="<%=request.getContextPath()%>/editCustomer/${customer.id}.html"><img alt="<spring:message code="global.edit"/>" src="<%=request.getContextPath()%>/images/edit.png"/></a></td>
						</tr>
						</c:forEach>
					</table>
				</c:when>
				<c:otherwise>
					<span><spring:message code="global.customer.noCustomers"></spring:message></span>
				</c:otherwise>
			</c:choose>				
			</div>
		</div>
	</div>
</body>

</html>