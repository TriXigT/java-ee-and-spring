package se.plushogskolan.jetbroker.order.service;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.*;

import java.util.logging.Logger;

import org.easymock.Capture;
import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.order.TestFixture;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.order.domain.FlightRequestStatus;
import se.plushogskolan.jetbroker.order.integration.agent.AgentIntegrationFacade;
import se.plushogskolan.jetbroker.order.repository.FlightRequestRepository;

public class FlightRequestServiceImplTest {

	private FlightRequestServiceImpl service;
	private FlightRequestRepository repository;
	private FlightUtilityService utils;
	private AgentIntegrationFacade facade;
	private FlightRequest request;
	
	@Before
	public void setup() {
		service = new FlightRequestServiceImpl();
		
		request = TestFixture.getValidFlightRequest();
		request.setId(1);
		
		repository = createMock(FlightRequestRepository.class);
		facade = createMock(AgentIntegrationFacade.class);
		utils = createMock(FlightUtilityService.class);
		
		service.setFlightRequestRepository(repository);
		service.setAgentIntegrationFacade(facade);
		service.setFlightUtils(utils);
		service.setLog(Logger.getLogger(FlightRequestServiceImpl.class.getName()));
	}
	
	@Test
	public void handleIncomingFlightRequestTest() {
		expect(repository.persist(request)).andReturn(request.getId());
		expect(repository.findById(request.getId())).andReturn(request);
		expect(utils.getDistanceForFlight(request.getDepartureAirportCode(), request.getArrivalAirportCode())).andReturn(20);
		Capture<FlightRequestConfirmation> confirmationCapture = new Capture<FlightRequestConfirmation>();
		facade.sendFlightRequestConfirmation(capture(confirmationCapture));

		replay(repository, utils, facade);
		
		service.handleIncomingFlightRequest(request);
		
		assertEquals("status is set", FlightRequestStatus.NEW, request.getStatus());
		assertEquals("distance is set", 20, request.getDistanceKm());
		assertEquals("send method in facade called with correct request", confirmationCapture.getValue().getAgentRequestId(), request.getAgentRequestId());
		assertEquals("send method in facade called with correct request", confirmationCapture.getValue().getOrderRequestId(), request.getId());
		
		verify(repository, utils, facade);
	}
}
