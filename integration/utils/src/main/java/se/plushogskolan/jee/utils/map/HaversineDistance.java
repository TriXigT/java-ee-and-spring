package se.plushogskolan.jee.utils.map;

/**
 * This is the implementation Haversine Distance Algorithm between two places
 */
public class HaversineDistance {

	public static void main(String[] args) {
		getDistance(59.3, 18, 57.7, 12);

	}

	public static double getDistance(double lat1, double lng1, double lat2, double lng2) {
		final int R = 6371; // Radius of the earth
		Double latDistance = toRad(lat2 - lat1);
		Double lonDistance = toRad(lng2 - lng1);
		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(toRad(lat1))
				* Math.cos(toRad(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		Double distance = R * c;

		// System.out.println("The distance between two lat and long is::" +
		// distance);

		return distance;

	}

	private static Double toRad(Double value) {
		return value * Math.PI / 180;
	}

}
