package se.plushogskolan.jetbroker;

import static org.junit.Assert.fail;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.apache.log4j.Logger;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.joda.time.DateTime;

import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.Status;

public class TestFixture {
	private static Logger log = Logger.getLogger(TestFixture.class);
	
	public static FlightRequest getValidFlightRequest() {
		
		return new FlightRequest(0,Status.CREATED,"GBG", "MLM", DateTime.now(), new Customer("Wut","Wuta","Wute@c.c","Wut Inc."), "TEST", 0, 0);
	}
	
	public static FlightRequest getValidFlightRequest(int id, String departure, String arrival, DateTime date, Customer customer, int passengers) {
		FlightRequest f = getValidFlightRequest();
		f.setId(id);
		f.setArrivalAirportCode(departure);
		f.setDepartureAirportCode(arrival);
		f.setDate(date);
		f.setCustomer(customer);
		f.setPassengers(passengers);
		return f;
	}

	public static Customer getValidCustomer() {
		return new Customer("Robert","Tornberg","robert@tce.se","Tornberg Consulting Enterprises");
	}
	
	public static Customer getValidCustomer(int id, String firstName, String lastName, String email) {
		Customer c = getValidCustomer();
		c.setId(id);
		c.setFirstName(firstName);
		c.setLastName(lastName);
		c.setEmail(email);
		return c;
	}
	
	/**
	 * Helper method to check if a certain property has reported errors
	 */
	public static void assertPropertyIsInvalid(String property,
			Set<? extends ConstraintViolation<?>> violations) {

		boolean errorFound = false;
		for (ConstraintViolation<?> constraintViolation : violations) {
			if (constraintViolation.getPropertyPath().toString()
					.equals(property)) {
				errorFound = true;
				break;
			}
		}

		if (!errorFound) {
			fail("Expected validation error for '" + property
					+ "', but no such error exists");
		}
	}

	public static Validator getValidator() {
		return Validation.buildDefaultValidatorFactory().getValidator();
	}
	
	public static Archive<?> createIntegrationTestArchive() {
		MavenDependencyResolver mvnResolver = DependencyResolvers.use(MavenDependencyResolver.class)
				.loadMetadataFromPom("pom.xml");

		WebArchive war = ShrinkWrap.create(WebArchive.class, "agent_test.war").addPackages(true, "se.plushogskolan")
				.addAsWebInfResource("beans.xml").addAsResource("META-INF/persistence.xml");

		war.addAsLibraries(mvnResolver.artifact("org.easymock:easymock:3.2").resolveAsFiles());
		war.addAsLibraries(mvnResolver.artifact("joda-time:joda-time:2.2").resolveAsFiles());
		war.addAsLibraries(mvnResolver.artifact("org.jadira.usertype:usertype.core:3.1.0.CR8").resolveAsFiles());

		return war;
	}
}
