<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
        
<script type="text/javascript">
$(document).ready(function() {
	$('.formElement float_left').attr('required', ''); 
});
</script>

<html>
<head>
<link href="<%=request.getContextPath()%>/style/common.css" type="text/css" rel="stylesheet" />
<link href="<%=request.getContextPath()%>/style/theme_stylish.css" type="text/css" rel="stylesheet" />
</head>
<body>
	<div class="container">
		<jsp:include page="header.jsp" />
		<div class="content">
			<div class="content_container" id="generic_container">
				<img src="<%=request.getContextPath()%>/images/flightrequest.png"/>
					<h2>
						<c:choose>
							<c:when test="${editCustomerBean.customer.id > 0}">
								<spring:message code="customer.edit.title"/>
							</c:when>
							<c:otherwise>
								<spring:message code="customer.add.title"/>
							</c:otherwise>
						</c:choose>
					</h2>
				<hr>
				<form:form id="generic_form" commandName="editCustomerBean">
					<c:if test="${editCustomerBean.customer.id > 0}">
						<label for="id_field" class="small_width"><spring:message code="global.id"/></label>
						<span id="id_field">${editCustomerBean.customer.id}</span>
						<span class="clear small_space float_left"></span>
					</c:if>
					
					<form:hidden path="customer.id" value="${editCustomerBean.customer.id}"/>
					
					<label for="firstName" class="small_width"><spring:message code="global.customer.firstName"/></label>
					<form:input id="firstName" class="formElement float_left" path="customer.firstName"/>
					<form:errors path="customer.firstName" class="errors"/>
					<span class="clear small_space float_left"></span>
					
					<label for="lastName" class="small_width"><spring:message code="global.customer.lastName"/></label>
					<form:input id="lastName" class="formElement float_left" path="customer.lastName"/>
					<form:errors path="customer.lastName" class="errors"/>
					<span class="clear small_space float_left"></span>
					
					<label for="email" class="small_width"><spring:message code="global.customer.email"/></label>
					<form:input id="email" class="formElement float_left" type="email" path="customer.email"/>	
					<form:errors path="customer.email" class="errors"/>
					<span class="clear small_space float_left"></span>
					
					<label for="company" class="small_width"><spring:message code="global.customer.company"/></label>	
					<form:input id="company" class="formElement float_left" path="customer.company"/> 
					<form:errors path="customer.company" class="errors"/>
					<span class="clear space float_left"></span>
					
					<input type="submit" value="<spring:message code="global.submit"/>"/>
					<a id="generic_button" class="button_style" href="<%=request.getContextPath()%>/index.html"><spring:message code="global.cancel"/></a>
				</form:form>
			</div>
		</div>
	</div>
</body>

</html>