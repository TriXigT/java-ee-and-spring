package se.plushogskolan.jetbroker.agent.integration.plane.mdb;

import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.MessageListener;

import net.sf.ehcache.Cache;
import se.plushogskolan.jee.utils.ehcache.CacheName;
import se.plushogskolan.jee.utils.jms.JmsConstants;

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = JmsConstants.DESTINATION_TYPE_TOPIC),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = JmsConstants.TOPIC_PLANE_BROADCAST),
		@ActivationConfigProperty(propertyName = "messageSelector",
		propertyValue = "messageType = '" + JmsConstants.MSGTYPE_PLANEBROADCAST_AIRPORTSCHANGED + "'")})
public class PlaneTypeMDB implements MessageListener{

	private Logger log = Logger.getLogger(PlaneTypeMDB.class.getName());

	@Inject
	@CacheName(cacheName = "planeTypeCache")
	private Cache cache;
	
	@Override
	public void onMessage(Message message) {
		log.fine("Received messeage");

		cache.removeAll();

		log.fine("Cache cleared");
	}
}
