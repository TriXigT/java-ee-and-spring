package se.plushogskolan.jetbroker.order.mvc;

import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.mvc.bean.EditPlaneBean;
import se.plushogskolan.jetbroker.order.service.PlaneService;
import se.plushogskolan.jetbroker.order.service.PlaneTypeService;

@Controller
@RequestMapping("editPlane/{id}.html")
public class EditPlaneController {
	private static Logger log = Logger.getLogger(EditPlaneController.class.getName());
	
	@Inject
	private PlaneTypeService planeTypeService;
	@Inject
	private PlaneService planeService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView get(@PathVariable long id) {
		log.fine("GET EditPlane");
		
		EditPlaneBean editPlaneBean = new EditPlaneBean();

		if(id>0) {
			log.fine(String.format("get Plane with id %d",id));
			editPlaneBean.setPlane(planeService.getPlane(id));
		}
		
		return getModelAndView(editPlaneBean);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView handleSubmit(@Valid EditPlaneBean editPlaneBean, BindingResult errors) {
		log.fine("handle submit");
		if(errors.hasErrors()) {
			log.fine("submit had errors");
			return getModelAndView(editPlaneBean);
		}
		
		Plane plane = editPlaneBean.getPlane();
		
		if(editPlaneBean.getPlane().getId() > 0) {
			log.fine("update plane");
			planeService.updatePlane(plane);
		} else {
			log.fine("persist new plane");
			planeService.createPlane(plane);
		}

		return new ModelAndView("redirect:/index.html");
	}
	
	public ModelAndView getModelAndView(EditPlaneBean editPlaneBean) {
		ModelAndView mav = new ModelAndView("editPlane");
		
		List<PlaneType> planeTypes = planeTypeService.getAllPlaneTypes();
		
		mav.addObject("editPlaneBean", editPlaneBean);
		mav.addObject("planeTypes", planeTypes);
		
		return mav;
	}
}
