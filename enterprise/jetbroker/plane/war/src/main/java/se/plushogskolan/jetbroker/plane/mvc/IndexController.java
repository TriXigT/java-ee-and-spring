package se.plushogskolan.jetbroker.plane.mvc;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.plane.domain.Airport;
import se.plushogskolan.jetbroker.plane.domain.PlaneType;
import se.plushogskolan.jetbroker.plane.service.AirportService;
import se.plushogskolan.jetbroker.plane.service.PlaneTypeService;

@Controller
public class IndexController {

	@Inject
	private PlaneTypeService planeService;

	@Inject
	private AirportService airportService;

	@RequestMapping("/index.html")
	public ModelAndView index() {
		
		List<PlaneType> planeTypes = getPlaneService().getAllPlaneTypes();
		List<Airport> airports = getAirportService().getAllAirports();
		double fuelCost = getAirportService().getFuelCost();

		ModelAndView mav = new ModelAndView("index");
		mav.addObject("planeTypes", planeTypes);
		mav.addObject("airports", airports);
		mav.addObject("fuelCost", fuelCost);
		return mav;
	}

	
	public PlaneTypeService getPlaneService() {
		return planeService;
	}

	public void setPlaneService(PlaneTypeService planeService) {
		this.planeService = planeService;
	}

	public AirportService getAirportService() {
		return airportService;
	}

	public void setAirportService(AirportService airportService) {
		this.airportService = airportService;
	}

}
