package se.plushogskolan.jetbroker.agent.mvc;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.Status;
import se.plushogskolan.jetbroker.agent.service.CustomerService;
import se.plushogskolan.jetbroker.agent.service.FlightRequestService;

@Controller
public class IndexController {
	private static Logger log = Logger.getLogger(IndexController.class.getName());

	@Inject
	private FlightRequestService flightRequestService;

	@Inject
	CustomerService customerService;

	@RequestMapping("/index.html")
	public ModelAndView index() {
		log.fine("GET index");
		ModelAndView mav = new ModelAndView("index");

		mav.addObject("createdFlightRequests", getFlightRequestBeansByStatuses(Status.CREATED, Status.REQUEST_CONFIRMED));
		mav.addObject("processedFlightRequests", getFlightRequestBeansByStatuses(Status.OFFER_RECEIVED));
		mav.addObject("rejectedFlightRequests", getFlightRequestBeansByStatuses(Status.REJECTED));
		mav.addObject("customers", customerService.getAllCustomers());

		return mav;
	}

	/**
	 * Utility method for retrieving FlightRequests matching supplied Statuses
	 * and returning a List of the FlightRequests.
	 * @param statuses to find
	 * @return a List of FlightRequests
	 */
	private List<FlightRequest> getFlightRequestBeansByStatuses(Status ...statuses) {
		List<FlightRequest> flightRequests = new ArrayList<FlightRequest>();

		for(Status status : statuses) {
			List<FlightRequest> byStatus = flightRequestService.getFlightRequestsByStatus(status);
			flightRequests.addAll(byStatus);
		}

		return flightRequests;
	}
}
