package se.plushogskolan.jetbroker.agent.repository;

import java.util.List;

import se.plushogskolan.jee.utils.repository.BaseRepository;
import se.plushogskolan.jetbroker.agent.domain.PlaneType;


public interface PlaneTypeRepository extends BaseRepository<PlaneType>{

	List<PlaneType> getAllPlaneTypes();

	PlaneType getPlaneTypeByCode(String code);
}
