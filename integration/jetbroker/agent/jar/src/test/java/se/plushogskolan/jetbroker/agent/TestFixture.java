package se.plushogskolan.jetbroker.agent;

import org.apache.log4j.Logger;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.joda.time.DateTime;

import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestStatus;

public class TestFixture {

	private static Logger log = Logger.getLogger(TestFixture.class.getName());

	public static Customer getValidCustomer() {
		return getValidCustomer(1);
	}

	public static Customer getValidCustomer(long id) {
		return getValidCustomer(id, "Firstname", "Lastname");
	}

	public static Customer getValidCustomer(long id, String firstName, String lastName) {

		Customer customer = new Customer();
		customer.setId(id);
		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		String email = firstName.trim() + "@" + lastName.trim() + ".com";
		customer.setEmail(email.toLowerCase());
		customer.setCompany("Company");
		return customer;

	}

	public static FlightRequest getValidFlightRequest() {
		return getValidFlightRequest(getValidCustomer());
	}

	public static FlightRequest getValidFlightRequest(Customer customer) {

		FlightRequest r = new FlightRequest();
		r.setCustomer(customer);
		r.setArrivalAirportCode("GBG");
		r.setDepartureAirportCode("STH");
		r.setNoOfPassengers(200);
		r.setDepartureTime(new DateTime(2015, 5, 24, 11, 12, 13, 0));
		r.setRequestStatus(FlightRequestStatus.CREATED);
		return r;
	}
	
	public static FlightRequestConfirmation getValidFlightRequestConfirmation() {
		return new FlightRequestConfirmation(1l,1l);
	}

	public static Archive<?> createIntegrationTestArchive() {

		MavenDependencyResolver mvnResolver = DependencyResolvers.use(MavenDependencyResolver.class)
				.loadMetadataFromPom("pom.xml");

		WebArchive war = ShrinkWrap.create(WebArchive.class, "agent_test.war").addPackages(true, "se.plushogskolan")
				.addAsWebInfResource("beans.xml").addAsResource("META-INF/persistence.xml");

		war.addAsLibraries(mvnResolver.artifact("joda-time:joda-time:2.2").resolveAsFiles());
		war.addAsLibraries(mvnResolver.artifact("org.jadira.usertype:usertype.core:3.1.0.CR8").resolveAsFiles());

		log.info("JAR: " + war.toString(true));
		return war;
	}

}
