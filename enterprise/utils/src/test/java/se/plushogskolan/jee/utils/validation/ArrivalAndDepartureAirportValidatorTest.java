package se.plushogskolan.jee.utils.validation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class ArrivalAndDepartureAirportValidatorTest {

	private ArrivalAndDepartureAirportValidator validator;
	private HolderImpl impl;

	@Before
	public void setup() {
		validator = new ArrivalAndDepartureAirportValidator();
		impl = new HolderImpl("AAA", "AAA");
	}
	@Test
	public void isSameTest() {
		assertFalse("is same", validator.isValid(impl, null));
	}
	
	@Test
	public void isNotSameTest() {
		impl.setArrivalAirportCode("OTHER");
		assertTrue("is not same", validator.isValid(impl, null));
	}
	
	@Test
	public void isNullTest() {
		impl.setDepartureAirportCode(null);
		assertFalse("is null", validator.isValid(impl, null));
	}


	
	private class HolderImpl implements ArrivalAndDepartureAirportHolder {
		String arrivalCode;
		String departureCode;
		
		public HolderImpl(String arrivalCode, String departureCode) {
			this.arrivalCode = arrivalCode;
			this.departureCode = departureCode;
		}
		
		@Override
		public String getDepartureAirportCode() {
			return arrivalCode;
		}

		@Override
		public String getArrivalAirportCode() {
			return departureCode;
		}

		public void setArrivalAirportCode(String arrivalCode) {
			this.arrivalCode = arrivalCode;
		}

		public void setDepartureAirportCode(String departureCode) {
			this.departureCode = departureCode;
		}
		
	}

}
