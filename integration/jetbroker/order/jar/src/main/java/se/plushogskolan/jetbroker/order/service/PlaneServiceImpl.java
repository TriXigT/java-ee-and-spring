package se.plushogskolan.jetbroker.order.service;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.repository.AirportRepository;
import se.plushogskolan.jetbroker.order.repository.PlaneRepository;
import se.plushogskolan.jetbroker.order.repository.PlaneTypeRepository;

@Stateless
public class PlaneServiceImpl implements PlaneService {

	@Inject
	private Logger log;
	
	@Inject
	private AirportRepository airportRepository;

	@Inject
	private PlaneRepository planeRepository;

	@Inject
	private PlaneTypeRepository planeTypeRepository;

	@Override
	public Airport getAirport(String code) {
		log.fine(String.format("get airport by code [%s]", code));
		
		return airportRepository.getAirport(code);
	}

	@Override
	public List<Plane> getAllPlanes() {
		log.fine("get all planes");
		
		return planeRepository.getAllPlanes();
	}

	@Override
	public Plane getPlane(long id) {
		log.fine(String.format("get plane by id [%d]", id));
		
		return planeRepository.findById(id);
	}

	@Override
	public Plane createPlane(Plane plane) {
		log.fine(String.format("create plane: %s", plane.toString()));
		
		long id = planeRepository.persist(plane);
		return planeRepository.findById(id);
	}

	@Override
	public void updatePlane(Plane plane) {
		log.fine(String.format("update plane: %s", plane.toString()));
		
		planeRepository.update(plane);

	}

	@Override
	public PlaneType getPlaneType(String code) {
		log.fine(String.format("get planetype by code [%s]", code));
		
		return planeTypeRepository.getPlaneType(code);
	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {
		log.fine("get all planetypes");
		
		return planeTypeRepository.getAllPlaneTypes();
	}

	@Override
	public void deletePlane(long id) {
		Plane plane = getPlane(id);
		
		log.fine(String.format("delete plane: %s", plane.toString()));
		
		planeRepository.remove(plane);

	}

}
