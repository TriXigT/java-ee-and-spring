package se.plushogskolan.jetbroker.agent.domain.request;

/**
 * A confirmation from the order system that a flight request was received
 * 
 */
public class FlightRequestConfirmation {

	private long agentRequestId;
	private long orderRequestId;

	public FlightRequestConfirmation() {

	}

	public FlightRequestConfirmation(long agentRequestId, long orderRequestId) {
		setAgentRequestId(agentRequestId);
		setOrderRequestId(orderRequestId);
	}

	public long getAgentRequestId() {
		return agentRequestId;
	}

	public void setAgentRequestId(long agentRequestId) {
		this.agentRequestId = agentRequestId;
	}

	public long getOrderRequestId() {
		return orderRequestId;
	}

	public void setOrderRequestId(long orderRequestId) {
		this.orderRequestId = orderRequestId;
	}

	@Override
	public String toString() {
		return "FlightRequestConfirmation [agentRequestId=" + agentRequestId + ", orderRequestId=" + orderRequestId
				+ "]";
	}

}
