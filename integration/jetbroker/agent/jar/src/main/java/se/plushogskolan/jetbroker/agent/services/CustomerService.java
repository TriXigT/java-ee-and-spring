package se.plushogskolan.jetbroker.agent.services;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.agent.domain.Customer;

@Local
public interface CustomerService {

	Customer getCustomer(long id);

	Customer createCustomer(Customer customer);

	void updateCustomer(Customer customer);

	List<Customer> getAllCustomers();

	void deleteCustomer(long id);

}
