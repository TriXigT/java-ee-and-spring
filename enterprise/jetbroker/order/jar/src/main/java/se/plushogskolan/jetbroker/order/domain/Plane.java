package se.plushogskolan.jetbroker.order.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotBlank;

import se.plushogskolan.jee.utils.domain.IdHolder;

@Entity
public class Plane implements IdHolder{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@NotBlank
	private String planeTypeCode;
	
	@NotBlank
	private String description;

	public Plane () {
	}
	
	public Plane(int id, String planeTypeCode, String description) {
		setId(id);
		setPlaneTypeCode(planeTypeCode);
		setDescription(description);
	}
	
	public String getPlaneTypeCode() {
		return planeTypeCode;
	}


	public void setPlaneTypeCode(String planeTypeCode) {
		this.planeTypeCode = planeTypeCode;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public void setId(long id) {
		this.id = id;
	}
	
	@Override
	public long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result
				+ ((planeTypeCode == null) ? 0 : planeTypeCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Plane other = (Plane) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (planeTypeCode == null) {
			if (other.planeTypeCode != null)
				return false;
		} else if (!planeTypeCode.equals(other.planeTypeCode))
			return false;
		return true;
	}



	
}
