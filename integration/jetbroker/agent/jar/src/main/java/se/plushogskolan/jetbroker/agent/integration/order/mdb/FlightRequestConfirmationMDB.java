package se.plushogskolan.jetbroker.agent.integration.order.mdb;

import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.agent.services.FlightRequestService;

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = JmsConstants.DESTINATION_TYPE_QUEUE),
		@ActivationConfigProperty(propertyName="destination", propertyValue=JmsConstants.QUEUE_FLIGHTREQUEST_RESPONSE),
		@ActivationConfigProperty(propertyName="messageType", propertyValue=JmsConstants.MSGTYPE_FLIGHTREQUEST_RESPONSE_CONFIRMATION)
})
public class FlightRequestConfirmationMDB implements MessageListener {

	@Inject
	private Logger log;
	
	@Inject
	private FlightRequestService flightRequestService;
	
	@Override
	public void onMessage(Message message) {
		log.fine("Received message");
		
		BytesMessage bytesMessage = (BytesMessage) message;
		long agentId;
		long orderId;
		try {
			agentId = bytesMessage.getLongProperty("agentRequestId");
			orderId = bytesMessage.getLongProperty("orderRequestId");
		} catch (JMSException e) {
			throw new RuntimeException(e);
		}
		
		FlightRequestConfirmation response = new FlightRequestConfirmation(agentId, orderId);

		log.fine(String.format("confirmation received: %s", response.toString()));
		
		flightRequestService.handleFlightRequestConfirmation(response);
	}
}