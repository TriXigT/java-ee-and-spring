package se.plushogskolan.jetbroker.plane.mvc;

import java.util.logging.Logger;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.plane.service.AirportService;
import se.plushogskolan.jetbroker.plane.service.FuelPriceService;

@Controller
@RequestMapping("/editFuelCost.html")
public class EditFuelCostController {

	private static Logger log = Logger.getLogger(EditFuelCostController.class.getName());

	@Inject
	private AirportService airportService;
	
	@Inject
	private FuelPriceService fuelPriceService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView index() {

		log.fine("Edit fuel cost");

		double fuelCost = fuelPriceService.getFuelPrice();

		EditFuelCostBean bean = new EditFuelCostBean();
		bean.setFuelCost(fuelCost);
		ModelAndView mav = new ModelAndView("editFuelCost");
		mav.addObject("editFuelCostBean", bean);
		return mav;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView handleSubmit(@Valid EditFuelCostBean bean, BindingResult errors) throws Exception {

		if (errors.hasErrors()) {
			ModelAndView mav = new ModelAndView("editFuelCost");
			mav.addObject("editFuelCostBean", bean);
			return mav;
		}

		fuelPriceService.updateFuelPrice(bean.getFuelCost());

		return new ModelAndView("redirect:/index.html");
	}

	public AirportService getAirportService() {
		return airportService;
	}

	public void setAirportService(AirportService airportService) {
		this.airportService = airportService;
	}

}
