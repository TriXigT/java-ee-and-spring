package se.plushogskolan.jetbroker.agent.repository.jpa;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.repository.AbstractRepositoryTest;

@RunWith(Arquillian.class)
@Transactional(value = TransactionMode.ROLLBACK)
public class JpaCustomerRepositoryIntegrationTest extends AbstractRepositoryTest<Customer, JpaCustomerRepository>{

	private static Logger log = Logger.getLogger(JpaCustomerRepositoryIntegrationTest.class);
	
	@Inject
	JpaCustomerRepository repo;

	@Test
	public void testDeleteOldCustomers() {
		long id1 = repo.persist(getEntity1());
		
		Customer customer2 = getEntity2();
		repo.persist(customer2);
		
		assertEquals("Customers in DB", 2, repo.getAllCustomers().size());
		
		repo.remove(customer2);
		
		assertEquals("Number of Customers after delete", 1, repo.getAllCustomers().size());
		assertEquals("Correct customer is in DB", id1, repo.getAllCustomers().iterator().next().getId());
	}
	
	@Test
	public void testGetAllCustomers() {
		repo.persist(getEntity1());
		repo.persist(getEntity2());
		
		assertTrue("No. of customers", repo.getAllCustomers().size() == 2);
	}
	
	@Test(expected = Exception.class)
	public void testDuplicateEmailValidation() {
		Customer first = getEntity1();
		Customer second = getEntity2();
		first.setEmail("hej@hej.se");
		second.setEmail("hej@hej.se");
		
		repo.persist(first);
		repo.persist(second);
	}
	
	@Test(expected = Exception.class)
	public void testValidationInvalidState() {
		Customer customer = getEntity1();
		customer.setEmail("testing");
		
		repo.persist(customer);
	}

	@Override
	protected JpaCustomerRepository getRepository() {
		return repo;
	}

	@Override
	protected Customer getEntity1() {
		return TestFixture.getValidCustomer(0, "Robert", "Tornberg", "a@a.se");
	}

	@Override
	protected Customer getEntity2() {
		return TestFixture.getValidCustomer(0, "Linnea", "Amsen", "b@b.se");
	}
}
