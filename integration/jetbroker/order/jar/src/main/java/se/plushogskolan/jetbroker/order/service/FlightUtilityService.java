package se.plushogskolan.jetbroker.order.service;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.PlaneType;

@Local
public interface FlightUtilityService {
	int getTimeForFlightInMinutes(String firstAirportCode, String secondAirportCode, PlaneType planeType);
	double getFuelPriceForFlight(String firstAirportCode, String secondAirportCode, PlaneType planeType);
	int getDistanceForFlight(String firstAirportCode, String secondAirportCode);
}
