package se.plushogskolan.jetbroker.agent.services;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.agent.domain.request.FlightOffer;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestConfirmation;

@Local
public interface FlightRequestService {

	FlightRequest getFlightRequest(long id);

	FlightRequest createFlightRequest(FlightRequest request);

	void updateFlightRequest(FlightRequest request);

	List<FlightRequest> getAllFlightRequests();

	void handleFlightRequestRejection(long flightRequestId);

	void handleFlightRequestConfirmation(FlightRequestConfirmation response);

	void handleFlightOffer(FlightOffer offer, long flightRequestId);

	List<FlightRequest> getFlightRequestsForCustomer(long id);

	void deleteFlightRequest(long id);

}