package se.plushogskolan.jee.utils.jms;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Session;

public class JmsHelper {

	public static void closeConnectionAndSession(Connection con, Session session) throws RuntimeException {

		try {
			if (session != null) {
				session.close();
			}
			if (con != null) {
				con.close();
			}
		} catch (JMSException e) {
			throw new RuntimeException("Unable to close JMS connection and session", e);
		}
	}

}
