package se.plushogskolan.jetbroker.agent.rest.flightrequest.model;

import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.rest.flightrequest.FlightRequestsController;

public class GetFlightRequestResponse {
	private long id;
	private long customerId;
	private long confirmationId;
	private int noOfPassengers;
	private String departureAirportCode;
	private String arrivalAirportCode;
	private String date;
	private String status;

	public GetFlightRequestResponse(FlightRequest fr) {

		setId(fr.getId());
		setStatus(fr.getRequestStatus().toString());
		setCustomerId(fr.getCustomer().getId());
		setConfirmationId(fr.getConfirmationId());
		setNoOfPassengers(fr.getNoOfPassengers());
		setDepartureAirportCode(fr.getDepartureAirportCode());
		setArrivalAirportCode(fr.getArrivalAirportCode());
		setDate(fr.getDepartureTime().toString(JmsConstants.DATE_FORMAT));
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public int getNoOfPassengers() {
		return noOfPassengers;
	}

	public void setNoOfPassengers(int noOfPassengers) {
		this.noOfPassengers = noOfPassengers;
	}

	public String getDepartureAirportCode() {
		return departureAirportCode;
	}

	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}

	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}

	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public long getConfirmationId() {
		return confirmationId;
	}

	public void setConfirmationId(long confirmationId) {
		this.confirmationId = confirmationId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "GetFlightRequestResponse [id=" + id + ", customerId=" + customerId + ", confirmationId="
				+ confirmationId + ", noOfPassengers=" + noOfPassengers + ", departureAirportCode="
				+ departureAirportCode + ", arrivalAirportCode=" + arrivalAirportCode + ", date=" + date + ", status="
				+ status + "]";
	}

}
