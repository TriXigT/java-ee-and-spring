package se.plushogskolan.jetbroker.order.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.annotation.Prod;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.Status;
import se.plushogskolan.jetbroker.order.integration.annotation.FlightRequestChanged;
import se.plushogskolan.jetbroker.order.integration.annotation.FlightRequestChanged.FlightRequestStatus;
import se.plushogskolan.jetbroker.order.integration.event.FlightRequestChangedEvent;
import se.plushogskolan.jetbroker.order.integration.event.NewFlightRequestEvent;
import se.plushogskolan.jetbroker.order.integration.facade.IntegrationFacade;
import se.plushogskolan.jetbroker.order.integration.simulator.SystemSimulator;
import se.plushogskolan.jetbroker.order.repository.FlightRequestRepository;
import se.plushogskolan.jetbroker.order.service.FlightRequestService;

@Stateless
public class FlightRequestServiceImpl implements FlightRequestService {
	@Inject
	private FlightRequestRepository repo;
	
	@Inject
	@Prod
	@FlightRequestChanged(FlightRequestStatus.CONFIRMED)
	private Event<FlightRequestChangedEvent> flightRequestConfirmEvent;
	
	@Inject
	@Prod
	@FlightRequestChanged(FlightRequestStatus.OFFER_SENT)
	private Event<FlightRequestChangedEvent> flightRequestSentEvent;
	
	@Inject
	@Prod
	@FlightRequestChanged(FlightRequestStatus.REJECTED)
	private Event<FlightRequestChangedEvent> flightRequestRejectedEvent;
	
	@Inject
	private IntegrationFacade facade;
	
	@Override
	public FlightRequest getFlightRequest(long id) {
		return repo.findById(id);
	}

	@Override
	public FlightRequest createFlightRequest(FlightRequest flightRequest) {
		sendEventByChangedStatus(flightRequest.getStatus());
		return repo.findById(repo.persist(flightRequest));
	}

	@Override
	public void updateFlightRequest(FlightRequest flightRequest) {
		sendEventByChangedStatus(flightRequest.getStatus());
		facade.sendFlightRequest(flightRequest);
		repo.update(flightRequest);
	}

	@Override
	public List<FlightRequest> getAllFlightRequests() {
		return repo.getAllFlightRequests();
	}

	@Override
	public List<FlightRequest> getFlightRequestsByStatus(Status status) {
		return repo.getFlightRequestsByStatus(status);
	}

	protected FlightRequestRepository getRepository() {
		return repo;
	}

	protected void setRepository(FlightRequestRepository repo) {
		this.repo = repo;
	}
	
	/**
	 * Send event FlightRequestChangedEvent depending on status
	 * @param status
	 */
	private void sendEventByChangedStatus(Status status) {
		FlightRequestChangedEvent event = new FlightRequestChangedEvent();
		
		if(status == Status.NEW) {
			flightRequestConfirmEvent.fire(event);
		} else if(status == Status.OFFER_SENT) {
			flightRequestSentEvent.fire(event);
		} else if(status == Status.REJECTED) {
			flightRequestRejectedEvent.fire(event);
		}
	}
	
	

}
