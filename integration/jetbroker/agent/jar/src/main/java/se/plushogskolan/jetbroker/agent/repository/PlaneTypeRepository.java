package se.plushogskolan.jetbroker.agent.repository;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.agent.domain.PlaneType;

@Local
public interface PlaneTypeRepository {

	List<PlaneType> getAllPlaneTypes();

	PlaneType getPlaneType(String code);

	void handlePlaneTypeChangedEvent();

}
