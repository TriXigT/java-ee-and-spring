package se.plushogskolan.jetbroker.agent.domain;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Set;

import javax.validation.ConstraintViolation;

import org.junit.Test;

import se.plushogskolan.jetbroker.TestFixture;

public class CustomerTest {
	
	@Test
	public void testEmptyConstructor() {
		Customer customer = new Customer();
		
		assertEquals("id 0", 0, customer.getId());
		assertNull("firstName null", customer.getFirstName());
		assertNull("lastname null", customer.getLastName());
		assertNull("email null", customer.getEmail());
		assertNull("company null", customer.getCompany());
	}
	
	@Test
	public void testNameConstructor() {
		Customer customer = new Customer("Bertil", "Holmstrom");
		
		assertEquals("id 0", 0, customer.getId());
		assertEquals("firstName set", "Bertil", customer.getFirstName());
		assertEquals("lastname set", "Holmstrom", customer.getLastName());
		assertNull("email null", customer.getEmail());
		assertNull("company null", customer.getCompany());
	}
	
	@Test
	public void testFullConstructor() {
		Customer customer = new Customer("Linnea","Amsen","a@msen.se","companiet");
		
		assertEquals("id 0", 0, customer.getId());
		assertEquals("firstName set", "Linnea", customer.getFirstName());
		assertEquals("lastname set", "Amsen", customer.getLastName());
		assertEquals("email set","a@msen.se", customer.getEmail());
		assertEquals("company set", "companiet", customer.getCompany());
	}
	
	@Test
	public void testValidationFirstNameBlank() {
		Customer customer = TestFixture.getValidCustomer();
		customer.setFirstName("");
		
		Set<ConstraintViolation<Customer>> violations = TestFixture.getValidator().validate(
				customer);
		
		TestFixture.assertPropertyIsInvalid("firstName", violations);	
	}
	
	@Test
	public void testValidationLastNameBlank() {
		Customer customer = TestFixture.getValidCustomer();
		customer.setLastName("");
		
		Set<ConstraintViolation<Customer>> violations = TestFixture.getValidator().validate(
				customer);
		
		TestFixture.assertPropertyIsInvalid("lastName", violations);	
	}
	
	@Test
	public void testValidationEmailInvalid() {
		Customer customer = TestFixture.getValidCustomer();
		customer.setEmail("hej");
		
		Set<ConstraintViolation<Customer>> violations = TestFixture.getValidator().validate(
				customer);
		
		TestFixture.assertPropertyIsInvalid("email", violations);	
	}
}

