package se.plushogskolan.jetbroker.order.repository.jpa;

import java.util.List;

import javax.persistence.Query;

import se.plushogskolan.jee.utils.repository.jpa.JpaRepository;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.Status;
import se.plushogskolan.jetbroker.order.repository.FlightRequestRepository;

public class JpaFlightRequestRepository extends JpaRepository<FlightRequest> implements FlightRequestRepository{

	@SuppressWarnings("unchecked")
	@Override
	public List<FlightRequest> getAllFlightRequests() {
		return em.createQuery("select f from FlightRequest f").getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FlightRequest> getFlightRequestsByStatus(Status status) {
		Query query =  em.createQuery("select f from FlightRequest f where f.status=?1");
		return query.setParameter(1, status).getResultList();
	}

}
