package se.plushogskolan.jetbroker.plane.service;

import java.util.logging.Logger;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.plane.integration.plane.PlaneIntegrationFacade;

@Singleton
public class FuelPriceServiceImpl implements FuelPriceService{

	@Inject
	private Logger log;
	
	@Inject
	@Prod
	private PlaneIntegrationFacade facade;
	
	private double fuelPrice = 5.2;
	
	@Lock(LockType.READ)
	@Override
	public double getFuelPrice() {
		log.fine(String.format("get fuelprice [%f]", fuelPrice));
		
		return fuelPrice;
	}

	/**
	 * Updates the fuelprice and calls @Prod PlaneIntegrationFacade
	 * to broadcast the update
	 */
	@Lock(LockType.WRITE)
	@Override
	public void updateFuelPrice(double fuelPrice) {
		log.fine(String.format("update fuelprice from [%f] to [%f]", this.fuelPrice, fuelPrice));
		
		facade.broadcastNewFuelPrice(fuelPrice);
		
		this.fuelPrice = fuelPrice;
	}

}
