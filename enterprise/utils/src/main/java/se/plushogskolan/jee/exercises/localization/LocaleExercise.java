package se.plushogskolan.jee.exercises.localization;

import java.math.RoundingMode;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.commons.lang.math.Fraction;

public class LocaleExercise {
	
	private ResourceBundle getResources(Locale locale) {
		return ResourceBundle.getBundle("localizationexercise", locale);
	}
	
	public String sayHello(Locale locale) {
		return getResources(locale).getString("exercise.hello");
	}
	
	public String sayName(String name, Locale locale) {
		ResourceBundle messages = getResources(locale);
		MessageFormat form = new MessageFormat(messages.getString("exercise.name.pattern"), locale);
		Object[] messageArguments = {messages.getString("exercise.hello"), name};
		
		return form.format(messageArguments);
	}
	
	public String saySomethingEnglish(Locale locale) {
		return getResources(locale).getString("exercise.onlyEnglish");
	}
	
	public String introduceYourself(int age, String town, String name, Locale locale) {
		ResourceBundle messages = getResources(locale);
		MessageFormat form = new MessageFormat(messages.getString("exercise.introduce.pattern"));
		Object[] messageArguments = {age, town, name};
		
		return form.format(messageArguments);
	}
	
	public String sayApointmentDate(Date date, Locale locale) {
		ResourceBundle messages = getResources(locale);
		MessageFormat form = new MessageFormat(messages.getString("exercise.date.pattern"), locale);
		Object[] messageArguments = {date};
		
		return form.format(messageArguments);
	}
	
	public String sayPrice(double price, Locale locale) {
		return NumberFormat.getCurrencyInstance(locale).format(price);
	}

	public String sayFractionDigits(double no) {
		return NumberFormat.getInstance().format(no);
	}	
}
