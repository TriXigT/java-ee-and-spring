package se.plushogskolan.jetbroker.agent.repository.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;
import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jee.utils.ehcache.CacheName;
import se.plushogskolan.jetbroker.agent.domain.PlaneType;
import se.plushogskolan.jetbroker.agent.integration.plane.PlaneFacade;
import se.plushogskolan.jetbroker.agent.repository.PlaneTypeRepository;

@Stateless
public class CachedPlaneTypeRepository implements PlaneTypeRepository {

	@Inject
	private Logger log;

	@Inject
	@Prod
	private PlaneFacade planeFacade;

	@Inject
	@CacheName(cacheName="planeTypeCache")
	private Cache cache;

	@SuppressWarnings("unchecked")
	@Override
	public List<PlaneType> getAllPlaneTypes() {
		Map<String, PlaneType> planeTypesMap = new HashMap<String, PlaneType>();

		if(cache.isKeyInCache("allPlaneTypes")) {
			log.fine("Get PlaneTypes from cache");
			
			planeTypesMap = (HashMap<String, PlaneType>) cache.get("allPlaneTypes").getObjectValue();
		} else {
			log.fine("No PlaneTypes in cache, get PlaneTypes from planeFacade");

			planeTypesMap = getPlaneTypeListAsMap(planeFacade.getAllPlaneTypes());

			log.fine("Put Map<(String) Code, PlaneType> with key \"allPlaneTypes\" in cache");

			cache.put(new Element("allPlaneTypes", planeTypesMap));
		}

		return new ArrayList<PlaneType>(planeTypesMap.values());
	}

	@Override
	public PlaneType getPlaneType(String code) {
		Element element = cache.get(code);

		if(element == null) {
			Map<String, PlaneType> planeTypesMap = getPlaneTypeListAsMap(getAllPlaneTypes());
			
			log.fine(String.format("No cached PlaneType found for code [%s], get PlaneType from cached Map", code));

			PlaneType planeType = planeTypesMap.get(code);

			if(planeType != null) {
				log.fine(String.format("Put retrieved PlaneType in cache with code [%s]", code));

				cache.put(new Element(code, planeType));

				return planeType;
			} else {
				log.warning(String.format("No PlaneType matching code [%s] found, will return null", code));
			}
		}

		return (PlaneType) element.getObjectValue();
	}
	
	private Map<String,PlaneType> getPlaneTypeListAsMap(List<PlaneType> planeTypeList) {
		Map<String, PlaneType> planeTypesMap = new HashMap<String, PlaneType>();
		
		log.fine("Put PlaneTypes from List into Map <Code, PlaneType>");
		
		for(PlaneType planeType : planeTypeList) {
			planeTypesMap.put(planeType.getCode(), planeType);
		}
		
		return planeTypesMap;
	}
	
	@Override
	public void handlePlaneTypeChangedEvent() {
		log.fine("Clear cache");

		cache.removeAll();
	}

}
