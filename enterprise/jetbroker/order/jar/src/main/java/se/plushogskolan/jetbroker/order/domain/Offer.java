package se.plushogskolan.jetbroker.order.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Min;

import se.plushogskolan.jee.utils.domain.IdHolder;

@Entity
public class Offer implements IdHolder{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	@Min(value=1)
	private long flightRequestId;
	
	@Min(value=1)
	private long planeId;
	@Min(value=0)
	private double price;
	
	public Offer() {
		
	}
	
	public Offer(long flightRequestId) {
		this(flightRequestId,0,0);
	}
	
	public Offer(long flightRequestId, long planeId, double price) {
		setFlightRequestId(flightRequestId);
		setPlaneId(planeId);
		setPrice(price);
	}
	
	@Override
	public long getId() {
		return id;
	}

	@Override
	public void setId(long id) {
		this.id = id;
	}

	public long getFlightRequestId() {
		return flightRequestId;
	}

	public void setFlightRequestId(long flightRequestId) {
		this.flightRequestId = flightRequestId;
	}

	public long getPlaneId() {
		return planeId;
	}

	public void setPlaneId(long planeId) {
		this.planeId = planeId;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

}
