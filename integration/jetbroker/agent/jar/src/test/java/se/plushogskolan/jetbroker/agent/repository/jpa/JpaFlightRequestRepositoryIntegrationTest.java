package se.plushogskolan.jetbroker.agent.repository.jpa;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.jboss.arquillian.junit.Arquillian;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.plushogskolan.jetbroker.agent.TestFixture;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.repository.FlightRequestRepository;
import se.plushogskolan.jetbroker.agent.services.CustomerService;

@RunWith(Arquillian.class)
public class JpaFlightRequestRepositoryIntegrationTest extends AbstractRepositoryTest<FlightRequest, FlightRequestRepository> {

	private static Logger log = Logger.getLogger(JpaFlightRequestRepositoryIntegrationTest.class.getName());

	@Inject
	FlightRequestRepository repository;

	@Inject
	CustomerService customerService;

	@Test
	public void testGetFlightRequestsForCustomer() throws Exception {

		Customer customer1 = customerService.createCustomer(TestFixture.getValidCustomer(0));
		Customer customer2 = customerService.createCustomer(TestFixture.getValidCustomer(0));

		long fr1 = repository.persist(TestFixture.getValidFlightRequest(customer1));
		long fr2 = repository.persist(TestFixture.getValidFlightRequest(customer2));

		List<FlightRequest> requestForCustomer1 = repository.getFlightRequestsForCustomer(customer1.getId());
		assertEquals("Requests for customer 1", 1, requestForCustomer1.size());
		assertEquals("FlightRequestID for customer 1", fr1, requestForCustomer1.get(0).getId());
		assertEquals("CustomerID for customer 1", customer1.getId(), requestForCustomer1.get(0).getCustomer().getId());

	}

	protected FlightRequestRepository getRepository() {

		return repository;
	}

	protected FlightRequest getEntity1() throws Exception {
		Customer customer = customerService.createCustomer(TestFixture.getValidCustomer(0));
		FlightRequest request = TestFixture.getValidFlightRequest(customer);
		return request;
	}

	protected FlightRequest getEntity2() throws Exception {
		Customer customer = customerService.createCustomer(TestFixture.getValidCustomer(0));
		FlightRequest request = TestFixture.getValidFlightRequest(customer);
		return request;
	}

}
