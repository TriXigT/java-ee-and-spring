package se.plushogskolan.jetbroker.agent.rest.exercises.xml.xmlhandlers;

import java.io.StringWriter;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;


public class XsltTransformer {

	public static String transformUsingXslt(String xmlFile, String xlsFile) throws Exception {
		StreamSource xlsStream = new StreamSource(SaxParser.class.getResourceAsStream(xlsFile));
		StreamSource xmlStream = new StreamSource(SaxParser.class.getResourceAsStream(xmlFile));
		
		Transformer transformer = TransformerFactory.newInstance().newTransformer(xlsStream);
		
		StringWriter sw = new StringWriter();
		StreamResult result = new StreamResult(sw);
		
		transformer.transform(xmlStream, result);
		
		System.out.println(sw.toString());
		return sw.toString();

	}

}
