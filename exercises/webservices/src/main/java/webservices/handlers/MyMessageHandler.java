package webservices.handlers;

import java.io.ByteArrayOutputStream;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

/**
 * A message handler that can listen to outgoing and incoming message.
 * 
 * By using the callbacks in this handler, we can access various parts of the soap document.
 * 
 * This class should be implemented to listen to soap messages and transform them to xml. Outgoing soap messages should
 * be stored in soapBodyRequestXml, and incoming soap messages should be stored in soapBodyResponseXml.
 */
public class MyMessageHandler implements SOAPHandler<SOAPMessageContext> {

	private String soapBodyRequestXml;
	private String soapBodyResponseXml;

	public String getSoapBodyRequestXml() {
		return soapBodyRequestXml;
	}

	public String getSoapBodyResponseXml() {
		return soapBodyResponseXml;
	}

	@Override
	public void close(MessageContext arg0) {
		System.out.println("MyMessageHandler: close");

	}

	@Override
	public boolean handleFault(SOAPMessageContext arg0) {
		System.out.println("MyMessageHandler: handleFault");
		return true;
	}

	@Override
	public boolean handleMessage(SOAPMessageContext context) {
		System.out.println("MyMessageHandler: handleMessage");

		String soapMessageString;

		try {
			soapMessageString = convertSoapMessageToString(context.getMessage());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		if((Boolean) (context.get(SOAPMessageContext.MESSAGE_OUTBOUND_PROPERTY))) {
			soapBodyRequestXml = soapMessageString;
		} else {
			soapBodyResponseXml = soapMessageString;
		}
		
		return true;
	}

	@Override
	public Set<QName> getHeaders() {
		System.out.println("MyMessageHandler: getHeaders");
		return null;
	}

	/**
	 * A utility method to transform a SOAPMessage to string
	 */
	private String convertSoapMessageToString(SOAPMessage msg) throws Exception {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		msg.writeTo(baos);
		return baos.toString();
	}

}
