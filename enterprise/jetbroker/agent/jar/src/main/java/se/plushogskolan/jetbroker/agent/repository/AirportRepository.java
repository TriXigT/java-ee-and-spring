package se.plushogskolan.jetbroker.agent.repository;

import java.util.List;

import se.plushogskolan.jee.utils.repository.BaseRepository;
import se.plushogskolan.jetbroker.agent.domain.Airport;

public interface AirportRepository{
	Airport getAirportByCode(String code);
	
	List<Airport> getAllAirports();
}
