package se.plushogskolan.jetbroker.order.util;

import java.util.logging.Logger;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.enterprise.event.Observes;

import se.plushogskolan.jee.utils.annotation.Prod;
import se.plushogskolan.jetbroker.order.integration.annotation.FlightRequestChanged;
import se.plushogskolan.jetbroker.order.integration.annotation.FlightRequestChanged.FlightRequestStatus;
import se.plushogskolan.jetbroker.order.integration.event.FlightRequestChangedEvent;

@Singleton
public class FlightRequestLogger {
	private Logger log = Logger.getLogger(FlightRequestLogger.class.getName());
	private int confirmationCount;
	private int offerCount;
	private int rejectionCount;
	
	public void receiveConfirmation(
			@Observes @Prod @FlightRequestChanged(FlightRequestStatus.CONFIRMED) FlightRequestChangedEvent event) {
		updateData(FlightRequestStatus.CONFIRMED);
	}
	
	public void receiveOfferReceived(
			@Observes @Prod @FlightRequestChanged(FlightRequestStatus.OFFER_SENT) FlightRequestChangedEvent event) {
		updateData(FlightRequestStatus.OFFER_SENT);
	}
	
	public void receiveRejection(
			@Observes @Prod @FlightRequestChanged(FlightRequestStatus.REJECTED) FlightRequestChangedEvent event) {
		updateData(FlightRequestStatus.REJECTED);
	}
	
	@Lock(LockType.WRITE)
	private void updateData(FlightRequestStatus status) {
		switch(status) {
		case CONFIRMED:
			++confirmationCount;
			break;
		case OFFER_SENT:
			++offerCount;
			break;
		case REJECTED:
			++rejectionCount;
			break;
		default: 
			break;
		}
		
		log.fine("***FlightRequestLogger***");
		log.fine(String.format("No flight request confirmations: %d", confirmationCount));
		log.fine(String.format("No flight request offers sent: %d", offerCount));
		log.fine(String.format("No flight request rejections: %d", rejectionCount));
		log.fine("*************************");
	}
}
