package se.plushogskolan.jetbroker.agent.domain.request;

/**
 * The different statuses that a flight request might have.
 * 
 */
public enum FlightRequestStatus {

	CREATED("Created"), REQUEST_CONFIRMED("Request confirmed"), OFFER_RECEIVED("Offer recieved"), REJECTED("Rejected"), DECIDED(
			"Decided");

	private String niceName;

	private FlightRequestStatus(String niceName) {
		this.niceName = niceName;
	}

	public String getNiceName() {
		return niceName;
	}

}
