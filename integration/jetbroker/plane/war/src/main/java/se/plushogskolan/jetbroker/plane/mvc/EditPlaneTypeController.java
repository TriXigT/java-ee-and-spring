package se.plushogskolan.jetbroker.plane.mvc;

import java.util.logging.Logger;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.plane.domain.PlaneType;
import se.plushogskolan.jetbroker.plane.service.PlaneService;

/**
 * This controller sets form values directly to a domain object in the
 * EditPlaneTypeBean. This avoid duplicated variables in both the bean and the
 * domain object. However, there is a risk involved. If the domain object has 7
 * variables, but the form only sets 5 values, the other 2 variables might be
 * lost. Therefore we don´t save the domain object in the bean directly. Instead
 * we take the values from the domain object coming from the form, and transfers
 * the values to a fresh domain object from the database.
 * 
 */
@Controller
@RequestMapping("/editPlaneType/{id}.html")
public class EditPlaneTypeController {

	Logger log = Logger.getLogger(EditPlaneTypeController.class.getName());

	@Autowired
	private PlaneService planeService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView index(@PathVariable long id) {

		log.fine("Edit plane type, id=" + id);

		EditPlaneTypeBean bean = new EditPlaneTypeBean();
		boolean isNew = id <= 0;

		if (isNew) {
			PlaneType planeType = new PlaneType();
			bean.setPlaneType(planeType);
		} else {
			PlaneType planeType = getPlaneService().getPlaneType(id);
			bean.setPlaneType(planeType);
		}

		ModelAndView mav = new ModelAndView("editPlaneType");
		mav.addObject("editPlaneTypeBean", bean);
		return mav;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView handleSubmit(@Valid EditPlaneTypeBean bean, BindingResult errors) {

		if (errors.hasErrors()) {
			ModelAndView mav = new ModelAndView("editPlaneType");
			mav.addObject("editPlaneTypeBean", bean);
			return mav;
		}

		PlaneType planeType = bean.getPlaneType();

		if (planeType.getId() > 0) {
			/**
			 * Copy the updated values to a new PlaneType object taken directly
			 * from the database
			 */
			PlaneType dbPlaneType = getPlaneService().getPlaneType(bean.getPlaneType().getId());
			copyPlaneTypeValues(planeType, dbPlaneType);
			getPlaneService().updatePlaneType(dbPlaneType);
		} else {
			getPlaneService().createPlaneType(planeType);
		}

		return new ModelAndView("redirect:/index.html");
	}

	protected void copyPlaneTypeValues(PlaneType planeType, PlaneType dbPlaneType) {
		dbPlaneType.setId(planeType.getId());
		dbPlaneType.setCode(planeType.getCode());
		dbPlaneType.setName(planeType.getName());
		dbPlaneType.setMaxNoOfPassengers(planeType.getMaxNoOfPassengers());
		dbPlaneType.setMaxRangeKm(planeType.getMaxRangeKm());
		dbPlaneType.setMaxSpeedKmH(planeType.getMaxSpeedKmH());
		dbPlaneType.setFuelConsumptionPerKm(planeType.getFuelConsumptionPerKm());
	}

	public PlaneService getPlaneService() {
		return planeService;
	}

	public void setPlaneService(PlaneService planeService) {
		this.planeService = planeService;
	}

}
