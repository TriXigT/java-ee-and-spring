package se.plushogskolan.jetbroker.agent.repository.jpa;

import java.util.List;



import javax.persistence.Query;

import se.plushogskolan.jee.utils.repository.jpa.JpaRepository;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.repository.CustomerRepository;

public class JpaCustomerRepository extends JpaRepository<Customer> implements CustomerRepository{

	@Override
	public List<Customer> getAllCustomers() {
		return em.createQuery("select c from Customer c ", Customer.class).getResultList();
	}

	@Override
	public Customer getCustomerByEmail(String email) {
		Query query =  em.createQuery("select c from Customer c where c.email = :email");
		return (Customer) query.setParameter("email", email).getSingleResult();
	}
}
