package se.plushogskolan.jetbroker.plane.service;

import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.plane.domain.Airport;
import se.plushogskolan.jetbroker.plane.integration.plane.PlaneIntegrationFacade;
import se.plushogskolan.jetbroker.plane.repository.AirportRepository;

@Stateless
public class AirportServiceImpl implements AirportService {

	@Inject
	private Logger log;

	@Inject
	private AirportRepository airportRepo;
	
	@Inject
	@Prod
	private PlaneIntegrationFacade facade;

	@Override
	public Airport getAirport(long id) {
		log.fine(String.format("get airport by id [%d]", id));
		
		return getAirportRepo().findById(id);
	}

	@Override
	public Airport getAirportByCode(String code) {
		log.fine(String.format("get airport by code [%s]", code));
		
		return getAirportRepo().getAirportByCode(code);
	}

	@Override
	public void updateAirport(Airport airport) {
		log.fine(String.format("update airport: %s", airport.toString()));
		
		getAirportRepo().update(airport);
		
		facade.broadcastAirportsChanged();
	}

	@Override
	public Airport createAirport(Airport airport) {
		log.fine(String.format("create airport: %s", airport.toString()));
		
		long id = getAirportRepo().persist(airport);
		
		facade.broadcastAirportsChanged();
		
		return getAirport(id);
	}

	@Override
	public List<Airport> getAllAirports() {
		log.fine("get all airports");
		
		List<Airport> airports = getAirportRepo().getAllAirports();
		Collections.sort(airports);
		return airports;
	}

	@Override
	public void deleteAirport(long id) {
		Airport airport = getAirport(id);
		
		log.fine(String.format("delete airport: %s", airport.toString()));
		
		getAirportRepo().remove(airport);
		facade.broadcastAirportsChanged();
	}

	@Override
	public void updateAirportsFromWebService() {
		// Implement
	}

	public AirportRepository getAirportRepo() {
		return airportRepo;
	}

	public void setAirportRepo(AirportRepository airportRepo) {
		this.airportRepo = airportRepo;
	}

}
