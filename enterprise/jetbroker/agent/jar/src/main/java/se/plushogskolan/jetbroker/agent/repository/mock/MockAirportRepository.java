package se.plushogskolan.jetbroker.agent.repository.mock;

import java.util.ArrayList;
import java.util.List;

import se.plushogskolan.jee.utils.repository.mock.MockAbstractRepository;
import se.plushogskolan.jetbroker.agent.domain.Airport;
import se.plushogskolan.jetbroker.agent.repository.AirportRepository;

public class MockAirportRepository implements AirportRepository{
	
	private List<Airport> entities;
	
	public MockAirportRepository() {
		entities = new ArrayList<Airport>();
		
		Airport airport = new Airport("GBG","Gothenburg");
		Airport airport2 = new Airport("STM","Stockholm");
		
		entities.add(airport);
		entities.add(airport2);
	}

	protected List<Airport> getMockList() {
		return entities;
	}

	@Override
	public Airport getAirportByCode(String code) {
		for(Airport temp : getMockList()) {
			if(temp.getCode().equals(code)) {
				return temp;
			}
		}
		
		return null;
	}

	@Override
	public List<Airport> getAllAirports() {
		return getMockList();
	}
}
