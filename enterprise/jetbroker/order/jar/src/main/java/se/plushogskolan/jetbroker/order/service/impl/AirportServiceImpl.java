package se.plushogskolan.jetbroker.order.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.repository.AirportRepository;
import se.plushogskolan.jetbroker.order.service.AirportService;
@Stateless
public class AirportServiceImpl implements AirportService{

	@Inject
	AirportRepository repo;

	@Override
	public Airport getAirportByCode(String code) {
		return repo.getAirportByCode(code);
	}
	
	@Override
	public List<Airport> getAllAirports() {
		return repo.getAllAirports();
	}

	protected AirportRepository getRepository() {
		return repo;
	}

	protected void setRepository(AirportRepository repo) {
		this.repo = repo;
	}

}
