package se.plushogskolan.jetbroker.agent.rest.flightrequest;

import java.util.logging.Logger;

import javax.inject.Inject;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.rest.OkOrErrorResponse;
import se.plushogskolan.jetbroker.agent.rest.flightrequest.model.CreateFlightRequestRequest;
import se.plushogskolan.jetbroker.agent.rest.flightrequest.model.CreateFlightRequestResponse;
import se.plushogskolan.jetbroker.agent.rest.flightrequest.model.GetFlightRequestResponse;
import se.plushogskolan.jetbroker.agent.services.CustomerService;
import se.plushogskolan.jetbroker.agent.services.FlightRequestService;

@Controller
public class FlightRequestsController {

	Logger log = Logger.getLogger(FlightRequestsController.class.getName());

	@Inject
	private FlightRequestService flightRequestService;

	@Inject
	private CustomerService customerService;

	@RequestMapping(value = "/createFlightRequest", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public CreateFlightRequestResponse createFlightRequest(@RequestBody CreateFlightRequestRequest request)
			throws Exception {
		log.fine("createFlightRequest: " + request);
		Customer customer = customerService.getCustomer(request.getCustomerId());
		FlightRequest fr = new FlightRequest();
		fr.setCustomer(customer);
		fr.setArrivalAirportCode(request.getArrivalAirportCode());
		fr.setDepartureAirportCode(request.getDepartureAirportCode());
		fr.setNoOfPassengers(request.getNoOfPassengers());
		DateTimeFormatter dateFormatter = DateTimeFormat.forPattern(JmsConstants.DATE_FORMAT);
		fr.setDepartureTime(DateTime.parse(request.getDate(), dateFormatter));

		fr = flightRequestService.createFlightRequest(fr);

		return new CreateFlightRequestResponse(fr);

	}

	@RequestMapping(value = "/getFlightRequest/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public GetFlightRequestResponse getFlightRequest(@PathVariable long id) throws Exception {
		log.fine("getFlightRequest: " + id);

		FlightRequest flightRequest = getFlightRequestService().getFlightRequest(id);

		return new GetFlightRequestResponse(flightRequest);

	}

	@RequestMapping(value = "/deleteFlightRequest/{id}", method = RequestMethod.DELETE, produces = "application/json")
	@ResponseBody
	public OkOrErrorResponse deleteFlightRequest(@PathVariable long id) throws Exception {
		log.fine("deleteFlightRequest: " + id);

		getFlightRequestService().deleteFlightRequest(id);

		return OkOrErrorResponse.getOkResponse();

	}

	public FlightRequestService getFlightRequestService() {
		return flightRequestService;
	}

	public void setFlightRequestService(FlightRequestService flightRequestService) {
		this.flightRequestService = flightRequestService;
	}

}
