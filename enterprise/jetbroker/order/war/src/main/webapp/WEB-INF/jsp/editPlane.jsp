<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
<head>
<head>
<link href="<%=request.getContextPath()%>/style/common.css" type="text/css" rel="stylesheet" />
	<link href="<%=request.getContextPath()%>/style/theme_stylish.css" type="text/css" rel="stylesheet" />

</head>
<body>
	<div class="container">
		<jsp:include page="header.jsp" />
		<div class="content">
			<div class="content_container" id="generic_container">
				<img src="<%=request.getContextPath()%>/images/flightrequest.png"/>
					<h2>
						<c:choose>
							<c:when test="${editPlaneBean.plane.id > 0}">
								<spring:message code="editPlane.plane.edit.title"/>
							</c:when>
							<c:otherwise>
								<spring:message code="editPlane.plane.add.title"/>
							</c:otherwise>
						</c:choose>
					</h2>
				<hr>
				<form:form id="generic_form" commandName="editPlaneBean">
					<c:if test="${editPlaneBean.plane.id > 0}">
						<label for="id_field"><spring:message code="global.id"/></label>
						<span id="id_field">${editPlaneBean.plane.id}</span>
						<span class=clear></span>
					</c:if>

					<form:hidden path="plane.id" value="${editPlaneBean.plane.id}"/>
					
					<label for="planeType"><spring:message code="global.plane.planeType"/></label>
					<form:select id="planeType" class="formElement float_left" path="plane.planeTypeCode">
							<form:option value="" label=""/>
							<form:options items="${planeTypes}" itemValue="code" itemLabel="niceName"/>	
					</form:select>
					<form:errors path="plane.planeTypeCode" class="errors" />
					
					<span class=clear></span>
					<label for="description"><spring:message code="global.plane.description"/></label>
					<form:textarea id="description" class="formElement float_left" path="plane.description"/>
					<form:errors path="plane.description" class="errors"/>
					<span class=clear></span>
					<span class="space float_left clear"></span>
					<input type="submit" value="<spring:message code="global.submit"/>"/>
					<a id="generic_button" class="button_style" href="<%=request.getContextPath()%>/index.html"><spring:message code="global.cancel"/></a>
				</form:form>
			</div>
		</div>
	</div>
</body>

</html>