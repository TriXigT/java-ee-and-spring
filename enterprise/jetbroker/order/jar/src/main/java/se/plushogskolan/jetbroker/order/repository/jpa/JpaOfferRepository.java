package se.plushogskolan.jetbroker.order.repository.jpa;

import java.util.List;

import se.plushogskolan.jee.utils.repository.jpa.JpaRepository;
import se.plushogskolan.jetbroker.order.domain.Offer;
import se.plushogskolan.jetbroker.order.repository.OfferRepository;

public class JpaOfferRepository extends JpaRepository<Offer> implements OfferRepository{

	@SuppressWarnings("unchecked")
	@Override
	public List<Offer> getAllOffers() {
		return em.createQuery("select o from Offer o").getResultList();
	}
	
}
