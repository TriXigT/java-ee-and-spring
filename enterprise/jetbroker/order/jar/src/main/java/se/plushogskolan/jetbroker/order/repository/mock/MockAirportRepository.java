package se.plushogskolan.jetbroker.order.repository.mock;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.repository.AirportRepository;

@Stateless
public class MockAirportRepository implements AirportRepository{
	
	private List<Airport> entities;
	
	public MockAirportRepository() {
		entities = new ArrayList<Airport>();
		
		Airport airport = new Airport("GBG","Gothenburg", 52.82, 12);
		Airport airport2 = new Airport("STM","Stockholm", 59.65, 17.91);
		
		entities.add(airport);
		entities.add(airport2);
	}

	protected List<Airport> getMockList() {
		return entities;
	}

	@Override
	public Airport getAirportByCode(String code) {
		for(Airport temp : getMockList()) {
			if(temp.getCode().equals(code)) {
				return temp;
			}
		}
		
		return null;
	}

	@Override
	public List<Airport> getAllAirports() {
		return getMockList();
	}
}
