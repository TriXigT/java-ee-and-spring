package se.plushogskolan.jetbroker.agent.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.repository.CustomerRepository;
import se.plushogskolan.jetbroker.agent.service.CustomerService;

@Stateless
public class CustomerServiceImpl implements CustomerService {

	@Inject
	private CustomerRepository repo;

	@Override
	public Customer getCustomer(long id) {
		return repo.findById(id);
	}

	@Override
	public Customer createCustomer(Customer customer) {
		repo.persist(customer);
		return repo.findById(customer.getId());
	}

	@Override
	public void updateCustomer(Customer customer) {
		repo.update(customer);
	}

	@Override
	public List<Customer> getAllCustomers() {
		return repo.getAllCustomers();
	}

	@Override
	public Customer getCustomerByEmail(String email) {
		return repo.getCustomerByEmail(email);
	}

}