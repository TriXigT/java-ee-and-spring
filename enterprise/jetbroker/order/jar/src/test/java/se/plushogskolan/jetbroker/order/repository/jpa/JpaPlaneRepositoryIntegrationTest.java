package se.plushogskolan.jetbroker.order.repository.jpa;

import static org.junit.Assert.assertEquals;

import javax.inject.Inject;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.repository.AbstractRepositoryTest;
import se.plushogskolan.jetbroker.order.repository.jpa.JpaPlaneRepository;

@RunWith(Arquillian.class)
@Transactional(TransactionMode.ROLLBACK)
public class JpaPlaneRepositoryIntegrationTest extends AbstractRepositoryTest<Plane, JpaPlaneRepository>{

	@Inject
	JpaPlaneRepository repo;

	@Override
	protected JpaPlaneRepository getRepository() {
		return repo;
	}
	
	@Test
	public void getAllPlanesTest() {
		repo.persist(getEntity1());
		repo.persist(getEntity2());
		
		assertEquals("get all planes", 2, repo.getAllPlanes().size());
	}
	
	@Test(expected = Exception.class)
	public void validationCodeTest() {
		// Create plane with empty code, should throw new Exception
		Plane plane = TestFixture.getValidPlane("", "Hej");
		repo.persist(plane);
	}
	
	@Test(expected = Exception.class)
	public void validationDescriptionTest() {

		// Create plane with empty description, should throw new Exception
		Plane plane = TestFixture.getValidPlane("AIR", "");
		repo.persist(plane);
	}

	@Override
	protected Plane getEntity1() {
		return TestFixture.getValidPlane("AIR", "Funny plane bus");
	}

	@Override
	protected Plane getEntity2() {
		return TestFixture.getValidPlane("DOGE", "much plain");
	}

}
