package se.plushogskolan.jetbroker.order.rest.plane;

import java.util.logging.Logger;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import se.plushogskolan.jetbroker.order.rest.OkOrErrorResponse;
import se.plushogskolan.jetbroker.order.rest.plane.model.CreatePlaneRequest;
import se.plushogskolan.jetbroker.order.rest.plane.model.CreatePlaneResponse;
import se.plushogskolan.jetbroker.order.rest.plane.model.GetPlaneResponse;
import se.plushogskolan.jetbroker.order.service.PlaneService;

@Controller
public class PlaneController {
	Logger log = Logger.getLogger(PlaneController.class.getName());
	
	@Inject
	PlaneService planeService;
	
	@RequestMapping(value = "/createPlane", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public CreatePlaneResponse createPlane(@RequestBody CreatePlaneRequest request) throws Exception {
		log.fine("createPlane: " + request);
		return new CreatePlaneResponse(planeService.createPlane(request.buildPlane()).getId());
	}

	@RequestMapping(value = "/getPlane/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public GetPlaneResponse getPlane(@PathVariable long id) {
		log.fine(String.format("getPlane id:%d", id));
		return new GetPlaneResponse(planeService.getPlane(id));
	}

	@RequestMapping(value = "/deletePlane/{id}", method = RequestMethod.DELETE, produces = "application/json")
	@ResponseBody
	public OkOrErrorResponse deletePlane(@PathVariable long id) {
		log.fine(String.format("deletePlane id:%d", id));
		try {
			planeService.deletePlane(id);
		} catch(Exception e) {
			return OkOrErrorResponse.getErrorResponse(e.getMessage());
		}
		
		return OkOrErrorResponse.getOkResponse();
	}
}
