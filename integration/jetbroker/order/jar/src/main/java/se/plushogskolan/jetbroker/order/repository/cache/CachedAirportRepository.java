package se.plushogskolan.jetbroker.order.repository.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.integration.plane.PlaneIntegrationFacade;
import se.plushogskolan.jetbroker.order.repository.AirportRepository;

@Stateless
public class CachedAirportRepository implements AirportRepository {

	@Inject
	private Logger log;
	
	@Inject
	@Prod
	private PlaneIntegrationFacade planeIntegrationFacade;

	@Override
	public Airport getAirport(String code) {
		List<Airport> airports = planeIntegrationFacade.getAllAirports();

		for (Airport airport : airports) {
			if (airport.getCode().equalsIgnoreCase(code)) {
				return airport;
			}
		}
		return null;
	}

	@Override
	public List<Airport> getAllAirports() {
		return planeIntegrationFacade.getAllAirports();
	}
	
	private Map<String,Airport> getAirportsFromFacadeAsMap() {
		Map<String, Airport> airportsMap = new HashMap<String, Airport>();

		log.fine("Get Airports from PlaneFacade");

		for(Airport planeType : planeIntegrationFacade.getAllAirports()) {
			airportsMap.put(planeType.getCode(), planeType);
		}

		return airportsMap;
	}

}
