package se.plushogskolan.jetbroker.agent.service;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.agent.domain.Airport;

@Local
public interface AirportService {
	Airport getAirportByCode(String code);

	List<Airport> getAllAirports();

}
