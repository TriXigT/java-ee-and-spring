package se.plushogskolan.jetbroker.order.mvc;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.Offer;
import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.service.FlightRequestService;
import se.plushogskolan.jetbroker.order.service.FuelPriceService;
import se.plushogskolan.jetbroker.order.service.PlaneService;

@Controller
public class EditFlightRequestController {

	@Inject
	private FlightRequestService flightRequestService;

	@Inject
	private PlaneService planeService;
	
	@Inject
	private FuelPriceService fuelPriceService;

	@RequestMapping(value = "/editFlightRequest/{id}.html", method = RequestMethod.GET)
	public ModelAndView index(@PathVariable long id) {

		EditFlightRequestBean bean = new EditFlightRequestBean();
		FlightRequest flightRequest = getFlightRequestService().getFlightRequest(id);

		bean.copyFlightRequestValuesToBean(flightRequest);

		ModelAndView mav = new ModelAndView("editFlightRequest");
		mav.addObject("editFlightRequestBean", bean);
		addReferenceDataToModel(mav, flightRequest);

		return mav;
	}

	@RequestMapping(value = "/editFlightRequest/{id}.html", method = RequestMethod.POST)
	public ModelAndView handleSubmit(@PathVariable long id, @Valid EditFlightRequestBean bean, BindingResult errors)
			throws Exception {

		FlightRequest flightRequest = getFlightRequestService().getFlightRequest(id);

		if (errors.hasErrors()) {
			ModelAndView mav = new ModelAndView("editFlightRequest");
			mav.addObject("editFlightRequestBean", bean);
			addReferenceDataToModel(mav, flightRequest);
			return mav;
		}

		Offer offer = null;
		if (flightRequest.getOffer() != null) {
			offer = flightRequest.getOffer();
		} else {
			offer = new Offer();
		}
		offer.setPrice(bean.getPrice());
		Plane plane = getPlaneService().getPlane(bean.getPlaneId());
		offer.setPlane(plane);
		getFlightRequestService().handleUpdatedOffer(id, offer);

		return new ModelAndView("redirect:/index.html");
	}

	protected void addReferenceDataToModel(ModelAndView mav, FlightRequest flightRequest) {

		Airport departureAirport = getPlaneService().getAirport(flightRequest.getDepartureAirportCode());
		Airport arrivalAirport = getPlaneService().getAirport(flightRequest.getArrivalAirportCode());
		double fuelCost = fuelPriceService.getFuelPrice();

		// Plane wrappers
		List<Plane> planes = getPlaneService().getAllPlanes();
		List<PlaneWrapper> planeWrappers = new ArrayList<PlaneWrapper>();
		for (Plane plane : planes) {
			PlaneType planeType = getPlaneService().getPlaneType(plane.getPlaneTypeCode());
			PlaneWrapper wrapper = new PlaneWrapper(plane, planeType, flightRequest.getDistanceKm(), fuelCost);
			planeWrappers.add(wrapper);
		}

		mav.addObject("departureAirport", departureAirport);
		mav.addObject("arrivalAirport", arrivalAirport);
		mav.addObject("planeWrappers", planeWrappers);
		mav.addObject("flightRequest", flightRequest);
		mav.addObject("fuelCost", fuelCost);
	}

	@RequestMapping("/editFlightRequest/reject/{id}.html")
	public ModelAndView reject(@PathVariable long id) throws Exception {

		getFlightRequestService().rejectFlightRequest(id);

		return new ModelAndView("redirect:/index.html");

	}

	public FlightRequestService getFlightRequestService() {
		return flightRequestService;
	}

	public void setFlightRequestService(FlightRequestService service) {
		this.flightRequestService = service;
	}

	public PlaneService getPlaneService() {
		return planeService;
	}

	public void setPlaneService(PlaneService planeService) {
		this.planeService = planeService;
	}

	public static class PlaneWrapper {

		private Plane plane;
		private PlaneType planeType;
		private int fuelCost;
		private int travelTimeMin;

		public PlaneWrapper(Plane plane, PlaneType planeType, int distance, double fuelCost) {
			setPlane(plane);
			setPlaneType(planeType);
			setFuelCost((int) (distance * fuelCost * planeType.getFuelConsumptionPerKm()));

			int kmPerMin = planeType.getMaxSpeedKmH() / 60;
			setTravelTimeMin(distance / kmPerMin);

		}

		public Plane getPlane() {
			return plane;
		}

		public void setPlane(Plane plane) {
			this.plane = plane;
		}

		public PlaneType getPlaneType() {
			return planeType;
		}

		public void setPlaneType(PlaneType planeType) {
			this.planeType = planeType;
		}

		public int getFuelCost() {
			return fuelCost;
		}

		public void setFuelCost(int fuelCost) {
			this.fuelCost = fuelCost;
		}

		public int getTravelTimeMin() {
			return travelTimeMin;
		}

		public void setTravelTimeMin(int travelTimeMin) {
			this.travelTimeMin = travelTimeMin;
		}

	}

}
