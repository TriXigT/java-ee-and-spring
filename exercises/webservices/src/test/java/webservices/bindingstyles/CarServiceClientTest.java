package webservices.bindingstyles;

import static org.junit.Assert.*;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Endpoint;
import javax.xml.ws.Service;
import javax.xml.ws.handler.HandlerResolver;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import webservices.bindingstyles.document.CarServiceDocumentStyle;
import webservices.bindingstyles.document.CarServiceDocumentStyleImpl;
import webservices.bindingstyles.rpc.CarServiceRpcStyle;
import webservices.bindingstyles.rpc.CarServiceRpcStyleImpl;
import webservices.handlers.MyHandlerResolver;
import webservices.handlers.MyMessageHandler;

/**
 * This test tests the creation of two almost identical web services. But one uses RPC style and one uses document
 * style.
 * 
 * There are some slight differences in RPC and document style. In order to see the SOAP messages we implement handlers
 * that can intercept those messages.
 * 
 */
public class CarServiceClientTest {

	Endpoint rpcEndpoint = null;
	Endpoint documentEndpoint = null;
	RegNumber regNumber;

	@Before
	public void setup() {
		regNumber = new RegNumber("ABC", "123");
	}

	@After
	public void cleanup() {

		if (rpcEndpoint != null) {
			rpcEndpoint.stop();
		}
		if (documentEndpoint != null) {
			documentEndpoint.stop();
		}
	}

	/**
	 * Test that a RPC web service can be started and executed
	 */
	@Test
	public void testRpcCarService() throws Exception {

		rpcEndpoint = startRpcEndpoint();
		CarService rpcCarService = createRpcCarService(null);
		Car car = rpcCarService.getCar(regNumber);
		System.out.println("RPC car: " + car);
		assertEquals("RegNumber", regNumber, car.getRegNo());

	}

	/**
	 * Test that a document web service can be started and executed
	 */
	@Test
	public void testDocumentCarService() throws Exception {

		documentEndpoint = startDocumentEndpoint();
		CarService documentCarService = createDocumentCarService(null);
		Car car = documentCarService.getCar(regNumber);
		System.out.println("Doc car: " + car);
		assertEquals("RegNumber", regNumber, car.getRegNo());
	}

	/**
	 * Test that a RPC web service can be started and executed.
	 * 
	 * This time we add a message handler to listen to the SOAP communication.
	 */
	@Test
	public void testRpcCarService_messageHandler() throws Exception {

		MyMessageHandler messageHandler = new MyMessageHandler();
		MyHandlerResolver handlerResolver = new MyHandlerResolver(messageHandler);

		rpcEndpoint = startRpcEndpoint();
		CarService rpcCarService = createRpcCarService(handlerResolver);
		rpcCarService.getCar(regNumber);

		String requestXml = messageHandler.getSoapBodyRequestXml();
		String responseXml = messageHandler.getSoapBodyResponseXml();

		System.out.println("RPC Request: " + requestXml);
		System.out.println("RPC Response: " + responseXml);

		assertTrue("Request contains SOAP", requestXml.contains("<letters>ABC</letters>"));
		assertTrue("Response contains SOAP", responseXml.contains("<brand>Volvo</brand>"));

	}

	/**
	 * Test that a document web service can be started and executed.
	 * 
	 * This time we add a message handler to listen to the SOAP communication.
	 */
	@Test
	public void testDocumentCarService_messageHandler() throws Exception {
		MyMessageHandler messageHandler = new MyMessageHandler();
		MyHandlerResolver handlerResolver = new MyHandlerResolver(messageHandler);

		documentEndpoint = startDocumentEndpoint();
		CarService documentCarService = createDocumentCarService(handlerResolver);

		documentCarService.getCar(regNumber);

		String requestXml = messageHandler.getSoapBodyRequestXml();
		String responseXml = messageHandler.getSoapBodyResponseXml();

		System.out.println("Document Request: " + requestXml);
		System.out.println("Document Response: " + responseXml);

		assertTrue("Request contains SOAP", requestXml.contains("<letters>ABC</letters>"));
		assertTrue("Response contains SOAP", responseXml.contains("<brand>Volvo</brand>"));
	}

	public Endpoint startRpcEndpoint() {
		return Endpoint.publish("http://localhost:8180/ws/carServiceRpcStyle", new CarServiceRpcStyleImpl());
	}

	public Endpoint startDocumentEndpoint() {
		return Endpoint.publish("http://localhost:8180/ws/carServiceDocumentStyle", new CarServiceDocumentStyleImpl());
	}

	/**
	 * Return a CarService backed by the RCP web service.
	 * 
	 * Only set handlerResolver if it is not null
	 */
	public CarService createRpcCarService(HandlerResolver handlerResolver) throws Exception {
		URL url = new URL("http://localhost:8180/ws/carServiceRpcStyle");
		QName qName = new QName("http://rpc.bindingstyles.webservices/","CarServiceRpcStyleImplService");
		Service service = Service.create(url, qName);

		if(handlerResolver != null) {
			service.setHandlerResolver(handlerResolver);
		}

		return service.getPort(CarServiceRpcStyle.class);
	}

	/**
	 * Return a CarService backed by the document web service.
	 * 
	 * Only set handlerResolver if it is not null
	 */
	public CarService createDocumentCarService(HandlerResolver handlerResolver) throws Exception {
		URL url = new URL("http://localhost:8180/ws/carServiceDocumentStyle");
		QName qName = new QName("http://document.bindingstyles.webservices/", "CarServiceDocumentStyleImplService");
		Service service = Service.create(url, qName);

		if(handlerResolver != null) {
			service.setHandlerResolver(handlerResolver);
		}

		return service.getPort(CarServiceDocumentStyle.class);
	}

}
