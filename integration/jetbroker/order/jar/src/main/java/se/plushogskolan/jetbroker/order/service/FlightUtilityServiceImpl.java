package se.plushogskolan.jetbroker.order.service;

import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.map.HaversineDistance;
import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.PlaneType;

@Stateless
public class FlightUtilityServiceImpl implements FlightUtilityService{
	@Inject
	private FuelPriceService fuelPriceService;

	@Inject
	private AirportService airportService;
	
	private Logger log = Logger.getLogger(FlightUtilityService.class.getName());
	
	@Override
	public int getTimeForFlightInMinutes(String firstAirportCode, String secondAirportCode, PlaneType plane) {
		log.fine(String.format("Get time for flight in minutes between [airport a: %s] [airport b: %s]", firstAirportCode, secondAirportCode));
		
		return getDistanceForFlight(firstAirportCode, secondAirportCode) / plane.getMaxSpeedKmH() * 60;
	}

	@Override
	public double getFuelPriceForFlight(String firstAirportCode, String secondAirportCode, PlaneType plane) {
		log.fine(String.format("Get fuel price for flight [airport a: %s] [airport b: %s]", firstAirportCode, secondAirportCode));
		
		return getDistanceForFlight(firstAirportCode, secondAirportCode) * plane.getFuelConsumptionPerKm() * fuelPriceService.getFuelPrice();
	}
	
	@Override
	public int getDistanceForFlight(String firstAirportCode, String secondAirportCode) {
		log.fine(String.format("Get distance for flight [airport a: %s] [airport b: %s]", firstAirportCode, secondAirportCode));
	
		Airport first = airportService.getAirportByCode(firstAirportCode);
		Airport second = airportService.getAirportByCode(secondAirportCode);
		
		
		double distance = HaversineDistance.getDistance(first.getLatitude(), first.getLongitude(), second.getLatitude(), second.getLongitude());
		
		log.fine(String.format("calculated distance: %f", distance));
		
		return (int) distance;
	}

	protected FuelPriceService getFuelPriceService() {
		return fuelPriceService;
	}

	protected void setFuelPriceService(FuelPriceService fuelPriceService) {
		this.fuelPriceService = fuelPriceService;
	}
	
}
