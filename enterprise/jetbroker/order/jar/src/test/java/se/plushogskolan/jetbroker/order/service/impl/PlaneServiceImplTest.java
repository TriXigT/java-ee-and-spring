package se.plushogskolan.jetbroker.order.service.impl;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.repository.PlaneRepository;

public class PlaneServiceImplTest {

	PlaneServiceImpl service;
	PlaneRepository repo;
	
	Plane mPlane;
	
	
	@Before
	public void setup() {
		service = new PlaneServiceImpl();
		repo = createMock(PlaneRepository.class);
		mPlane = new Plane(1, "AIR", "Our finest plane");

		service.setRepository(repo);
	}
	
	@Test
	public void createAndFindTest() {
		expect(repo.persist(mPlane)).andReturn(mPlane.getId());
		expect(repo.findById(mPlane.getId())).andReturn(mPlane).times(2); //2 times as service.create() also call repo.findById
		replay(repo);
		assertEquals("create", mPlane, service.createPlane(mPlane));
		assertEquals("find", mPlane, service.getPlane(mPlane.getId()));
		verify(repo);
	}
	
	@Test
	public void getAllPlaneTypesTest() {
		List<Plane> planeList = new ArrayList<Plane>();
		planeList.add(mPlane);
		expect(repo.getAllPlanes()).andReturn(planeList);
		replay(repo);
		assertEquals("get all planes", planeList, service.getAllPlanes());
		verify(repo);
	}
	
	@Test
	public void removeTest() {
		service.removePlane(mPlane);
		expectLastCall().once();
		replay(repo);
		service.removePlane(mPlane);
		verify(repo);
	}

	@Test
	public void updateTest() {
		service.updatePlane(mPlane);
		expectLastCall().once();
		replay(repo);
		service.updatePlane(mPlane);
		verify(repo);
	}
}
