package se.plushogskolan.jetbroker.order.rest.plane.model;

import se.plushogskolan.jetbroker.order.domain.Plane;

public class GetPlaneResponse {
	private long id;
	private String planeTypeCode;
	private String description;
	
	public GetPlaneResponse(Plane plane) {
		setId(plane.getId());
		setPlaneTypeCode(plane.getPlaneTypeCode());
		setDescription(plane.getDescription());
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPlaneTypeCode() {
		return planeTypeCode;
	}

	public void setPlaneTypeCode(String planeTypeCode) {
		this.planeTypeCode = planeTypeCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
