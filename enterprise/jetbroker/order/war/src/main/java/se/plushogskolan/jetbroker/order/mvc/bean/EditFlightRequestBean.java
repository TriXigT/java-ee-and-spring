package se.plushogskolan.jetbroker.order.mvc.bean;

import javax.validation.Valid;

import se.plushogskolan.jetbroker.order.domain.FlightRequest;

public class EditFlightRequestBean {
	@Valid
	private FlightRequest flightRequest;

	public FlightRequest getFlightRequest() {
		return flightRequest;
	}

	public void setFlightRequest(FlightRequest flightRequest) {
		this.flightRequest = flightRequest;
	}
	
}
