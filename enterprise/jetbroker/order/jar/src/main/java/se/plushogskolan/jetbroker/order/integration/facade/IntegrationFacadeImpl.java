package se.plushogskolan.jetbroker.order.integration.facade;

import java.util.logging.Logger;

import javax.ejb.Stateless;

import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.Offer;

@Stateless
public class IntegrationFacadeImpl implements IntegrationFacade {
	Logger log = Logger.getLogger(IntegrationFacadeImpl.class.getName());
	
	@Override
	public void sendFlightRequest(FlightRequest flightRequest) {
		log.fine(String.format("[Confirm flightRequest with confirmationId=%d, agentId=%d, status=%s]", flightRequest.getId(), flightRequest.getAgentId(), flightRequest.getStatus().toString()));
	}

	@Override
	public void sendOffer(Offer offer) {
		log.fine(String.format("[Send offer with flightRequestId=%d]", offer.getFlightRequestId()));
	}
}
