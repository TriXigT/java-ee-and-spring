package se.plushogskolan.jee.utils.repository.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.logging.Logger;

import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.junit.Test;

import se.plushogskolan.jee.utils.domain.IdHolder;
import se.plushogskolan.jee.utils.repository.BaseRepository;

/**
 * An abstract test class that tests basic CRUD functionality. All repositories
 * can reuse these tests, hereby reducing code duplication.
 * 
 * This class uses the Template pattern. This abstract class is the template
 * that coordinates basic test cases. The concrete implementations provide the
 * specific details, such as which entities and repositories to use etc.
 * 
 */
@Transactional(TransactionMode.ROLLBACK)
public abstract class AbstractRepositoryTestNoRemoteServer<E extends IdHolder, F extends BaseRepository<E>> {

	private static Logger log = Logger.getLogger(AbstractRepositoryTestNoRemoteServer.class.getName());

	/*
	 * Subclasses must return the repository to use
	 */
	protected abstract F getRepository();

	/*
	 * Sublcasses must return a valid entity to persist.
	 */
	protected abstract E getEntity1();

	/*
	 * Sublclasses must return a second valid entity. Should be different from
	 * entity 1.
	 */
	protected abstract E getEntity2();

	@Test
	public void testCreate() {

		long id1 = getRepository().persist(getEntity1());
		long id2 = getRepository().persist(getEntity2());

		assertTrue("Entity 1 ID", id1 > 0);
		assertTrue("Entity 2 ID", id2 > id1);
	}

	@Test
	public void testFindById() {

		long id = getRepository().persist(getEntity1());
		IdHolder entity = getRepository().findById(id);
		assertNotNull("Entity is not null", entity);
		assertEquals("Id is correct", id, entity.getId());
	}

	@Test
	public void testDelete() {

		long id = getRepository().persist(getEntity1());
		E entity = getRepository().findById(id);
		assertNotNull("Entity is not null", entity);

		getRepository().remove(entity);
		assertNull("Deleted object return null", getRepository().findById(id));
	}

	@Test
	public void testUpdate() {
		long id = getRepository().persist(getEntity1());
		E entity = getRepository().findById(id);

		// This doesn´t test much, but is still some control that no exception
		// is thrown.
		getRepository().update(entity);

	}

}
