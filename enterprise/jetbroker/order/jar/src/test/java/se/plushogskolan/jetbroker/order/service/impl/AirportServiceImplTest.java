package se.plushogskolan.jetbroker.order.service.impl;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.repository.AirportRepository;

public class AirportServiceImplTest {

	AirportServiceImpl service;
	AirportRepository repo;
	
	Airport mAirport;
	
	
	@Before
	public void setup() {
		service = new AirportServiceImpl();
		repo = createMock(AirportRepository.class);
		mAirport = TestFixture.getValidAirport();

		service.setRepository(repo);
	}
		
	@Test
	public void getAirportByCodeTest() {
		expect(repo.getAirportByCode(mAirport.getCode())).andReturn(mAirport);
		replay(repo);
		assertEquals("get by code", mAirport, service.getAirportByCode(mAirport.getCode()));
		verify(repo);
	}
	
	@Test
	public void getAllPlaneTypesTest() {
		List<Airport> airportList = new ArrayList<Airport>();
		airportList.add(mAirport);
		expect(repo.getAllAirports()).andReturn(airportList);
		replay(repo);
		assertEquals("get all planes", airportList, service.getAllAirports());
		verify(repo);
	}
}
