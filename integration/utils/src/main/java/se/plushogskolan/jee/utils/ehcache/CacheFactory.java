package se.plushogskolan.jee.utils.ehcache;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;

/**
 * A CDI factory for injecting net.sf.ehcache.CacheManager instances. This makes it easy to inject
 * CacheManager with
 * 
 * @Inject CacheManager manager;
 * 
 *         instead of
 * 
 * CacheManager cacheManager = CacheManager.getInstance();
 * 
 * The Qualifier CacheName is used to specify the id/name of the cache to inject.
 */
@ApplicationScoped
public class CacheFactory {
	
	@Produces @CacheName Cache getCache(InjectionPoint injectionPoint) {
		CacheManager mgr = CacheManager.getInstance();
		CacheName name = injectionPoint.getAnnotated().getAnnotation(CacheName.class);
		
		return mgr.getCache(name.cacheName());
	}
}