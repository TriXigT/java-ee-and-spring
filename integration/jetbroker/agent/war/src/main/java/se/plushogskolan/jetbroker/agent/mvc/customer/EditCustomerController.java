package se.plushogskolan.jetbroker.agent.mvc.customer;

import java.util.logging.Logger;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.services.CustomerService;

@Controller
@RequestMapping("/editCustomer/{customerId}.html")
public class EditCustomerController {

	Logger log = Logger.getLogger(EditCustomerController.class.getName());

	@Inject
	private CustomerService customerService;

	@Autowired
	private Validator validator;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView index(@PathVariable long customerId) {

		log.fine("Edit customer, id=" + customerId);

		EditCustomerBean bean = new EditCustomerBean();
		boolean isNewCustomer = customerId <= 0;

		if (isNewCustomer) {
			Customer customer = new Customer();
			bean.setCustomer(customer);
		} else {
			Customer customer = getCustomerService().getCustomer(customerId);
			bean.setCustomer(customer);
		}

		ModelAndView mav = new ModelAndView("customer/editCustomer");
		mav.addObject("editCustomerBean", bean);
		return mav;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView handleSubmit(@Valid EditCustomerBean bean, BindingResult bindingResult) throws Exception {

		if (bindingResult.hasErrors()) {
			ModelAndView mav = new ModelAndView("customer/editCustomer");
			mav.addObject("editCustomerBean", bean);
			return mav;
		} else {

			Customer customer = bean.getCustomer();

			if (customer.getId() > 0) {
				getCustomerService().updateCustomer(customer);
			} else {
				getCustomerService().createCustomer(customer);
			}
		}
		return new ModelAndView("redirect:/index.html");
	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

}
