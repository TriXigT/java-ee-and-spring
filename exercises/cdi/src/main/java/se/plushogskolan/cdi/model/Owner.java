package se.plushogskolan.cdi.model;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;

import se.plushogskolan.cdi.annotations.Income;
import se.plushogskolan.cdi.annotations.Income.IncomeType;

public class Owner {

	private static long nextId = 1;
	private long id;
	private String name;

	/**
	 * Important to have an empty constructor. Otherwise this is not a managed
	 * bean and will not be found by CDI.
	 */
	public Owner() {
		setId(nextId++);
	}

	public Owner(String name) {
		this();
		setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Owner [id=" + id + ", name=" + name + "]";
	}

	/**
	 * Producer method for generating a high income Owner
	 */
	@Produces @ApplicationScoped @Income(IncomeType.HIGH)
	public Owner createOwner() {
		System.out.println("Creating a new high income user");
		return new Owner("Hans von Sparre");
	}

	/**
	 * Producer method for generating a medium income Owner
	 */
	@Produces @Income(IncomeType.MEDIUM)
	public Owner createOwner_medium() {
		System.out.println("Creating a new medium income user");
		return new Owner("Hans Olsson");
	}

}
