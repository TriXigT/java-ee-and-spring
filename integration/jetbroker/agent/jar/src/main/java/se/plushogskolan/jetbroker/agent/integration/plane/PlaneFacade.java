package se.plushogskolan.jetbroker.agent.integration.plane;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.agent.domain.AirPort;
import se.plushogskolan.jetbroker.agent.domain.PlaneType;

@Local
public interface PlaneFacade {

	public List<AirPort> getAllAirports();

	public List<PlaneType> getAllPlaneTypes();

}
