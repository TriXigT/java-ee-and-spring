package webservices.bindingstyles.rpc;

import javax.jws.WebService;
import javax.xml.ws.Endpoint;

import webservices.bindingstyles.Car;
import webservices.bindingstyles.CarRepository;
import webservices.bindingstyles.RegNumber;

/**
 * Implement the CarService web service.
 */
@WebService(endpointInterface = "webservices.bindingstyles.rpc.CarServiceRpcStyle")
public class CarServiceRpcStyleImpl implements CarServiceRpcStyle {

	private CarRepository repo = new CarRepository();

	public static void main(String[] args) {
		Endpoint.publish("http://localhost:8180/ws/carServiceRpcStyle", new CarServiceRpcStyleImpl());
	}

	@Override
	public Car getCar(RegNumber regNo) {
		return repo.getCar(regNo);
	}

}
