package se.plushogskolan.jetbroker.order.integration.simulator;

import javax.ejb.Local;

@Local
public interface SystemSimulator {
	void receiveFlightRequest(long agentId);
}
