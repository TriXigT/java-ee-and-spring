package se.plushogskolan.jetbroker.agent.repository.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;
import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jee.utils.ehcache.CacheName;
import se.plushogskolan.jetbroker.agent.domain.AirPort;
import se.plushogskolan.jetbroker.agent.integration.plane.PlaneFacade;
import se.plushogskolan.jetbroker.agent.repository.AirPortRepository;

@Stateless
public class CachedAirPortRepository implements AirPortRepository {

	@Inject
	private Logger log;

	@Inject
	@Prod
	private PlaneFacade planeFacade;

	@Inject
	@CacheName(cacheName="airportCache")
	private Cache cache;

	@SuppressWarnings("unchecked")
	@Override
	public List<AirPort> getAllAirPorts() {
		Map<String, AirPort> airPortsMap = new HashMap<String, AirPort>();

		if(cache.isKeyInCache("allAirPorts")) {
			log.fine("Get AirPorts from cache");
			
			airPortsMap = (HashMap<String, AirPort>) cache.get("allAirPorts").getObjectValue();
		} else {
			log.fine("No AirPorts in cache, get AirPorts from planeFacade");

			airPortsMap = getAirPortListAsMap(planeFacade.getAllAirports());

			log.fine("Put Map<(String) Code, AirPort> with key \"allAirPorts\" in cache");

			cache.put(new Element("allAirPorts", airPortsMap));
		}

		return new ArrayList<AirPort>(airPortsMap.values()) ;
	}

	@Override
	public AirPort getAirPort(String code) {
		Element element = cache.get(code);

		if(element == null) {
			Map<String, AirPort> airPortsMap = getAirPortListAsMap(getAllAirPorts());
			
			log.fine(String.format("No cached AirPort found for code [%s], get AirPort from cached Map", code));

			AirPort airPort = airPortsMap.get(code);

			if(airPort != null) {
				log.fine(String.format("Put retrieved AirPort in cache with code [%s]", code));

				cache.put(new Element(code, airPort));

				return airPort;
			} else {
				log.warning(String.format("No AirPort matching code [%s] found, will return null", code));
			}
		}

		return (AirPort) element.getObjectValue();
	}
	
	private Map<String,AirPort> getAirPortListAsMap(List<AirPort> airPortList) {
		Map<String, AirPort> airPortsMap = new HashMap<String, AirPort>();
		
		log.fine("Put AirPorts from List into Map <Code, AirPort>");
		
		for(AirPort airPort : airPortList) {
			airPortsMap.put(airPort.getCode(), airPort);
		}
		
		return airPortsMap;
	}

	@Override
	public void handleAirportsChangedEvent() {
		log.fine("Clear cache");
		
		cache.removeAll();
	}

}
