package se.plushogskolan.jetbroker.plane.integration.jetbroker.ws.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebService;

import se.plushogskolan.jetbroker.plane.domain.Airport;
import se.plushogskolan.jetbroker.plane.domain.PlaneType;
import se.plushogskolan.jetbroker.plane.integration.jetbroker.ws.PlaneWebService;
import se.plushogskolan.jetbroker.plane.integration.jetbroker.ws.WSAirport;
import se.plushogskolan.jetbroker.plane.integration.jetbroker.ws.WSPlaneType;
import se.plushogskolan.jetbroker.plane.service.AirportService;
import se.plushogskolan.jetbroker.plane.service.PlaneService;


@Stateless
@WebService(
		endpointInterface = "se.plushogskolan.jetbroker.plane.integration.jetbroker.ws.PlaneWebService", 
		name = "PlaneWebService")
public class PlaneWebServiceImpl implements PlaneWebService{

	@Inject
	private Logger log;
	
	@Inject
	private PlaneService planeService;
	
	@Inject
	private AirportService airportService;
	
	@Override
	public List<WSAirport> getAirports() {
		log.fine("get airports by SOAP");
		
		List<WSAirport> airports = new ArrayList<WSAirport>();
		
		for(Airport airport : airportService.getAllAirports()) {
			airports.add(new WSAirport(airport));
		}
		
		return airports;
	}

	@Override
	public List<WSPlaneType> getPlaneTypes() {
		log.fine("get planetypes by SOAP");
		
		List<WSPlaneType> planeTypes = new ArrayList<WSPlaneType>();
		
		for(PlaneType planeType : planeService.getAllPlaneTypes()) {
			planeTypes.add(new WSPlaneType(planeType));
		}
		
		return planeTypes;
	}

}
