package se.plushogskolan.jetbroker.order.service;

import java.util.logging.Logger;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class FuelPriceServiceImpl implements FuelPriceService{

	@Inject
	private Logger log;
	
	private double fuelPrice = 5.2;
	
	@Lock(LockType.READ)
	@Override
	public double getFuelPrice() {
		log.fine(String.format("get fuel price [%f]", fuelPrice));
		
		return fuelPrice;
	}

	@Lock(LockType.WRITE)
	@Override
	public void setFuelPrice(double fuelPrice) {
		log.fine(String.format("set fuel price from [%f] to [%f]", this.fuelPrice, fuelPrice));
		
		this.fuelPrice = fuelPrice;
	}

}
