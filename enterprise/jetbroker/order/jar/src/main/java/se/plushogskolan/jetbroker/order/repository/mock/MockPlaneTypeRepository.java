package se.plushogskolan.jetbroker.order.repository.mock;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.repository.PlaneTypeRepository;
@Stateless
public class MockPlaneTypeRepository implements PlaneTypeRepository{

	private List<PlaneType> entities;
	
	public MockPlaneTypeRepository() {
		entities = new ArrayList<PlaneType>();
		
		PlaneType planeType = new PlaneType("BOI","Boeing 404",100,100,100,22.5);
		
		entities.add(planeType);
	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {
		return getMockList();
	}

	protected List<PlaneType> getMockList() {
		return entities;
	}
	
	@Override
	public PlaneType getPlaneTypeByCode(String code) {
		for(PlaneType planeType : entities) {
			if(planeType.getCode().equals(code)) {
				return planeType;
			}
		}
		return null;
	}

}
