package webservices.bindingstyles.document;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import webservices.bindingstyles.Car;
import webservices.bindingstyles.CarService;
import webservices.bindingstyles.RegNumber;

/**
 * Add annotations to make this a document style web service
 */
@WebService
@SOAPBinding(style=Style.DOCUMENT)
public interface CarServiceDocumentStyle extends CarService {
	
	@WebMethod
	@Override
	public Car getCar(RegNumber regNo);
}
