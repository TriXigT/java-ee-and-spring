package se.plushogskolan.jetbroker.agent.services;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.agent.domain.PlaneType;
import se.plushogskolan.jetbroker.agent.events.PlaneTypesChangedEvent;
import se.plushogskolan.jetbroker.agent.repository.PlaneTypeRepository;

@Stateless
public class PlaneServiceImp implements PlaneService {

	@Inject
	private Logger log;
	
	@Inject
	private PlaneTypeRepository planeTypeRepository;

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public List<PlaneType> getAllPlaneTypes() {
		log.fine("get all planetypes");
		
		return getPlaneTypeRepository().getAllPlaneTypes();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public PlaneType getPlaneType(String code) {
		log.fine(String.format("get planetype by code [%s]", code));
		return getPlaneTypeRepository().getPlaneType(code);
	}

	public PlaneTypeRepository getPlaneTypeRepository() {
		return planeTypeRepository;
	}

	public void setPlaneTypeRepository(PlaneTypeRepository planeTypeRepository) {
		this.planeTypeRepository = planeTypeRepository;
	}

	@Override
	public void handlePlaneTypesChangedEvent(@Observes PlaneTypesChangedEvent event) {
		log.fine("handle received PlaneTypesChangedEvent");
		
		planeTypeRepository.handlePlaneTypeChangedEvent();
	}

}
