package se.plushogskolan.cdi.services;

import javax.enterprise.inject.Produces;

import org.joda.time.DateTime;

import se.plushogskolan.cdi.annotations.TodaysDate;
import se.plushogskolan.cdi.annotations.YesterdaysDate;

/**
 * A class for producing a dates, as specified by the @TodaysDate and @YesterdaysDate
 * annotations.
 * 
 */
public class DateGenerator {
	@Produces @TodaysDate
	public String getTodaysDate() {
		return new DateTime().toString("dd-MM-yyyy");
	}

	@Produces @YesterdaysDate
	public String getYesterdaysDate() {
		DateTime date = new DateTime();
		date = date.minusDays(1);
		return date.toString("dd-MM-yyyy");
	}

}
