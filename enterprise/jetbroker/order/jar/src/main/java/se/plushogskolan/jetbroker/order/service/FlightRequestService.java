package se.plushogskolan.jetbroker.order.service;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.Status;

@Local
public interface FlightRequestService {
	FlightRequest getFlightRequest(long id);

	FlightRequest createFlightRequest(FlightRequest flightRequest);

	void updateFlightRequest(FlightRequest flightRequest);

	/**
	 * Retrieve a list with all FlightRequests
	 * @return a List of FlightRequests
	 */
	List<FlightRequest> getAllFlightRequests();
	
	/**
	 * Get FlightRequests having a status matching supplied Status
	 * @return List of FlightRequests mathing supplied status
	 */
	List<FlightRequest> getFlightRequestsByStatus(Status status);
	
}
