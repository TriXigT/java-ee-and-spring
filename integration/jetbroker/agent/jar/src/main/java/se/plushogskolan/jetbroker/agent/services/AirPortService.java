package se.plushogskolan.jetbroker.agent.services;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.agent.domain.AirPort;
import se.plushogskolan.jetbroker.agent.events.AirportsChangedEvent;

@Local
public interface AirPortService {

	AirPort getAirPort(String code);

	List<AirPort> getAllAirPorts();

	void handleAirportsChangedEvent(AirportsChangedEvent event);

}
