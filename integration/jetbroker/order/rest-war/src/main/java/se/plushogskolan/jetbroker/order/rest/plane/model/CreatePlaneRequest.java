package se.plushogskolan.jetbroker.order.rest.plane.model;

import se.plushogskolan.jetbroker.order.domain.Plane;

public class CreatePlaneRequest {
	private String planeTypeCode;
	private String description;
	
	public Plane buildPlane() {
		Plane plane = new Plane();
		plane.setPlaneTypeCode(planeTypeCode);
		plane.setDescription(description);
		
		return plane;
	}

	public String getPlaneTypeCode() {
		return planeTypeCode;
	}

	public void setPlaneTypeCode(String planeTypeCode) {
		this.planeTypeCode = planeTypeCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
