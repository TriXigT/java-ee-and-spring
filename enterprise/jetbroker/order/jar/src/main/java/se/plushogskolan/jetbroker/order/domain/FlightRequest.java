package se.plushogskolan.jetbroker.order.domain;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.joda.time.DateTime;

import se.plushogskolan.jee.utils.domain.IdHolder;
import se.plushogskolan.jee.utils.validation.ArrivalAndDepartureAirport;
import se.plushogskolan.jee.utils.validation.ArrivalAndDepartureAirportHolder;

@Entity
@ArrivalAndDepartureAirport
public class FlightRequest implements IdHolder, ArrivalAndDepartureAirportHolder{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	private long agentId;
	@NotNull
	@Enumerated(EnumType.STRING)
	private Status status = Status.NEW;
	@NotBlank
	private String departureAirportCode;
	@NotBlank
	private String arrivalAirportCode;	
	@NotNull
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime date;
	
	private String planeCode;
	
	@Range(min=1, max=500)
	private int passengers;
	
	private double offeredPrice;
	
	public FlightRequest() {

	}
	
	public FlightRequest(long agentId, Status status, String departureAirportCode,
			String arrivalAirportCode, DateTime date, String planeCode,
			int passengers, double offeredPrice) {
		this.agentId = agentId;
		this.status = status;
		this.departureAirportCode = departureAirportCode;
		this.arrivalAirportCode = arrivalAirportCode;
		this.date = date;
		this.planeCode = planeCode;
		this.passengers = passengers;
		this.offeredPrice = offeredPrice;
	}

	@Override
	public long getId() {
		return id;
	}

	@Override
	public void setId(long id) {
		this.id = id;
	}
	
	public long getAgentId() {
		return agentId;
	}

	public void setAgentId(long agentId) {
		this.agentId = agentId;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	@Override
	public String getDepartureAirportCode() {
		return departureAirportCode;
	}
	
	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}
	@Override
	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}

	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode;
	}

	public DateTime getDate() {
		return date;
	}

	public void setDate(DateTime date) {
		this.date = date;
	}

	public int getPassengers() {
		return passengers;
	}

	public void setPassengers(int passengers) {
		this.passengers = passengers;
	}

	public double getOfferedPrice() {
		return offeredPrice;
	}

	public void setOfferedPrice(double offeredPrice) {
		this.offeredPrice = offeredPrice;
	}

	public String getPlaneTypeCode() {
		return planeCode;
	}

	public void setPlaneCode(String planeCode) {
		this.planeCode = planeCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((arrivalAirportCode == null) ? 0 : arrivalAirportCode
						.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime
				* result
				+ ((departureAirportCode == null) ? 0 : departureAirportCode
						.hashCode());
		result = prime * result + passengers;
		result = prime * result
				+ ((planeCode == null) ? 0 : planeCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FlightRequest other = (FlightRequest) obj;
		if (arrivalAirportCode == null) {
			if (other.arrivalAirportCode != null)
				return false;
		} else if (!arrivalAirportCode.equals(other.arrivalAirportCode))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (departureAirportCode == null) {
			if (other.departureAirportCode != null)
				return false;
		} else if (!departureAirportCode.equals(other.departureAirportCode))
			return false;
		if (passengers != other.passengers)
			return false;
		if (planeCode == null) {
			if (other.planeCode != null)
				return false;
		} else if (!planeCode.equals(other.planeCode))
			return false;
		return true;
	}



}
