package se.plushogskolan.jetbroker.order.mvc.bean;

import javax.validation.Valid;

import se.plushogskolan.jetbroker.order.domain.Offer;

public class OfferBean {
	@Valid
	private Offer offer;

	public OfferBean() {

	}
	
	public OfferBean(Offer offer) {
		this.offer = offer;
	}

	public Offer getOffer() {
		return offer;
	}

	public void setOffer(Offer offer) {
		this.offer = offer;
	}
	
	
}
