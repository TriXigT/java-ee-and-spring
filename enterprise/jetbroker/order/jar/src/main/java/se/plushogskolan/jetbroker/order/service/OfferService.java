package se.plushogskolan.jetbroker.order.service;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.order.domain.Offer;

@Local
public interface OfferService {
	Offer getOffer(long id);
	Offer createOffer(Offer Offer);
	void updateOffer(Offer Offer);
	void removeOffer(Offer Offer);
	List<Offer> getAllOffers();
}
