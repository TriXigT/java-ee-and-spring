package se.plushogskolan.jee.utils.repository.mock;

import se.plushogskolan.jee.utils.domain.IdHolder;

/**
 * A MockIdGenerator is a utility class for assigning and keeping track of id's when
 * using mock data.
 * @author Pär
 *
 */
public class MockIdGenerator {
	long id = 0;
	
	/**
	 * Assign next id to supplied IdHolder and increment id counter. Id's starting at 1 (1>2>3...).
	 * @param An implementation of IdHolder 
	 * @return the next id
	 */
	public long assignNextId(IdHolder entity) {
		entity.setId(id);
		return ++id;
	}
}
