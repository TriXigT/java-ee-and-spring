package se.plushogskolan.jee.utils.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LongitudeValidator implements
		ConstraintValidator<Longitude, Object> {

	@Override
	public void initialize(Longitude longitude) {

	}

	@Override
	public boolean isValid(Object longitude, ConstraintValidatorContext context) {
		double casted;
		
		if(longitude == null) {
			return false;
		} else if(longitude instanceof String) {
			String s = (String) longitude;
			casted = Double.valueOf(s);
		} else if(longitude instanceof Double) {
			casted = (Double) longitude;
		} else if(longitude instanceof Integer) {
			casted = Double.valueOf((Integer) longitude);
		} else {
			return false;
		}
		
		if((casted >= -180 && casted <= 180) && casted != 0) {
			return true;
		}
		
		return false;
	}

}
