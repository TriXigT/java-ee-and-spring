package se.plushogskolan.jetbroker.order.service;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.domain.PlaneType;

@Local
public interface PlaneService {

	// Airport
	Airport getAirport(String code);

	// Plane
	List<Plane> getAllPlanes();

	Plane getPlane(long id);

	Plane createPlane(Plane plane);

	void updatePlane(Plane plane);

	void deletePlane(long id);

	// Plane types
	PlaneType getPlaneType(String planeTypeCode);

	List<PlaneType> getAllPlaneTypes();

}
