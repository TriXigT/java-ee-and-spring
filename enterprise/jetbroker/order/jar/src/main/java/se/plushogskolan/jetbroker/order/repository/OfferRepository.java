package se.plushogskolan.jetbroker.order.repository;

import java.util.List;

import se.plushogskolan.jee.utils.repository.BaseRepository;
import se.plushogskolan.jetbroker.order.domain.Offer;

public interface OfferRepository extends BaseRepository<Offer>{
	List<Offer> getAllOffers();
}
