package se.plushogskolan.jetbroker.plane.mvc;

import org.hibernate.validator.constraints.NotBlank;

import se.plushogskolan.jee.utils.validation.AirportCode;
import se.plushogskolan.jee.utils.validation.Latitude;
import se.plushogskolan.jee.utils.validation.Longitude;
import se.plushogskolan.jetbroker.plane.domain.Airport;

public class EditAirportBean {

	private long id;
	@NotBlank
	@AirportCode
	private String code;
	@NotBlank(message = "{validation.airport.name.missing}")
	private String name;
	@Latitude
	private String latitude;
	@Longitude
	private String longitude;

	public void copyAirportValuesToBean(Airport airport) {
		setId(airport.getId());
		setCode(airport.getCode());
		setName(airport.getName());
		setLatitude("" + airport.getLatitude());
		setLongitude("" + airport.getLongitude());
	}

	public void copyBeanValuesToAirport(Airport airport) {
		airport.setId(getId());
		airport.setCode(getCode());
		airport.setName(getName());
		airport.setLatitude(getLatitude() != null ? Double.parseDouble(getLatitude()) : 0);
		airport.setLongitude(getLongitude() != null ? Double.parseDouble(getLongitude()) : 0);

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

}
