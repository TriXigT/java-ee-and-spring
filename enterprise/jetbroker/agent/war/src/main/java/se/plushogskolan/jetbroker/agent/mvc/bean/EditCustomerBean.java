package se.plushogskolan.jetbroker.agent.mvc.bean;

import javax.validation.Valid;

import se.plushogskolan.jetbroker.agent.domain.Customer;

public class EditCustomerBean {
	@Valid
	private Customer customer;

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
}
