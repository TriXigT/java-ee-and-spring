package se.plushogskolan.jetbroker.agent.integration.order.mock.mdb;

import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Mock;
import se.plushogskolan.jetbroker.agent.annotations.FlightRequestChanged;
import se.plushogskolan.jetbroker.agent.annotations.FlightRequestChanged.ChangeType;
import se.plushogskolan.jetbroker.agent.domain.request.FlightOffer;
import se.plushogskolan.jetbroker.agent.events.FlightRequestChangedEvent;
import se.plushogskolan.jetbroker.agent.services.FlightRequestService;

@ApplicationScoped
public class MockFlightRequestOfferMDB {

	private static Logger log = Logger.getLogger(MockFlightRequestOfferMDB.class.getName());

	private static long offerId = 1;

	@Inject
	private FlightRequestService flightRequestService;

	public void handleEvent(
			@Observes @FlightRequestChanged(ChangeType.OFFER_RECEIVED) @Mock FlightRequestChangedEvent event) {

		long id = offerId++;
		log.fine("FlightRequest offer is recieved in mock");

		FlightOffer offer = new FlightOffer();
		offer.setId(id);
		offer.setOfferedPrice(Math.random() * 1000);
		offer.setPlaneTypeCode("B770");
		getFlightRequestService().handleFlightOffer(offer, event.getFlightRequestId());
	}

	public FlightRequestService getFlightRequestService() {
		return flightRequestService;
	}

	public void setFlightRequestService(FlightRequestService flightRequestService) {
		this.flightRequestService = flightRequestService;
	}

}
