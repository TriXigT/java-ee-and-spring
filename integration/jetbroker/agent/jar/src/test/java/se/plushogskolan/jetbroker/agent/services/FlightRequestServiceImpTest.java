package se.plushogskolan.jetbroker.agent.services;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertEquals;

import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.agent.TestFixture;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestStatus;
import se.plushogskolan.jetbroker.agent.integration.order.OrderFacade;
import se.plushogskolan.jetbroker.agent.repository.FlightRequestRepository;

public class FlightRequestServiceImpTest {

	private FlightRequestServiceImp service;

	private FlightRequest request;
	private OrderFacade facade;
	private FlightRequestRepository repository;
	
	@Before
	public void setup() {
		service = new FlightRequestServiceImp();
		
		request = TestFixture.getValidFlightRequest();
		request.setId(1);
		
		repository = createMock(FlightRequestRepository.class);
		service.setRepository(repository);
		
		service.setLog(Logger.getLogger(FlightRequestServiceImp.class.getName()));
	}
	
	@Test
	public void createFlightRequestTest() throws Exception {
		facade = createMock(OrderFacade.class);
		service.setOrderFacade(facade);
		
		expect(repository.persist(request)).andReturn(request.getId());
		expect(repository.findById(request.getId())).andReturn(request);
		replay(repository);
		
		facade.sendFlightRequest(request);
		replay(facade);
		
		service.createFlightRequest(request);
		
		assertEquals("flightrequest status is set", FlightRequestStatus.CREATED, request.getRequestStatus());
		
		verify(repository);
		verify(facade);
	}
	
	@Test
	public void handleFlightRequestConfirmation() {
		FlightRequestConfirmation confirmation = TestFixture.getValidFlightRequestConfirmation();
		
		expect(repository.findById(confirmation.getAgentRequestId())).andReturn(request);
		repository.update(request);
		replay(repository);
		
		service.handleFlightRequestConfirmation(confirmation);
		
		assertEquals("confirmation id set", 1, request.getConfirmationId());
		assertEquals("request status set", request.getRequestStatus(), FlightRequestStatus.REQUEST_CONFIRMED);
		
		verify(repository);
	}

}
