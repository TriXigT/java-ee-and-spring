package se.plushogskolan.jetbroker.order.service.impl;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.integration.facade.IntegrationFacade;
import se.plushogskolan.jetbroker.order.repository.FlightRequestRepository;

public class FlightRequestServiceImplTest {
	private FlightRequestServiceImpl flightRequestService;
	private FlightRequestRepository flightRequestRepository;
	
	private FlightRequest flightRequest;
	
	@Before
	public void setup() {
		flightRequestService = new FlightRequestServiceImpl();
		flightRequestRepository = createMock(FlightRequestRepository.class);
		
		flightRequestService.setRepository(flightRequestRepository);
		
		flightRequest = TestFixture.getValidFlightRequest();
	}

	@Test
	public void getAllFlightRequestsTest() {
		List<FlightRequest> flightRequestList = new ArrayList<FlightRequest>();
		flightRequestList.add(flightRequest);
		expect(flightRequestRepository.getAllFlightRequests()).andReturn(flightRequestList);
		replay(flightRequestRepository);
		assertEquals("get all FlightRequests", flightRequestList, flightRequestService.getAllFlightRequests());
		verify(flightRequestRepository);
	}

}
