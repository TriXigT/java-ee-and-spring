package se.plushogskolan.jee.utils.validation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class LatitudeValidatorTest {

	@Test
	public void isValidTest() {
		LatitudeValidator validator = new LatitudeValidator();
		
		assertTrue("is valid integer", validator.isValid(90, null));
		assertTrue("is valid double", validator.isValid(9.99d, null));
		assertTrue("is valid string", validator.isValid("-66", null));
		
		assertFalse("is not valid integer", validator.isValid(999, null));
		assertFalse("is not valid double", validator.isValid(-111.9d, null));
		assertFalse("is not valid string", validator.isValid("0", null));
	}

}
