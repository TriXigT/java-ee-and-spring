package se.plushogskolan.jetbroker.plane.repository.jpa;

import static org.junit.Assert.assertEquals;

import javax.inject.Inject;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.plane.domain.Airport;

@RunWith(Arquillian.class)
@Transactional(TransactionMode.ROLLBACK)
public class JpaAirportRepositoryIntegrationTest extends AbstractRepositoryTest<Airport, JpaAirportRepository> {

	@Inject
	JpaAirportRepository repo;

	@Test
	public void testDeleteOldAirports() {

		DateTime now = new DateTime();
		DateTime dateMinus1 = now.minusHours(1);
		DateTime dateMinus2 = now.minusHours(2);

		Airport airport = TestFixture.getValidAirport();
		airport.setId(0);
		airport.setLastUpdated(now);
		long id1 = repo.persist(airport);

		Airport airport2 = TestFixture.getValidAirport();
		airport2.setId(0);
		airport2.setLastUpdated(dateMinus2);
		repo.persist(airport2);

		assertEquals("All airports", 2, repo.getAllAirports().size());

		int noOfDeletes = repo.deleteAirportsOlderThan(dateMinus1);
		assertEquals("No of deletes", 1, noOfDeletes);
		assertEquals("All airports after delete", 1, repo.getAllAirports().size());
		assertEquals("Correct airport is in DB", id1, repo.getAllAirports().iterator().next().getId());

	}

	@Test(expected = Exception.class)
	public void testValidation() {

		// Create airport with empty code. ID must be 0 for integrations tests
		// using JPA.
		Airport airport = TestFixture.getValidAirport("", "Gothenburg");
		repo.persist(airport);
	}

	@Override
	protected JpaAirportRepository getRepository() {
		return repo;
	}

	@Override
	protected Airport getEntity1() {
		return TestFixture.getValidAirport("GBG", "City airport");
	}

	@Override
	protected Airport getEntity2() {
		return TestFixture.getValidAirport("STM", "Stockholm");
	}

}
