package se.plushogskolan.jetbroker.order.repository;

import java.util.List;

import se.plushogskolan.jetbroker.order.domain.PlaneType;


public interface PlaneTypeRepository {

	List<PlaneType> getAllPlaneTypes();

	PlaneType getPlaneTypeByCode(String code);
}
