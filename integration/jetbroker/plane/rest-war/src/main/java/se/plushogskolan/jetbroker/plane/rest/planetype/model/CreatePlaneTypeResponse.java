package se.plushogskolan.jetbroker.plane.rest.planetype.model;

public class CreatePlaneTypeResponse {
	private long id;

	public CreatePlaneTypeResponse(long id) {
		super();
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
}
