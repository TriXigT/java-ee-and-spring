package se.plushogskolan.jee.utils.repository.mock;

import java.util.ArrayList;
import java.util.List;

import se.plushogskolan.jee.utils.domain.IdHolder;

/**
 * Utility class to provide test data for testing classes in package se.plushogskolan.jee.utils.repository.mock
 * @author Pär
 *
 */
public class MockTestFixture {

	public static IdHolderImpl getIdHolderImpl() {
		return new IdHolderImpl();
	}

	/**
	 * Create and return a new instance with a supplied number of IdHolderImpl objects
	 * to add to the List which the class operates on.
	 * @param numberOfMockEntities
	 */
	public static MockAbstractRepositoryImpl getMockAbstractRepositoryImpl(int numberOfMockEntities) {
		return new MockAbstractRepositoryImpl(numberOfMockEntities);
	}

	/**
	 * Basic implementation of IdHolder
	 * @author Pär
	 *
	 */
	public static class IdHolderImpl implements IdHolder{
		private long id;

		public IdHolderImpl() {
		}

		@Override
		public long getId() {
			return id;
		}

		@Override
		public void setId(long id) {
			this.id = id;
		}
	}

	/**
	 * Basic implementation of MockAbstractRepository
	 * @author Pär
	 *
	 */
	public static class MockAbstractRepositoryImpl extends MockAbstractRepository<IdHolder>{

		private int numberOfMockEntities;

		/**
		 * Set the number of number of IdHolderImpl objects to add to the List which 
		 * the class operates on.
		 * @param numberOfMockEntities number of IdHolderImpl objects to add to the List
		 */
		public MockAbstractRepositoryImpl(int numberOfMockEntities) {
			this.numberOfMockEntities = numberOfMockEntities;
		}

		@Override
		public List<IdHolder> getMockList() {
			List<IdHolder> data = new ArrayList<IdHolder>();

			for(int i = 1 ; i <= numberOfMockEntities ; i++) {
				IdHolderImpl temp = new IdHolderImpl();
				//Use the idGenerator to have correct incrementation of ids
				getIdGenerator().assignNextId(temp);
				
				data.add(temp);
			}

			return data;
		}

		public int getNumberOfMockEntities() {
			return numberOfMockEntities;
		}

		public void setNumberOfMockEntities(int numberOfMockEntities) {
			this.numberOfMockEntities = numberOfMockEntities;
		}

	}
}
