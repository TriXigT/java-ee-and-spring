package se.plushogskolan.jetbroker.order.service;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.PlaneType;

@Local
public interface FlightUtilityService {
	double getTimeForFlightInMinutes(Airport a, Airport b, PlaneType planeType);
	double getFuelPriceForFlight(Airport a, Airport b, PlaneType planeType);
	double getDistanceForFlight(Airport a, Airport b);
}
