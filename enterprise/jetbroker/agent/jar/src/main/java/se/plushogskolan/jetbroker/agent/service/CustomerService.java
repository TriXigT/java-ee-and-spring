package se.plushogskolan.jetbroker.agent.service;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.agent.domain.Customer;

@Local
public interface CustomerService {
	Customer getCustomer(long id);

	Customer createCustomer(Customer customer);

	void updateCustomer(Customer customer);

	/**
	 * Retrieve a list with all Customers
	 * @return a List of Customers
	 */
	List<Customer> getAllCustomers();
	
	Customer getCustomerByEmail(String email);
}
