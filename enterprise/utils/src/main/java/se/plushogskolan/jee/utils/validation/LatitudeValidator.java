package se.plushogskolan.jee.utils.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LatitudeValidator implements
		ConstraintValidator<Latitude, Object> {
	
	@Override
	public void initialize(Latitude latitude) {

	}

	@Override
	public boolean isValid(Object latitude, ConstraintValidatorContext context) {
		double casted;
		
		if(latitude == null) {
			return false;
		}else if(latitude instanceof String) {
			String s = (String) latitude;
			casted = Double.valueOf(s);
		} else if(latitude instanceof Double) {
			casted = (Double) latitude;
		} else if(latitude instanceof Integer) {
			casted = Double.valueOf((Integer) latitude);
		} else {
			return false;
		}
		
		if((casted >= -90 && casted <= 90) && casted != 0) {
			return true;
		}
		
		return false;
	}

}
