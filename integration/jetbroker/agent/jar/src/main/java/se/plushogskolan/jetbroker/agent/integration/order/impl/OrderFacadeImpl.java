package se.plushogskolan.jetbroker.agent.integration.order.impl;

import java.text.SimpleDateFormat;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSession;
import javax.jms.Session;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jee.utils.jms.JmsHelper;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.integration.order.OrderFacade;

@Prod
@Stateless
public class OrderFacadeImpl implements OrderFacade {

	@Inject
	private Logger log;
	
	@Resource(mappedName=JmsConstants.JNDI_CONNECTIONFACTORY)
	private QueueConnectionFactory connectionFactory;
	@Resource(mappedName=JmsConstants.QUEUE_FLIGHTREQUEST_REQUEST)
	private Queue queue;
	
	private QueueConnection connection;
	private QueueSession session;

	@PostConstruct
	private void setup() {
		try {
			log.fine("initialize");
			
			connection = connectionFactory.createQueueConnection();
			session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			connection.start();
		} catch (JMSException e) {
			throw new RuntimeException(e);
		}
	}
	
	@PreDestroy
	private void cleanUp() {
		log.fine("destroy");
		
		JmsHelper.closeConnectionAndSession(connection, session);
	}
	
	@Override
	public void sendFlightRequest(FlightRequest request) throws Exception {
		log.fine(String.format("sendFlightRequest: %s ", request));
		
		SimpleDateFormat dateFormat = new SimpleDateFormat(JmsConstants.DATE_FORMAT);
		
		MapMessage message = session.createMapMessage();
		message.setLong("agentId", request.getId());
		message.setInt("noOfPassengers", request.getNoOfPassengers());
		message.setString("departureAirportCode", request.getDepartureAirportCode());
		message.setString("arrivalAirportCode", request.getArrivalAirportCode());
		message.setString("date", dateFormat.format(request.getDepartureTimeAsDate()));
		
		session.createSender(queue).send(message);
	}

}
