package se.plushogskolan.jetbroker.order.mvc;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import se.plushogskolan.jetbroker.order.domain.Plane;

public class EditPlaneBean {

	private long planeId;
	@NotBlank
	private String planeTypeCode;
	@Length(max = 300)
	private String description;

	public long getPlaneId() {
		return planeId;
	}

	public void setPlaneId(long planeId) {
		this.planeId = planeId;
	}

	public String getPlaneTypeCode() {
		return planeTypeCode;
	}

	public void setPlaneTypeCode(String planeTypeCode) {
		this.planeTypeCode = planeTypeCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void fillBeanWithPlaneValues(Plane plane) {
		setPlaneId(plane.getId());
		setDescription(plane.getDescription());
		setPlaneTypeCode(plane.getPlaneTypeCode());

	}

	public void fillPlaneWithBeanValues(Plane plane) {
		plane.setId(getPlaneId());
		plane.setPlaneTypeCode(getPlaneTypeCode());
		plane.setDescription(getDescription());

	}
}
