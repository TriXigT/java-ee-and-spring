package se.plushogskolan.jetbroker.order.service;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.repository.AirportRepository;
@Stateless
public class AirportServiceImpl implements AirportService{

	@Inject
	private Logger log;
	
	@Inject
	private AirportRepository repo;

	@Override
	public Airport getAirportByCode(String code) {
		log.fine(String.format("get airport by code [%s]", code));
		
		return repo.getAirport(code);
	}
	
	@Override
	public List<Airport> getAllAirports() {
		log.fine("get all airports");
		
		return repo.getAllAirports();
	}

	protected AirportRepository getRepository() {
		return repo;
	}

	protected void setRepository(AirportRepository repo) {
		this.repo = repo;
	}

}
