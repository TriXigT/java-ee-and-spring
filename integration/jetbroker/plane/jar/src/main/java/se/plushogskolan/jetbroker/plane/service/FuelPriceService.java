package se.plushogskolan.jetbroker.plane.service;

import javax.ejb.Local;

@Local
public interface FuelPriceService {
	double getFuelPrice();
	void updateFuelPrice(double fuelPrice);
}
