package se.plushogskolan.cdi.services;

import se.plushogskolan.cdi.annotations.InvocationCounter;
import se.plushogskolan.cdi.model.Car;


public interface CarService {

	@InvocationCounter
	Car getCar();

	void saveCar(Car car);
}
