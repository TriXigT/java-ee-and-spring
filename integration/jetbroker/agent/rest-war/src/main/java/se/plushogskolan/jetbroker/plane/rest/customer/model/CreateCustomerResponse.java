package se.plushogskolan.jetbroker.plane.rest.customer.model;

public class CreateCustomerResponse {
	private long id;

	public CreateCustomerResponse(long id) {
		setId(id);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}
