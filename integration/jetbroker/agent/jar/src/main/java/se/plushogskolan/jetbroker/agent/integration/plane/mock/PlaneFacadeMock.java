package se.plushogskolan.jetbroker.agent.integration.plane.mock;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import se.plushogskolan.jee.utils.cdi.Mock;
import se.plushogskolan.jetbroker.agent.domain.AirPort;
import se.plushogskolan.jetbroker.agent.domain.PlaneType;
import se.plushogskolan.jetbroker.agent.integration.plane.PlaneFacade;

@Stateless
@Mock
public class PlaneFacadeMock implements PlaneFacade {

	@Override
	public List<AirPort> getAllAirports() {

		List<AirPort> airports = new ArrayList<AirPort>();
		airports.add(new AirPort("GBG", "Gothenburg [Mocked]"));
		airports.add(new AirPort("STM", "Stockholm [Mocked]"));
		return airports;

	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {
		List<PlaneType> planeTypes = new ArrayList<PlaneType>();

		PlaneType type1 = new PlaneType();
		type1.setCode("B770");
		type1.setName("Boeing 770 [Mocked]");
		type1.setFuelConsumptionPerKm(200);
		type1.setMaxNoOfPassengers(300);
		type1.setMaxRangeKm(700);
		type1.setMaxSpeedKmH(980);
		type1.setFuelConsumptionPerKm(34);
		planeTypes.add(type1);

		PlaneType type2 = new PlaneType();
		type2.setCode("A450");
		type2.setName("Airbus 450 [Mocked]");
		type2.setFuelConsumptionPerKm(140);
		type2.setMaxNoOfPassengers(270);
		type2.setMaxRangeKm(600);
		type2.setMaxSpeedKmH(680);
		type2.setFuelConsumptionPerKm(22);
		planeTypes.add(type2);

		return planeTypes;
	}

}
