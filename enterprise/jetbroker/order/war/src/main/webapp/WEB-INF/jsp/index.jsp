<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>

<html>
<head>
<link href="<%=request.getContextPath()%>/style/common.css" type="text/css" rel="stylesheet" />
<link href="<%=request.getContextPath()%>/style/theme_stylish.css" type="text/css" rel="stylesheet" />
<c:set var="dateFormat"><spring:message code="index.flightRequest.dateFormat"/></c:set>

</head>
<body>
	<div class="container">
		<jsp:include page="header.jsp" />
		<div class="content">
			<div class="content_container" id="flight_request_container">
				<img src="<%=request.getContextPath()%>/images/flightrequest.png"/><h2><spring:message code="index.flightRequests.title"/></h2>
				<hr>
				<h3><spring:message code="index.flightRequests.title"/></h3>
				<c:choose>
					<c:when test="${!empty flightRequestBeans}">
					<table class="datatable">
						<tr>
							<th><spring:message code="global.id"/></th>
							<th><spring:message code="flightRequest.status"/></th>
							<th><spring:message code="flightRequest.departureAirport"/></th>
							<th><spring:message code="flightRequest.arrivalAirport"/></th>
							<th><spring:message code="flightRequest.departureDate"/></th>
							<th><spring:message code="flightRequest.noOfPassengers"/></th>
							<th><spring:message code="flightRequest.offeredPrice"/></th>
							<th><spring:message code="flightRequest.offeredPlane"/></th>
							<th><spring:message code="global.edit"/></th>
						</tr>
						<c:forEach items="${flightRequestBeans}" var="flightRequestBean">
						<tr>
							<td>${flightRequestBean.flightRequest.id}</td>
							<td>${flightRequestBean.flightRequest.status.niceName}</td>
							<td>${flightRequestBean.departureAirport.niceName}</td>
							<td>${flightRequestBean.arrivalAirport.niceName}</td>
							<td><joda:format value="${flightRequestBean.flightRequest.date}" pattern="${dateFormat}"/></td>
							<td>${flightRequestBean.flightRequest.passengers}</td>
							<td><spring:message code="flightRequest.offeredPrice.pattern" arguments="${flightRequestBean.flightRequest.offeredPrice}" htmlEscape="false"/></td>
							<td>
								<c:if test="${!empty flightRequestBean.planeType}">
									${flightRequestBean.planeType.niceName}
								</c:if>
							</td>							
							<td><a href="<%=request.getContextPath()%>/editFlightRequest/${flightRequestBean.flightRequest.id}.html"><img alt="<spring:message code="global.edit"/>" src="<%=request.getContextPath()%>/images/edit.png"/></a></td>
						</tr>
						</c:forEach>
					</table>
				</c:when>
				<c:otherwise>
					<span><spring:message code="index.flightRequests.noRequests"></spring:message></span>
				</c:otherwise>
			</c:choose>
			</div>
			
			<div class="content_container" id="generic_container">
			<img src="<%=request.getContextPath()%>/images/customer.png"/><h2><spring:message code="global.planes.title"/></h2>
				<hr>
				<a href="<%=request.getContextPath()%>/editPlane/0.html" class="button" id="create_customer_button"><spring:message code="index.plane.addButton"/></a>
				<br>
				<c:choose>
					<c:when test="${!empty planeBeans}">
					<table class="datatable">
						<tr>
							<th><spring:message code="global.id"/></th>
							<th><spring:message code="global.plane.planeTypeCode"/></th>
							<th><spring:message code="global.plane.name"/></th>
							<th><spring:message code="global.plane.maxPassengers"/></th>
							<th><spring:message code="global.plane.range"/></th>
							<th><spring:message code="global.plane.maxSpeed"/></th>
							<th><spring:message code="global.plane.description"/></th>
							<th><spring:message code="global.edit"/></th>
						</tr>
						
						<c:forEach items="${planeBeans}" var="planeBean">
						<tr>
							<td>${planeBean.plane.id}</td>
							<td>${planeBean.plane.planeTypeCode}</td>
							<td>${planeBean.planeType.name}</td>
							<td>${planeBean.planeType.maxNoOfPassengers}</td>
							<td>${planeBean.planeType.maxRangeKm}</td>
							<td>${planeBean.planeType.maxSpeedKmH}</td>
							<td>${planeBean.plane.description}</td>
							<td><a href="<%=request.getContextPath()%>/editPlane/${planeBean.plane.id}.html"><img alt="<spring:message code="global.edit"/>" src="<%=request.getContextPath()%>/images/edit.png"/></a></td>
						</tr>
						</c:forEach>
					</table>
				</c:when>
				<c:otherwise>
					<span><spring:message code="global.plane.noPlane"></spring:message></span>
				</c:otherwise>
			</c:choose>				
			</div>
			
			<div class="content_container" id="generic_container">
			<img src="<%=request.getContextPath()%>/images/gas.png"/><h2><spring:message code="global.fuelPrice"/></h2>
				<hr>
				<c:choose>
					<c:when test="${!empty fuelPrice}">
						<span>
							<span><spring:message code="global.fuelPrice.pattern" arguments="${fuelPrice}" htmlEscape="false"/></span>
						</span>
					</c:when>
					<c:otherwise>
						<span><spring:message code="global.noFuelPriceString"></spring:message></span>
					</c:otherwise>
				</c:choose>				
			</div>
		</div>
	</div>
</body>

</html>