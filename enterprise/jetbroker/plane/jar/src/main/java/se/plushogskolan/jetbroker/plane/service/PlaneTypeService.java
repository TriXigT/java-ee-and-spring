package se.plushogskolan.jetbroker.plane.service;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.plane.domain.PlaneType;

@Local
public interface PlaneTypeService {

	PlaneType getPlaneType(long id);

	PlaneType createPlaneType(PlaneType planeType);

	void updatePlaneType(PlaneType planeType);

	List<PlaneType> getAllPlaneTypes();

}
