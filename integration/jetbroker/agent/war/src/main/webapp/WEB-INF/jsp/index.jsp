<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
<link href="<%=request.getContextPath()%>/style/common.css"
	type="text/css" rel="stylesheet" />

<style type="text/css">
</style>

</head>
<body>

	<jsp:include page="header.jsp" />


	<h2 class="underline">
		<img src="<%=request.getContextPath()%>/images/flightrequest.png">
		Flight requests
	</h2>

	<p>
		<a class="button"
			href="<%=request.getContextPath()%>/editFlightRequest/0.html">Create
			new flight request</a>
	</p>


	<c:choose>
		<c:when test="${empty createdRequests }">
			<p>There are no flight requests waiting to be processed.
		</c:when>
		<c:otherwise>
			<h3>Flight requests waiting to be processed</h3>

			<table class="dataTable">
				<tr>
					<th>ID</th>
					<th>Status</th>
					<th>Departure airport</th>
					<th>Arrival airport</th>
					<th>Date</th>
					<th>Customer</th>
					<th>No of passengers</th>
					<th>Edit</th>
				</tr>
				<c:forEach items="${createdRequests}" var="flightRequest">
					<tr>
						<td>${flightRequest.id}</td>
						<td>${flightRequest.requestStatus.niceName}</td>
						<td>${flightRequest.departureAirportCode}</td>
						<td>${flightRequest.arrivalAirportCode}</td>
						<td><fmt:formatDate value="${flightRequest.departureTimeAsDate}" type="both" dateStyle="medium" timeStyle="short"/></td>
						<td>${flightRequest.customer.fullName}</td>
						<td>${flightRequest.noOfPassengers}</td>
						<td><a
							href="<%= request.getContextPath() %>/editFlightRequest/${flightRequest.id}.html"><img src="images/edit.png"></a>
							
							<c:if test="${useMock}">
								<c:choose>
									<c:when test="${flightRequest.requestStatus == 'CREATED'}">
										<a href="<%= request.getContextPath() %>/mockConfirmation/${flightRequest.id}.html">Send confirmation</a>
									</c:when>
									<c:otherwise>
										<a href="<%= request.getContextPath() %>/mockOffer/${flightRequest.id}.html">Send offer</a>
										<a href="<%= request.getContextPath() %>/mockRejection/${flightRequest.id}.html">Reject</a>
									
									</c:otherwise>
								</c:choose>
							</c:if>
							</td>
					</tr>
				</c:forEach>
			</table>

		</c:otherwise>
	</c:choose>


	<c:choose>
		<c:when test="${empty offeredRequests }">
			<p>There are no flight requests with an offer.</p>
		</c:when>
		<c:otherwise>
			<h3>Flight requests with offer</h3>

			<table class="dataTable">
				<tr>
					<th>ID</th>
					<th>Status</th>
					<th>Departure airport</th>
					<th>Arrival airport</th>
					<th>Date</th>
					<th>Customer</th>
					<th>No of passengers</th>
					<th>Plane type</th>
					<th>Offered price</th>
					<c:if test="${useMock}">
						<th></th>
					</c:if>
				</tr>
				<c:forEach items="${offeredRequests}" var="wrapper">
					<tr>
						<td>${wrapper.flightRequest.id}</td>
						<td>${wrapper.flightRequest.requestStatus.niceName}</td>
						<td>${wrapper.flightRequest.departureAirportCode}</td>
						<td>${wrapper.flightRequest.arrivalAirportCode}</td>
						<td><fmt:formatDate value="${wrapper.flightRequest.departureTimeAsDate}" type="both" dateStyle="medium" timeStyle="short"/></td>
						<td>${wrapper.flightRequest.customer.fullName}</td>
						<td>${wrapper.flightRequest.noOfPassengers}</td>
						<td>${wrapper.planeType.name} (${wrapper.planeType.maxNoOfPassengers} seats)</td>
						<td><fmt:formatNumber value="${wrapper.flightRequest.offer.offeredPrice}" maxFractionDigits="0" /> SEK</td>
						<c:if test="${useMock}">						
							<td>
								<a href="<%= request.getContextPath() %>/mockOffer/${wrapper.flightRequest.id}.html">Send offer</a>
								<a href="<%= request.getContextPath() %>/mockRejection/${wrapper.flightRequest.id}.html">Reject</a>
							</td>
						</c:if>
					</tr>
				</c:forEach>
			</table>
		</c:otherwise>
	</c:choose>

<c:choose>
		<c:when test="${empty rejectedRequests }">
			<p>There are no rejected flight requests.
		</c:when>
		<c:otherwise>
			<h3>Rejected flight requests</h3>

			<table class="dataTable">
				<tr>
					<th>ID</th>
					<th>Status</th>
					<th>Departure airport</th>
					<th>Arrival airport</th>
					<th>Date</th>
					<th>Customer</th>
					<th>No of passengers</th>
					<c:if test="${useMock}">
						<th></th>
					</c:if>
				</tr>
				<c:forEach items="${rejectedRequests}" var="flightRequest">
					<tr>
						<td>${flightRequest.id}</td>
						<td>${flightRequest.requestStatus.niceName}</td>
						<td>${flightRequest.departureAirportCode}</td>
						<td>${flightRequest.arrivalAirportCode}</td>
						<td><fmt:formatDate value="${flightRequest.departureTimeAsDate}" type="both" dateStyle="medium" timeStyle="short"/></td>
						<td>${flightRequest.customer.fullName}</td>
						<td>${flightRequest.noOfPassengers}</td>
						<c:if test="${useMock}">						
							<td>
							<a href="<%= request.getContextPath() %>/mockOffer/${flightRequest.id}.html">Send offer</a>
							<a href="<%= request.getContextPath() %>/mockRejection/${flightRequest.id}.html">Reject</a>
							</td>
						</c:if>
					</tr>
				</c:forEach>
			</table>

		</c:otherwise>
	</c:choose>


	<h2 class="underline">
		<img src="<%=request.getContextPath()%>/images/customer.png">
		Customers
	</h2>

	<p>
		<a class="button"
			href="<%=request.getContextPath()%>/editCustomer/0.html">Create
			new customer</a>
	</p>

	<table class="dataTable">
		<tr>
			<th>ID</th>
			<th>First name</th>
			<th>Last name</th>
			<th>Company</th>
			<th>Edit</th>
		</tr>
		<c:forEach items="${customers}" var="customer">
			<tr>
				<td>${customer.id}</td>
				<td>${customer.firstName}</td>
				<td>${customer.lastName}</td>
				<td>${customer.company}</td>
				<td><a
					href="<%= request.getContextPath() %>/editCustomer/${customer.id}.html"><img src="images/edit.png" ></a></td>
			</tr>
		</c:forEach>
	</table>

</body>

</html>