
package webservices.wsimport.autogenerated;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebService(name = "GreetingWebService", targetNamespace = "http://wsgen.webservices/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface GreetingWebService {


    /**
     * 
     * @param arg0
     * @return
     *     returns java.lang.String
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "greetSomeone", targetNamespace = "http://wsgen.webservices/", className = "webservices.wsimport.autogenerated.GreetSomeone")
    @ResponseWrapper(localName = "greetSomeoneResponse", targetNamespace = "http://wsgen.webservices/", className = "webservices.wsimport.autogenerated.GreetSomeoneResponse")
    @Action(input = "http://wsgen.webservices/GreetingWebService/greetSomeoneRequest", output = "http://wsgen.webservices/GreetingWebService/greetSomeoneResponse")
    public String greetSomeone(
        @WebParam(name = "arg0", targetNamespace = "")
        String arg0);

}
