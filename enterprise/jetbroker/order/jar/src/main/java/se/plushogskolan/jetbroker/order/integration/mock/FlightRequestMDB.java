package se.plushogskolan.jetbroker.order.integration.mock;

import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.joda.time.DateTime;

import se.plushogskolan.jee.utils.annotation.Mock;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.Status;
import se.plushogskolan.jetbroker.order.integration.event.NewFlightRequestEvent;
import se.plushogskolan.jetbroker.order.service.FlightRequestService;

public class FlightRequestMDB {
	@Inject
	FlightRequestService flightRequestService;
	
	public void receiveFlightRequest(@Observes @Mock NewFlightRequestEvent event) {
		FlightRequest flightRequest = new FlightRequest(event.getFlightRequestId(), Status.NEW, "GBG", "STM", DateTime.now(), "",33,0);
		flightRequestService.createFlightRequest(flightRequest);
	}
}
