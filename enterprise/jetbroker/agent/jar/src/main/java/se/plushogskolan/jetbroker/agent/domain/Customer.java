package se.plushogskolan.jetbroker.agent.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import se.plushogskolan.jee.utils.domain.IdHolder;
/**
 * A Customer is a person renting an airplane, the Customer must have a firstName,
 * lastName, email and company.
 * @author Pär
 */
@Entity
@Table(uniqueConstraints={@UniqueConstraint(columnNames = {"email"})})
public class Customer implements IdHolder{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@NotBlank
	private String firstName;
	@NotBlank
	private String lastName;
	@Email
	private String email;
	@NotBlank
	private String company;
	
	public Customer() {
	}
	
	public Customer(String firstName, String lastName) {
		this(firstName,lastName, null, null);
	}
	
	public Customer(String firstName, String lastName, String email, String company) {
		setFirstName(firstName);
		setLastName(lastName);
		setEmail(email);
		setCompany(company);
	}

	@Override
	public long getId() {
		return id;
	}

	@Override
	public void setId(long id) {
		this.id = id;
	}
	
	
	public String getNiceName() {
		return String.format("%s %s", getFirstName(), getLastName());
	}


	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		return true;
	}


	
	
}
