package se.plushogskolan.jetbroker.order.integration.plane.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.PlaneWebService;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.PlaneWebServiceImplService;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.WsAirport;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.WsPlaneType;
import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.integration.plane.PlaneIntegrationFacade;

@Stateless
@Prod
public class PlaneIntegrationFacadeImpl implements PlaneIntegrationFacade {

	@Inject
	private Logger log;
	
	@Override
	public List<Airport> getAllAirports() {
		log.fine("get airports from plane using SOAP");
		
		List<Airport> airports = new ArrayList<Airport>();
		
		PlaneWebServiceImplService webService = new PlaneWebServiceImplService();
		PlaneWebService port = webService.getPlaneWebServicePort();

		for(WsAirport wsAirport : port.getAirports()) {
			Airport airport = new Airport(wsAirport.getCode(), wsAirport.getName(), wsAirport.getLatitude(), wsAirport.getLongitude());
			airports.add(airport);
		}
		
		return airports;
	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {
		log.fine("get plane types from plane using SOAP");
		
		List<PlaneType> planeTypes = new ArrayList<PlaneType>();
		
		PlaneWebServiceImplService webService = new PlaneWebServiceImplService();
		PlaneWebService port = webService.getPlaneWebServicePort();
		
		for(WsPlaneType wsPlaneType : port.getPlaneTypes()) {
			planeTypes.add(new PlaneType(
					wsPlaneType.getCode(), 
					wsPlaneType.getName(), 
					wsPlaneType.getMaxNoOfPassengers(), 
					wsPlaneType.getMaxRangeKm(), 
					wsPlaneType.getMaxSpeedKmH(), 
					wsPlaneType.getFuelConsumptionPerKm()));
		}
		
		return planeTypes;
	}

}
