package se.plushogskolan.jetbroker.order.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.repository.PlaneTypeRepository;
import se.plushogskolan.jetbroker.order.service.PlaneTypeService;
@Stateless
public class PlaneTypeServiceImpl implements PlaneTypeService{
	
	@Inject
	PlaneTypeRepository repo;

	@Override
	public List<PlaneType> getAllPlaneTypes() {
		return repo.getAllPlaneTypes();
	}
	
	protected void setRepositoryJUnit(PlaneTypeRepository repo) {
		this.repo = repo;
	}
	
	protected PlaneTypeRepository getRepositoryJUnit() {
		return repo;
	}

	@Override
	public PlaneType getPlaneTypeByCode(String code) {
		return repo.getPlaneTypeByCode(code);
	}

}
