package se.plushogskolan.jetbroker.plane.integration.plane.mock;

import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Mock;
import se.plushogskolan.jetbroker.plane.integration.plane.PlaneIntegrationFacade;

@Mock
@Stateless
public class PlaneIntegrationFacadeMock implements PlaneIntegrationFacade {

	@Inject
	Logger log;

	@Override
	public void broadcastNewFuelPrice(double fuelPrice) {
		log.info("MOCK: broadcastNewFuelPrice. New price is " + fuelPrice);
	}

	@Override
	public void broadcastAirportsChanged() {
		log.info("MOCK: Airports changed");
	}

	@Override
	public void broadcastPlaneTypesChanged() {
		log.info("MOCK: Plane types changed");

	}

}
