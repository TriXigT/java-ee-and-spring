package se.plushogskolan.jetbroker.order.mvc;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.Offer;
import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.domain.Status;
import se.plushogskolan.jetbroker.order.mvc.bean.FlightRequestBean;
import se.plushogskolan.jetbroker.order.mvc.bean.OfferBean;
import se.plushogskolan.jetbroker.order.mvc.bean.PlaneBean;
import se.plushogskolan.jetbroker.order.service.AirportService;
import se.plushogskolan.jetbroker.order.service.FlightRequestService;
import se.plushogskolan.jetbroker.order.service.FlightUtilityService;
import se.plushogskolan.jetbroker.order.service.FuelPriceService;
import se.plushogskolan.jetbroker.order.service.OfferService;
import se.plushogskolan.jetbroker.order.service.PlaneService;
import se.plushogskolan.jetbroker.order.service.PlaneTypeService;

@Controller
public class EditFlightRequestController {
	private static Logger log = Logger.getLogger(EditFlightRequestController.class.getName());
	
	@Inject
	private PlaneTypeService planeTypeService;
	@Inject
	private PlaneService planeService;
	@Inject
	private FuelPriceService fuelPriceService;
	@Inject
	private FlightRequestService flightRequestService;
	@Inject
	private AirportService airportService;
	@Inject
	private FlightUtilityService flightUtilityService;
	@Inject
	private OfferService offerService;
	
	@RequestMapping(value="editFlightRequest/{id}.html", method = RequestMethod.GET)
	public ModelAndView getEdit(@PathVariable long id) {
		log.fine("GET FlightRequest");
		
		if(id==0) {
			return new ModelAndView("redirect:/index.html");
		}
		FlightRequest flightRequest = flightRequestService.getFlightRequest(id);
		
		return getModelAndView(flightRequest, new OfferBean(new Offer(flightRequest.getId())));
	}
	
	@RequestMapping(value="rejectFlightRequest/{id}.html", method = RequestMethod.GET)
	public ModelAndView getReject(@PathVariable long id) {
		log.fine("reject request");
		
		FlightRequest flightRequest = flightRequestService.getFlightRequest(id);
		
		flightRequest.setStatus(Status.REJECTED);
		flightRequestService.updateFlightRequest(flightRequest);
		
		return new ModelAndView("redirect:/index.html");
	}
	
	
	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView handleSubmit(@Valid OfferBean offerBean, BindingResult errors) {
		log.fine("handle submit");
		if(errors.hasErrors()) {
			log.fine("submit had errors");
			return getModelAndView(flightRequestService.getFlightRequest(offerBean.getOffer().getFlightRequestId()), offerBean);
		}
		Offer offer = offerBean.getOffer();
		
		offerService.createOffer(offer);
		FlightRequest flightRequest = flightRequestService.getFlightRequest(offerBean.getOffer().getFlightRequestId());
		flightRequest.setOfferedPrice(offer.getPrice());
		flightRequest.setPlaneCode(planeService.getPlane(offer.getPlaneId()).getPlaneTypeCode());
		flightRequest.setStatus(Status.OFFER_SENT);
		flightRequestService.updateFlightRequest(flightRequest);
		
		return new ModelAndView("redirect:/index.html");
	}
	
	public ModelAndView getModelAndView(FlightRequest flightRequest, OfferBean offerBean ) {
		ModelAndView mav = new ModelAndView("editFlightRequest");
		
		FlightRequestBean flightRequestBean = getAssembledFlightRequestBean(flightRequest);
		List<PlaneBean> planeBeans = getAssembledPlaneBeans(flightRequest);
		
		mav.addObject("offerBean", offerBean);
		mav.addObject("flightRequestBean", flightRequestBean);
		mav.addObject("planeBeans", planeBeans);
		mav.addObject("fuelPrice", fuelPriceService.getFuelPrice());
		
		return mav;
	}
	
	/**
	 * Utility method for assembling EditPlaneBean which are wrappers holding a Plane and
	 * a PlaneType, fuelprice, time, etc
	 * 
	 * @return a List of IndexPlaneBeans
	 */
	private List<PlaneBean> getAssembledPlaneBeans(FlightRequest flightRequest) {
		Airport arrivalAirport = airportService.getAirportByCode(flightRequest.getArrivalAirportCode());
		Airport departureAirport = airportService.getAirportByCode(flightRequest.getDepartureAirportCode());
		
		List<PlaneBean> planeBeans = new ArrayList<PlaneBean>();
		
		for(Plane plane : planeService.getAllPlanes()) {
			PlaneType planeType = planeTypeService.getPlaneTypeByCode(plane.getPlaneTypeCode());
			
			int fuelCost = (int) flightUtilityService.getFuelPriceForFlight(arrivalAirport, departureAirport, planeType);
			int travelTime = (int) flightUtilityService.getTimeForFlightInMinutes(arrivalAirport, departureAirport, planeType);
			
			PlaneBean bean = new PlaneBean(plane, planeType, fuelCost, travelTime);
			planeBeans.add(bean);
		}
		
		return planeBeans;
	}
	
	/**
	 * Utility method for assembling a FlightRequestBean which is a wrapper holding a FlightRequest,
	 * a PlaneType, Airports etc
	 * 
	 * @return a List of FlightRequestBeans
	 */
	private FlightRequestBean getAssembledFlightRequestBean(FlightRequest flightRequest) {
			Airport arrivalAirport = airportService.getAirportByCode(flightRequest.getArrivalAirportCode());
			Airport departureAirport = airportService.getAirportByCode(flightRequest.getDepartureAirportCode());
			
			int distance = (int) flightUtilityService.getDistanceForFlight(arrivalAirport, departureAirport);
			
			FlightRequestBean bean = new FlightRequestBean(flightRequest, 
					planeTypeService.getPlaneTypeByCode(flightRequest.getPlaneTypeCode()),
					airportService.getAirportByCode(flightRequest.getDepartureAirportCode()),
					airportService.getAirportByCode(flightRequest.getArrivalAirportCode()),
					distance);
		
		return bean;
	}

}
