package se.plushogskolan.jee.utils.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;

public class ArrivalAndDepartureAirportValidator implements
		ConstraintValidator<ArrivalAndDepartureAirport, ArrivalAndDepartureAirportHolder> {

	
	@Override
	public void initialize(ArrivalAndDepartureAirport object) {
	}

	@Override
	public boolean isValid(ArrivalAndDepartureAirportHolder holder, ConstraintValidatorContext context) {
		if((holder.getArrivalAirportCode() != null && holder.getDepartureAirportCode() != null) 
				&& !holder.getArrivalAirportCode().equalsIgnoreCase(holder.getDepartureAirportCode())) {
			return true;
		}
		
		return false;
	}

}
