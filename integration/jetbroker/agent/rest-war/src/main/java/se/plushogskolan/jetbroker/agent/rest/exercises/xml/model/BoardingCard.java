package se.plushogskolan.jetbroker.agent.rest.exercises.xml.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * A class that should be transformed to xml using JAXB.
 */
@XmlRootElement
public class BoardingCard {
	private String date;
	private int boardingCardNo;
	private String departureAirport;
	private String arrivalAirport;

	@XmlElement(name = "departureDate")
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getBoardingCardNo() {
		return boardingCardNo;
	}

	public void setBoardingCardNo(int boardingCardNo) {
		this.boardingCardNo = boardingCardNo;
	}

	public String getDepartureAirport() {
		return departureAirport;
	}

	public void setDepartureAirport(String departureAirport) {
		this.departureAirport = departureAirport;
	}

	public String getArrivalAirport() {
		return arrivalAirport;
	}

	public void setArrivalAirport(String arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}

}
