package se.plushogskolan.jetbroker.agent.mvc;

import java.util.Locale;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.agent.mvc.bean.EditCustomerBean;
import se.plushogskolan.jetbroker.agent.service.CustomerService;

@Controller
@RequestMapping("editCustomer/{id}.html")
public class EditCustomerController {
	private static Logger log = Logger.getLogger(EditCustomerController.class.getName());
	
	@Inject
	private CustomerService service;
	
	@Autowired
	private MessageSource messageSource;
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView get(@PathVariable long id) {
		log.fine("GET customer");
		ModelAndView mav = new ModelAndView("editCustomer");
		EditCustomerBean bean = new EditCustomerBean();
		
		if(id>0) {
			bean.setCustomer(service.getCustomer(id));
			log.fine(String.format("get customer with ID %d", id));
		}
		
		mav.addObject("editCustomerBean", bean);
		
		return mav;
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView handleSubmit(@Valid EditCustomerBean bean, BindingResult errors, Locale locale) {
		if(errors.hasErrors()) {
			log.fine("customer data not valid");
			ModelAndView mav = new ModelAndView("editCustomer");
			mav.addObject("editCustomerBean", bean);
			return mav;
		}
		
		if(bean.getCustomer().getFirstName().equals("i18n")) {
			log.fine(messageSource.getMessage("i18n.test.message", null, locale));
		} else if(bean.getCustomer().getId() > 0) {
			service.updateCustomer(bean.getCustomer());
			
			log.fine(String.format("update customer with ID %d", bean.getCustomer().getId()));
		} else {
			service.createCustomer(bean.getCustomer());
			
			log.fine(String.format("add new customer with ID %d", bean.getCustomer().getId()));
		}
		return new ModelAndView("redirect:/index.html");
	}
}