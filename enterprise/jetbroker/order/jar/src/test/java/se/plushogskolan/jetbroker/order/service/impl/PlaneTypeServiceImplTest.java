package se.plushogskolan.jetbroker.order.service.impl;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.repository.PlaneTypeRepository;

public class PlaneTypeServiceImplTest {

	PlaneTypeServiceImpl service;
	PlaneTypeRepository repo;
	
	PlaneType mPlaneType;
	
	
	@Before
	public void setup() {
		service = new PlaneTypeServiceImpl();
		repo = createMock(PlaneTypeRepository.class);
		mPlaneType = TestFixture.getValidPlaneType();

		service.setRepositoryJUnit(repo);
	}
	
	@Test
	public void createAndFindTest() {
		expect(repo.getPlaneTypeByCode(mPlaneType.getCode())).andReturn(mPlaneType);
		replay(repo);
		assertEquals("find", mPlaneType, service.getPlaneTypeByCode(mPlaneType.getCode()));
		verify(repo);
	}
	
	@Test
	public void getAllPlaneTypesTest() {
		List<PlaneType> planeTypeList = new ArrayList<PlaneType>();
		planeTypeList.add(mPlaneType);
		expect(repo.getAllPlaneTypes()).andReturn(planeTypeList);
		replay(repo);
		assertEquals("get all planetypes", planeTypeList, service.getAllPlaneTypes());
		verify(repo);
	}
	
}
