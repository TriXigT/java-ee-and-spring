package webservices.bindingstyles.document;

import javax.jws.WebService;
import javax.xml.ws.Endpoint;

import webservices.bindingstyles.Car;
import webservices.bindingstyles.CarRepository;
import webservices.bindingstyles.RegNumber;

/**
 * Implement the CarService web service.
 */
@WebService(endpointInterface="webservices.bindingstyles.document.CarServiceDocumentStyle" )
public class CarServiceDocumentStyleImpl implements CarServiceDocumentStyle {

	private CarRepository repo = new CarRepository();

	public static void main(String[] args) {
		Endpoint.publish("http://localhost:8180/ws/carServiceDocumentStyle", new CarServiceDocumentStyleImpl());
	}

	@Override
	public Car getCar(RegNumber regNo) {
		return repo.getCar(regNo);
	}

}
