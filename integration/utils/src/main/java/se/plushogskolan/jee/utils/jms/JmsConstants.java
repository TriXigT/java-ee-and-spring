package se.plushogskolan.jee.utils.jms;

public class JmsConstants {

	public static final String JNDI_CONNECTIONFACTORY = "java:/ConnectionFactory";
	public static final String DESTINATION_TYPE_QUEUE = "javax.jms.Queue";
	public static final String DESTINATION_TYPE_TOPIC = "javax.jms.Topic";

	public static final String QUEUE_FLIGHTREQUEST_REQUEST = "java:jboss/exported/jms/queue/flightRequestQueue";
	public static final String QUEUE_FLIGHTREQUEST_RESPONSE = "java:jboss/exported/jms/queue/flightRequestResponseQueue";
	public static final String TOPIC_PLANE_BROADCAST = "java:jboss/exported/jms/topic/planeBroadcastTopic";

	public static final String MSGTYPE_FLIGHTREQUEST_RESPONSE_CONFIRMATION = "Confirmation";
	public static final String MSGTYPE_FLIGHTREQUEST_RESPONSE_REJECTION = "Rejection";
	public static final String MSGTYPE_FLIGHTREQUEST_RESPONSE_OFFER = "Offer";

	public static final String MSGTYPE_PLANEBROADCAST_AIRPORTSCHANGED = "AirportsChanged";
	public static final String MSGTYPE_PLANEBROADCAST_FUELPRICECHANGED = "FuelPriceChanged";
	public static final String MSGTYPE_PLANEBROADCAST_PLANETYPESCHANGED = "PlaneTypesChanged";

	public static final String DATE_FORMAT = "dd-MM-yyyy HH:mm";
}
