package se.plushogskolan.jetbroker.agent.rest.exercises.xml.xmlhandlers;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import se.plushogskolan.jetbroker.agent.rest.exercises.xml.model.BoardingCard;

public class JaxbTransformer {

	public static String transformToXml(BoardingCard boardingCard) throws Exception {

		JAXBContext ctx = JAXBContext.newInstance(BoardingCard.class);
		Marshaller marshaller = ctx.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		
		StringWriter sw = new StringWriter();
		
		marshaller.marshal(boardingCard, sw);
		
		return sw.toString();
	}

}
