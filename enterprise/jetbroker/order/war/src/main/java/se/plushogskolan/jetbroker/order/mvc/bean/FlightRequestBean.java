package se.plushogskolan.jetbroker.order.mvc.bean;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.PlaneType;

public class FlightRequestBean {
	private FlightRequest flightRequest;
	private PlaneType planeType;
	private Airport departureAirport;
	private Airport arrivalAirport;
	
	private int travelDistance;
	
	public FlightRequestBean() {
		
	}
	
	public FlightRequestBean(FlightRequest flightRequest, PlaneType planeType, Airport departureAirport, Airport arrivalAirport) {
		this(flightRequest, planeType, departureAirport, arrivalAirport, 0);
	}
	
	public FlightRequestBean(FlightRequest flightRequest, PlaneType planeType, Airport departureAirport, Airport arrivalAirport, int travelDistance) {
		this.flightRequest = flightRequest;
		this.planeType = planeType;
		this.departureAirport = departureAirport;
		this.arrivalAirport = arrivalAirport;
		this.travelDistance = travelDistance;
	}

	public FlightRequest getFlightRequest() {
		return flightRequest;
	}

	public void setFlightRequest(FlightRequest flightRequest) {
		this.flightRequest = flightRequest;
	}

	public PlaneType getPlaneType() {
		return planeType;
	}

	public void setPlaneType(PlaneType planeType) {
		this.planeType = planeType;
	}

	public Airport getDepartureAirport() {
		return departureAirport;
	}

	public void setDepartureAirport(Airport departureAirport) {
		this.departureAirport = departureAirport;
	}

	public Airport getArrivalAirport() {
		return arrivalAirport;
	}

	public void setArrivalAirport(Airport arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}

	public int getTravelDistance() {
		return travelDistance;
	}

	public void setTravelDistance(int travelDistance) {
		this.travelDistance = travelDistance;
	}
}
