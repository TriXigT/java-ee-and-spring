package se.plushogskolan.jetbroker.agent.rest.exercises.xml.xmlhandlers;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import se.plushogskolan.jee.utils.xml.XmlUtil;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;

public class DomBuilder {

	public static String buildXmlUsingDom(FlightRequest flightRequest) throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();

		Document doc = builder.newDocument();

		Element flightRequestElement = doc.createElement("flightrequest");
		doc.appendChild(flightRequestElement);

		Element arrival = doc.createElement("arrivalAirportCode");
		Element departure = doc.createElement("departureAirportCode");
		Element noOfPassengers = doc.createElement("noOfPassengers");
		Element status = doc.createElement("status");
		Attr idAttr = doc.createAttribute("id");

		flightRequestElement.appendChild(arrival);
		flightRequestElement.appendChild(departure);
		flightRequestElement.appendChild(noOfPassengers);
		flightRequestElement.appendChild(status);
		flightRequestElement.setAttributeNode(idAttr);

		arrival.setTextContent(flightRequest.getArrivalAirportCode());
		departure.setTextContent(flightRequest.getDepartureAirportCode());
		noOfPassengers.setTextContent(String.valueOf(flightRequest.getNoOfPassengers()));
		status.setTextContent(flightRequest.getRequestStatus().name());
		idAttr.setTextContent(String.valueOf(flightRequest.getId()));
		
		return XmlUtil.convertXmlDocumentToString(doc);
	}
}
