package se.plushogskolan.jetbroker.plane.rest.planetype.model;

import se.plushogskolan.jetbroker.plane.domain.PlaneType;

public class CreatePlaneTypeRequest {
	private String code;
	private String name;
	private int maxNoOfPassengers;
	private int maxRangeKm;
	private int maxSpeedKmH;
	private double fuelConsumptionPerKm;
	
	public PlaneType buildPlaneType() {
		PlaneType planeType = new PlaneType();
		planeType.setCode(code);
		planeType.setName(name);
		planeType.setMaxNoOfPassengers(maxNoOfPassengers);
		planeType.setMaxRangeKm(maxRangeKm);
		planeType.setMaxSpeedKmH(maxSpeedKmH);
		planeType.setFuelConsumptionPerKm(fuelConsumptionPerKm);
		
		return planeType;
		
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMaxNoOfPassengers() {
		return maxNoOfPassengers;
	}

	public void setMaxNoOfPassengers(int maxNoOfPassengers) {
		this.maxNoOfPassengers = maxNoOfPassengers;
	}

	public int getMaxRangeKm() {
		return maxRangeKm;
	}

	public void setMaxRangeKm(int maxRangeKm) {
		this.maxRangeKm = maxRangeKm;
	}

	public int getMaxSpeedKmH() {
		return maxSpeedKmH;
	}

	public void setMaxSpeedKmH(int maxSpeedKmH) {
		this.maxSpeedKmH = maxSpeedKmH;
	}

	public double getFuelConsumptionPerKm() {
		return fuelConsumptionPerKm;
	}

	public void setFuelConsumptionPerKm(double fuelConsumptionPerKm) {
		this.fuelConsumptionPerKm = fuelConsumptionPerKm;
	}
}
