package se.plushogskolan.jetbroker.order.repository;

import java.util.List;

import se.plushogskolan.jetbroker.order.domain.Airport;

public interface AirportRepository{
	Airport getAirportByCode(String code);
	
	List<Airport> getAllAirports();
}
