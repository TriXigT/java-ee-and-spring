package se.plushogskolan.jetbroker.order.service;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.order.domain.Airport;

@Local
public interface AirportService {
	Airport getAirportByCode(String code);

	List<Airport> getAllAirports();
}
