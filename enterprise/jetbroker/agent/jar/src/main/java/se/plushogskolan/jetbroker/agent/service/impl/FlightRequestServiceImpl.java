package se.plushogskolan.jetbroker.agent.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.Status;
import se.plushogskolan.jetbroker.agent.repository.FlightRequestRepository;
import se.plushogskolan.jetbroker.agent.service.FlightRequestService;

@Stateless
public class FlightRequestServiceImpl implements FlightRequestService {

	
	@Inject
	FlightRequestRepository repo;
	
	@Override
	public FlightRequest getFlightRequest(long id) {
		return repo.findById(id);
	}

	@Override
	public FlightRequest createFlightRequest(FlightRequest flightRequest) {
		return repo.findById(repo.persist(flightRequest));
	}

	@Override
	public void updateFlightRequest(FlightRequest flightRequest) {
			repo.update(flightRequest);
	}

	@Override
	public List<FlightRequest> getAllFlightRequests() {
		return repo.getAllFlightRequests();
	}

	@Override
	public List<FlightRequest> getFlightRequestsByStatus(Status status) {
		return repo.getFlightRequestsByStatus(status);
	}

}
