package se.plushogskolan.jetbroker.order.mvc.bean;

import javax.validation.Valid;

import se.plushogskolan.jetbroker.order.domain.Plane;

public class EditPlaneBean {
	@Valid
	private Plane plane;
	

	public EditPlaneBean() {
	}
	
	public Plane getPlane() {
		return plane;
	}
	public void setPlane(Plane plane) {
		this.plane = plane;
	}
	
	
}
