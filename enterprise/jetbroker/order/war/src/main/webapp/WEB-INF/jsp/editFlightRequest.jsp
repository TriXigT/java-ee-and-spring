<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
        
<script type="text/javascript">
$(document).ready(function() {
	$('.formElement').attr('required', ''); 
});
</script>

<html>
<head>
<link href="<%=request.getContextPath()%>/style/common.css" type="text/css" rel="stylesheet" />
<link href="<%=request.getContextPath()%>/style/theme_stylish.css" type="text/css" rel="stylesheet" />
<c:set var="dateFormat"><spring:message code="index.flightRequest.dateFormat"/></c:set>
</head>
<body>
	<div class="container">
		<jsp:include page="header.jsp" />
		<div class="content">
			<div class="content_container" id="generic_container">
				<img src="<%=request.getContextPath()%>/images/flightrequest.png"/>
				<h2><spring:message code="flightRequest.edit.title"/></h2>
				<hr>
				<div id="generic_form">
					<label for="id_field"><spring:message code="global.id"/></label>
					<span id="id_field" class="float_left">${flightRequestBean.flightRequest.id}</span>
					<span class=clear></span>
					
					<label for="departure"><spring:message code="flightRequest.departureAirport"/></label>
					<span id="departure" class="float_left">${flightRequestBean.departureAirport.niceName}</span>
					<span class=clear></span>
					
					<label for="arrival"><spring:message code="flightRequest.arrivalAirport"/></label>
					<span id="arrival" class="float_left">${flightRequestBean.arrivalAirport.niceName}</span>
					<span class=clear></span>
					
					<label for="date"><spring:message code="global.date"/></label>
					<span id="date" class="float_left"><joda:format value="${flightRequestBean.flightRequest.date}" pattern="${dateFormat}"/></span>
					<span class=clear></span>
					
					<label for="passengers"><spring:message code="global.noOfPassengers"/></label>
					<span id="passengers" class="float_left">${flightRequestBean.flightRequest.passengers}</span>
					<span class=clear></span>
					
					<label for="distance"><spring:message code="global.distance"/></label>
					<span id="distance" class="float_left">${flightRequestBean.travelDistance}</span>
					<span class=clear></span>
					<br>
					<label for="fuelPrice"><spring:message code="global.fuelPrice"/></label>
					<span id="fuelPrice" class="float_left">${fuelPrice}</span>
					<span class=clear></span>
				</div>
				<hr>
				<h2><spring:message code="flightRequest.offer.title"/></h2>
				<span class="clear"></span>
				<form:form id="generic_form" commandName="offerBean">
				<form:hidden path="offer.flightRequestId" value="${offerBean.offer.flightRequestId}"/>
					<c:choose>
						<c:when test="${!empty planeBeans}">
						<table class="datatable float_left clear">
							<tr>
								<th></th>
								<th><spring:message code="global.plane.name"/></th>
								<th><spring:message code="global.plane.maxPassengers"/></th>
								<th><spring:message code="global.plane.maxSpeed"/></th>
								<th><spring:message code="global.plane.fuelConsumption"/></th>
								<th><spring:message code="flightRequest.plane.fuelCost"/></th>
								<th><spring:message code="flightRequest.plane.travelTime"/></th>
							</tr>
							<c:forEach items="${planeBeans}" var="planeBean">
								<tr>
									<td>
										<form:radiobutton path="offer.planeId" value="${planeBean.plane.id}"/>
									</td>
									<td>${planeBean.planeType.niceName}</td>
									<td>${planeBean.planeType.maxNoOfPassengers}</td>
									<td>${planeBean.planeType.maxSpeedKmH}</td>
									<td>${planeBean.planeType.fuelConsumptionPerKm}</td>
									<td><spring:message code="global.price.pattern" arguments="${planeBean.fuelCost}" htmlEscape="false"/></td> 
									<td>${planeBean.travelTime}</td>
								</tr>
							</c:forEach>
						</table>
						
						<label for="offer"><spring:message code="flightRequest.offeredPrice"/></label>
						<form:input id="offer" class="formElement float_left" path="offer.price"/>
						<form:errors path="offer.price" class="errors"/>
						<br>
						<input type="submit" value="<spring:message code="global.submit"/>"/>
						<a id="generic_button" class="button_style" href="<%=request.getContextPath()%>/rejectFlightRequest/${flightRequestBean.flightRequest.id}.html"><spring:message code="flightRequest.reject"/></a>
						<a id="generic_button" class="button_style" href="<%=request.getContextPath()%>/index.html"><spring:message code="global.cancel"/></a>
						</c:when>
						<c:otherwise>
							<span><spring:message code="flightRequest.plane.noPlanes"></spring:message></span>
						</c:otherwise>
					</c:choose>
				</form:form>
			</div>
		</div>
	</div>
</body>

</html>