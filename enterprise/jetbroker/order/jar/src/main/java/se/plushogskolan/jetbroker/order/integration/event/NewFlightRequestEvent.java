package se.plushogskolan.jetbroker.order.integration.event;

public class NewFlightRequestEvent{

	private long flightRequestId;
	
	public NewFlightRequestEvent() {
	}
	
	public NewFlightRequestEvent(long flightRequestId) {
		this.flightRequestId = flightRequestId;
	}

	public long getFlightRequestId() {
		return flightRequestId;
	}

	public void setFlightRequestId(long flightRequestId) {
		this.flightRequestId = flightRequestId;
	}
	
}
