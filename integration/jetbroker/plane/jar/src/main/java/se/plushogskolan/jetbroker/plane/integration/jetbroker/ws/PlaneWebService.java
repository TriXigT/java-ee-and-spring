package se.plushogskolan.jetbroker.plane.integration.jetbroker.ws;

import java.util.List;

import javax.ejb.Local;
import javax.jws.WebMethod;
import javax.jws.WebService;

@Local
@WebService
public interface PlaneWebService {

	@WebMethod
	public List<WSAirport> getAirports();
	
	@WebMethod
	public List<WSPlaneType> getPlaneTypes();

}
