package se.plushogskolan.jetbroker.plane.rest.planetype.model;

import se.plushogskolan.jetbroker.plane.domain.PlaneType;

public class GetPlaneTypeResponse {
	private long id;
	private String code;
	private String name;
	private int maxNoOfPassengers;
	private int maxRangeKm;
	private int maxSpeedKmH;
	private double fuelConsumptionPerKm;
	
	public GetPlaneTypeResponse(PlaneType planeType) {
		setId(planeType.getId());
		setCode(planeType.getCode());
		setName(planeType.getName());
		setMaxNoOfPassengers(planeType.getMaxNoOfPassengers());
		setMaxRangeKm(planeType.getMaxRangeKm());
		setMaxSpeedKmH(planeType.getMaxSpeedKmH());
		setFuelConsumptionPerKm(planeType.getFuelConsumptionPerKm());
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMaxNoOfPassengers() {
		return maxNoOfPassengers;
	}

	public void setMaxNoOfPassengers(int maxNoOfPassengers) {
		this.maxNoOfPassengers = maxNoOfPassengers;
	}

	public int getMaxRangeKm() {
		return maxRangeKm;
	}

	public void setMaxRangeKm(int maxRangeKm) {
		this.maxRangeKm = maxRangeKm;
	}

	public int getMaxSpeedKmH() {
		return maxSpeedKmH;
	}

	public void setMaxSpeedKmH(int maxSpeedKmH) {
		this.maxSpeedKmH = maxSpeedKmH;
	}

	public double getFuelConsumptionPerKm() {
		return fuelConsumptionPerKm;
	}

	public void setFuelConsumptionPerKm(double fuelConsumptionPerKm) {
		this.fuelConsumptionPerKm = fuelConsumptionPerKm;
	}
}
