package se.plushogskolan.jetbroker.agent.events;

/**
 * An event that indicates that a  PlaneType has changed. This is a very generic event.
 * 
 */
public class PlaneTypesChangedEvent {

}
