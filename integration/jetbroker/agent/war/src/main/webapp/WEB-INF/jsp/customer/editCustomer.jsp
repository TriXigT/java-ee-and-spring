<%@ page contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>



<title>Edit customer</title>

<link href="<%=request.getContextPath()%>/style/common.css"
	type="text/css" rel="stylesheet" />

</head>

<body>

		<jsp:include page="../header.jsp" />

	<form:form commandName="editCustomerBean">

		<h2 class="underline"> <img src="<%=request.getContextPath()%>/images/customer.png">
		<c:choose>
			<c:when test="${editCustomerBean.customer.id > 0}">
				Edit customer ${customer.name}
			</c:when>
			<c:otherwise>
				Add new customer
			</c:otherwise>
		</c:choose>
		</h2>

		<form:hidden path="customer.id" />
		<table class="formTable">
			<c:if test="${editCustomerBean.customer.id > 0}">
				<tr>
					<th>ID</th>
					<td>${editCustomerBean.customer.id}</td>
					<td></td>
				</tr>
			</c:if>
			<tr>
				<th>First name</th>
				<td><form:input path="customer.firstName" /></td>
				<td><form:errors path="customer.firstName" cssClass="errors" />
				</td>
			</tr>
			<tr>
				<th>Last name</th>
				<td><form:input path="customer.lastName" />
				<td><form:errors path="customer.lastName" cssClass="errors" />
				</td>
				</td>
			</tr>
			<tr>
				<th>Email</th>
				<td><form:input path="customer.email" /></td>
				<td><form:errors path="customer.email" cssClass="errors" /></td>
			</tr>
			<tr>
				<th>Company</th>
				<td><form:input path="customer.company" /></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="Submit" />
					<a href="<%=request.getContextPath()%>/index.html">Cancel</a>
				</td>
				<td></td>
			</tr>

		</table>
	</form:form>

</body>
</html>