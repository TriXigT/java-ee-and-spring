package se.plushogskolan.jetbroker.agent.repository.mock;

import java.util.ArrayList;
import java.util.List;

import se.plushogskolan.jee.utils.repository.mock.MockAbstractRepository;
import se.plushogskolan.jetbroker.agent.domain.PlaneType;
import se.plushogskolan.jetbroker.agent.repository.PlaneTypeRepository;

public class MockPlaneTypeRepository extends MockAbstractRepository<PlaneType> implements PlaneTypeRepository{

	private List<PlaneType> entities;
	
	public MockPlaneTypeRepository() {
		entities = new ArrayList<PlaneType>();
		
		PlaneType planeTypeTemp = new PlaneType("SPTA11","Spitfire A11");
		idGenerator.assignNextId(planeTypeTemp);
		
		PlaneType planeType = new PlaneType("404","Boeing 404");
		idGenerator.assignNextId(planeType);
		
		entities.add(planeTypeTemp);
		entities.add(planeType);
	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {
		return getMockList();
	}

	@Override
	protected List<PlaneType> getMockList() {
		return entities;
	}

	@Override
	public PlaneType getPlaneTypeByCode(String code) {
		for(PlaneType planeType : entities) {
			if(planeType.getCode().equals(code)) {
				return planeType;
			}
		}
		return null;
	}

}
