package se.plushogskolan.jetbroker.plane.rest.planetype;

import java.util.logging.Logger;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import se.plushogskolan.jetbroker.plane.rest.OkOrErrorResponse;
import se.plushogskolan.jetbroker.plane.rest.planetype.model.CreatePlaneTypeRequest;
import se.plushogskolan.jetbroker.plane.rest.planetype.model.CreatePlaneTypeResponse;
import se.plushogskolan.jetbroker.plane.rest.planetype.model.GetPlaneTypeResponse;
import se.plushogskolan.jetbroker.plane.service.PlaneService;

@Controller
public class PlaneTypeController {
	Logger log = Logger.getLogger(PlaneTypeController.class.getName());
	
	@Inject
	PlaneService planeService;
	
	@RequestMapping(value = "/createPlaneType", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public CreatePlaneTypeResponse createPlaneType(@RequestBody CreatePlaneTypeRequest request) throws Exception {
		log.fine("createPlaneType: " + request);
		return new CreatePlaneTypeResponse(planeService.createPlaneType(request.buildPlaneType()).getId());
	}

	@RequestMapping(value = "/getPlaneType/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public GetPlaneTypeResponse getPlaneType(@PathVariable long id) {
		log.fine(String.format("getPlaneType id:%d", id));
		return new GetPlaneTypeResponse(planeService.getPlaneType(id));
	}

	@RequestMapping(value = "/deletePlaneType/{id}", method = RequestMethod.DELETE, produces = "application/json")
	@ResponseBody
	public OkOrErrorResponse deletePlaneType(@PathVariable long id) {
		log.fine(String.format("deletePlaneType id:%d", id));
		try {
			planeService.deletePlaneType(id);
		} catch(Exception e) {
			return OkOrErrorResponse.getErrorResponse(e.getMessage());
		}
		
		return OkOrErrorResponse.getOkResponse();
	}
}
