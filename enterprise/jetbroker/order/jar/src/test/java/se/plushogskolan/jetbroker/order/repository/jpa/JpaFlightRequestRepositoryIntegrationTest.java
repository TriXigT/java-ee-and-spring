package se.plushogskolan.jetbroker.order.repository.jpa;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.inject.Inject;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.Status;
import se.plushogskolan.jetbroker.order.repository.AbstractRepositoryTest;

@RunWith(Arquillian.class)
@Transactional(TransactionMode.ROLLBACK)
public class JpaFlightRequestRepositoryIntegrationTest extends AbstractRepositoryTest<FlightRequest, JpaFlightRequestRepository>{

	@Inject
	JpaFlightRequestRepository repo;

	@Test
	public void testDeleteOldFlightRequests() {
		long id1 = repo.persist(getEntity1());
		
		FlightRequest flightRequest2 = getEntity2();
		repo.persist(flightRequest2);
		
		assertEquals("FlightRequests in DB", 2, repo.getAllFlightRequests().size());
		
		repo.remove(flightRequest2);
		
		assertEquals("Number of FlightRequests after delete", 1, repo.getAllFlightRequests().size());
		assertEquals("Correct FlightRequest is in DB", id1, repo.getAllFlightRequests().iterator().next().getId());
	}
	
	@Test
	public void testGetAllFlightRequests() {
		repo.persist(getEntity1());
		repo.persist(getEntity2());
		
		assertTrue("No. of FlightRequestss", repo.getAllFlightRequests().size() == 2);
	}
	
	@Test
	public void testGetByStatus() {
		repo.persist(getEntity1());
		repo.persist(getEntity2());
		
		assertEquals("get FlightRequests by status", 2, repo.getFlightRequestsByStatus(Status.NEW).size());
	}
	
	@Test
	public void testPersist() {
		FlightRequest req = getEntity2();
		req.setStatus(Status.REJECTED);
		DateTime date = DateTime.now();
		req.setDate(date);
		repo.persist(req);
		
		assertEquals("status", Status.REJECTED, req.getStatus());
		assertEquals("date and time", date, req.getDate());
	}
	
	@Test(expected = Exception.class)
	public void testValidationInvalidState() {
		FlightRequest req = getEntity1();
		req.setPassengers(9999);
		
		repo.persist(req);
	}

	@Override
	protected JpaFlightRequestRepository getRepository() {
		return repo;
	}

	@Override
	protected FlightRequest getEntity1() {
		return TestFixture.getValidFlightRequest(2, Status.NEW, "GBG", "STM", DateTime.now(), "AIR", 32, 32);
	}

	@Override
	protected FlightRequest getEntity2() {
		return TestFixture.getValidFlightRequest(4, Status.NEW, "STM", "GBG", DateTime.now(), "BAG", 66, 11);
	}

}