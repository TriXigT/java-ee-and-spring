package se.plushogskolan.jetbroker.agent.domain;

public enum Status {
    CREATED("Request created"), REQUEST_CONFIRMED("Request confirmed"), OFFER_RECEIVED("Offer received"), REJECTED("Request rejected");
    
    String niceName;
    
    Status(String niceName) {
    	this.niceName = niceName;
    }
    
    public String getNiceName() {
		return niceName;
	}
}
