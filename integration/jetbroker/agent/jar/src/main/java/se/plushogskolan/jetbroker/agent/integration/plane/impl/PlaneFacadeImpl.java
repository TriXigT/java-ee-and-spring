package se.plushogskolan.jetbroker.agent.integration.plane.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.agent.domain.AirPort;
import se.plushogskolan.jetbroker.agent.domain.PlaneType;
import se.plushogskolan.jetbroker.agent.integration.plane.PlaneFacade;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.PlaneWebService;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.PlaneWebServiceImplService;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.WsAirport;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.WsPlaneType;

@Stateless
@Prod
public class PlaneFacadeImpl implements PlaneFacade{

	@Inject
	private Logger log;
	
	@Override
	public List<AirPort> getAllAirports() {
		log.fine("get all airports from plane with SOAP");
		
		List<AirPort> airPorts = new ArrayList<AirPort>();
		
		PlaneWebServiceImplService webService = new PlaneWebServiceImplService();
		PlaneWebService port = webService.getPlaneWebServicePort();

		for(WsAirport wsAirport : port.getAirports()) {
			AirPort airPort = new AirPort(wsAirport.getCode(), wsAirport.getName());
			airPorts.add(airPort);
		}
		
		return airPorts;
	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {
		log.fine("get all plane types from plane with SOAP");
		
		List<PlaneType> planeTypes = new ArrayList<PlaneType>();
		
		PlaneWebServiceImplService webService = new PlaneWebServiceImplService();
		PlaneWebService port = webService.getPlaneWebServicePort();
		
		for(WsPlaneType wsPlaneType : port.getPlaneTypes()) {
			planeTypes.add(new PlaneType(
					wsPlaneType.getCode(), 
					wsPlaneType.getName(), 
					wsPlaneType.getMaxNoOfPassengers(), 
					wsPlaneType.getMaxRangeKm(), 
					wsPlaneType.getMaxSpeedKmH(), 
					wsPlaneType.getFuelConsumptionPerKm()));
		}
		
		return planeTypes;
	}

}
