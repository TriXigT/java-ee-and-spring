package se.plushogskolan.jetbroker.agent.rest.exercises.xml.json;

import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;

public class GetFlightRequestAsJsonResponse {

	private long id;
	private String departureAirportCode;
	private String arrivalAirportCode;
	private String status;
	private int noOfPassengers;

	public GetFlightRequestAsJsonResponse(FlightRequest flightRequest) {
		setId(flightRequest.getId());
		setDepartureAirportCode(flightRequest.getDepartureAirportCode());
		setArrivalAirportCode(flightRequest.getArrivalAirportCode());
		setStatus(flightRequest.getRequestStatus().toString());
		setNoOfPassengers(flightRequest.getNoOfPassengers());
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDepartureAirportCode() {
		return departureAirportCode;
	}

	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}

	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}

	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getNoOfPassengers() {
		return noOfPassengers;
	}

	public void setNoOfPassengers(int noOfPassengers) {
		this.noOfPassengers = noOfPassengers;
	}

}
