<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<script src="<%=request.getContextPath()%>/js/jquery-1.9.1.js" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	$( '.formElement float_left' ).attr('required', ''); 
	$( '#departureDate' ).datepicker({ dateFormat: 'dd-mm-yy' }).val();
});
</script>

<html>
<head>
<link href="<%=request.getContextPath()%>/style/common.css" type="text/css" rel="stylesheet" />
<link href="<%=request.getContextPath()%>/style/theme_stylish.css" type="text/css" rel="stylesheet" />
<link href="<%=request.getContextPath()%>/js/jquery-ui-1.10.3.custom.css" type="text/css" rel="stylesheet" />
</head>
<body>
	<div class="container">
		<jsp:include page="header.jsp" />
		<div class="content">
			<div class="content_container" id="generic_container">
				<img src="<%=request.getContextPath()%>/images/flightrequest.png"/>
					<h2>
						<c:choose>
							<c:when test="${editFlightRequestBean.id > 0}">
								<spring:message code="flightRequest.edit.title"/>
							</c:when>
							<c:otherwise>
								<spring:message code="flightRequest.add.title"/>
							</c:otherwise>
						</c:choose>
					</h2>
				<hr>
				<form:form id="generic_form" class="generic_form_flight" commandName="editFlightRequestBean">
					<c:choose>
						<c:when test="${editFlightRequestBean.id > 0}">
							<label for="id_field"><spring:message code="global.id"/></label>
							<span id="id_field float_left">${flightRequestBean.id}</span></c:when>
						<c:otherwise>
							<form:hidden path="id" value="${editFlightRequestBean.id}"/>
						</c:otherwise>
					</c:choose>
					<span class="clear small_space float_left"></span>
					
					<label for="customer"><spring:message code="flightRequest.customer"/></label>
					<form:select id="customer" class="formElement float_left float_left" path="customerEmail">
							<form:option value="" label=""/>
							<form:options items="${customers}" itemValue="email" itemLabel="niceName"/>	
					</form:select>
					<form:errors path="customerEmail" class="errors" />
					<span class="clear small_space float_left"></span>
					
					<label for="departure"><spring:message code="flightRequest.departureAirport"/></label>
					<form:select id="departure" class="formElement float_left" path="departureAirportCode">
							<form:option value="" label=""/>				
							<form:options items="${airports}" itemValue="code" itemLabel="niceName"/>				
					</form:select>
					<form:errors path="departureAirportCode" class="errors" />
					<form:errors path="" class="errors"/>
					<span class="clear small_space float_left"></span>
					
					<label for="arrival"><spring:message code="flightRequest.arrivalAirport"/></label>
					<form:select id="arrival" class="formElement float_left" path="arrivalAirportCode">
							<form:option value="" label=""/>
							<form:options items="${airports}" itemValue="code" itemLabel="niceName"/>							
					</form:select>
					<form:errors path="arrivalAirportCode" class="errors"/>
					<span class="clear small_space float_left"></span>
					
					<label for="numberOfPassengers"><spring:message code="flightRequest.noOfPassengers"/></label>	
					<form:input id="numberOfPassengers" class="formElement float_left" path="numberOfPassengers"/> 
					<form:errors path="numberOfPassengers" class="errors" />
					<span class="clear small_space float_left"></span>
					
					<label for="departureDate"><spring:message code="flightRequest.departureDate"/></label>	
					<form:input id="departureDate" class="formElement float_left" path="date"/> 
					<form:errors path="date" class="errors" />
					<span class="clear small_space float_left"></span>
					
					<label><spring:message code="flightRequest.departureTime"/></label>	
					<div id="hour_minute">
						<form:select id="departureHour" class="formElement float_left" path="hour">
							<form:option value="-1" label=""/>
							<c:forEach begin="0" end="23" step="1" var="i">
								<form:option value="${i}" label="${i}"/>
							</c:forEach>								
						</form:select>
						<label for="departureHour"><spring:message code="flightRequest.hour"/></label>	
						<form:errors path="hour" class="errors" />
						<span class="clear small_space float_left"></span>
						
						<form:select id="departureMinute" class="formElement float_left" path="minute">
							<form:option value="-1" label=""/>
							<c:forEach begin="0" end="59" step="1" var="i">
								<form:option value="${i}" label="${i}"/>
							</c:forEach>								
						</form:select>
						<label for="departureMinute"><spring:message code="flightRequest.minute"/></label>	
						<form:errors path="minute" class="errors" />
						
						<span class="clear space float_left"></span>
							
						<input type="submit" value="<spring:message code="global.submit"/>"/>
						<a id="generic_button" class="button_style"href="<%=request.getContextPath()%>/index.html"><spring:message code="global.cancel"/></a>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</body>

</html>