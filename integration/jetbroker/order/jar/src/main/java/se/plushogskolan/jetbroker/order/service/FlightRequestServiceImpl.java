package se.plushogskolan.jetbroker.order.service;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.order.domain.FlightRequestStatus;
import se.plushogskolan.jetbroker.order.domain.Offer;
import se.plushogskolan.jetbroker.order.integration.agent.AgentIntegrationFacade;
import se.plushogskolan.jetbroker.order.repository.FlightRequestRepository;

@Stateless
public class FlightRequestServiceImpl implements FlightRequestService {

	@Inject
	private Logger log;

	@Inject
	private FlightRequestRepository flightRequestRepository;

	@Inject
	private PlaneService planeService;

	@Inject
	@Prod
	private AgentIntegrationFacade agentIntegrationFacade;
	
	@Inject
	private FlightUtilityService flightUtils;

	@Override
	public FlightRequestConfirmation handleIncomingFlightRequest(FlightRequest request) {
		log.fine(String.format("handle incoming request: %s", request.toString()));
		
		request.setStatus(FlightRequestStatus.NEW);
		request.setDistanceKm(flightUtils.getDistanceForFlight(request.getDepartureAirportCode(), request.getArrivalAirportCode()));
		
		FlightRequest persisted = flightRequestRepository.findById(flightRequestRepository.persist(request));
		
		FlightRequestConfirmation confirmation = new FlightRequestConfirmation(persisted.getAgentRequestId(), persisted.getId());
		
		agentIntegrationFacade.sendFlightRequestConfirmation(confirmation);
				
		return confirmation;

	}

	@Override
	public void handleUpdatedOffer(long flightRequestId, Offer offer) {

		// Implement
	}

	@Override
	public void rejectFlightRequest(long id) {

		// Implement

	}

	@Override
	public FlightRequest getFlightRequest(long id) {
		log.fine(String.format("get flightrequest by id [%d]", id));
		
		return getFlightRequestRepository().findById(id);
	}

	@Override
	public List<FlightRequest> getAllFlightRequests() {
		log.fine("get all flightrequests");
		
		return getFlightRequestRepository().getAllFlightRequests();
	}

	@Override
	public void deleteFlightRequest(long id) {
		log.fine(String.format("delete flightrequest with id [%d]", id));
		
		FlightRequest flightRequest = getFlightRequest(id);
		getFlightRequestRepository().remove(flightRequest);

	}

	public FlightRequestRepository getFlightRequestRepository() {
		return flightRequestRepository;
	}

	public void setFlightRequestRepository(FlightRequestRepository flightRequestRepository) {
		this.flightRequestRepository = flightRequestRepository;
	}

	public PlaneService getPlaneService() {
		return planeService;
	}

	public void setPlaneService(PlaneService planeService) {
		this.planeService = planeService;
	}

	public AgentIntegrationFacade getAgentIntegrationFacade() {
		return agentIntegrationFacade;
	}

	public void setAgentIntegrationFacade(AgentIntegrationFacade agentIntegrationFacade) {
		this.agentIntegrationFacade = agentIntegrationFacade;
	}

	public FlightUtilityService getFlightUtils() {
		return flightUtils;
	}

	public void setFlightUtils(FlightUtilityService flightUtils) {
		this.flightUtils = flightUtils;
	}

	protected void setLog(Logger log) {
		this.log = log;
	}
	
}
