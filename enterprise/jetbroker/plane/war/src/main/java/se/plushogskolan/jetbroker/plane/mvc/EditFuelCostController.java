package se.plushogskolan.jetbroker.plane.mvc;

import java.util.logging.Logger;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.plane.service.AirportService;

@Controller
@RequestMapping("/editFuelCost.html")
public class EditFuelCostController {

	private static Logger log = Logger.getLogger(EditFuelCostController.class.getName());

	@Inject
	private AirportService airportService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView index() {

		log.fine("Edit fuel cost");

		double fuelCost = getAirportService().getFuelCost();

		EditFuelCostBean bean = new EditFuelCostBean();
		bean.setFuelCost(fuelCost);
		ModelAndView mav = new ModelAndView("editFuelCost");
		mav.addObject("editFuelCostBean", bean);
		return mav;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView handleSubmit(@Valid EditFuelCostBean bean, BindingResult errors) throws Exception {

		if (errors.hasErrors()) {
			ModelAndView mav = new ModelAndView("editFuelCost");
			mav.addObject("editFuelCostBean", bean);
			return mav;
		}

		getAirportService().updateFuelCost(bean.getFuelCost());

		return new ModelAndView("redirect:/index.html");
	}

	public AirportService getAirportService() {
		return airportService;
	}

	public void setAirportService(AirportService airportService) {
		this.airportService = airportService;
	}

}
