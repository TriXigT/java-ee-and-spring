package se.plushogskolan.jetbroker.order.mvc;

import javax.validation.constraints.Min;

import se.plushogskolan.jetbroker.order.domain.FlightRequest;

public class EditFlightRequestBean {

	@Min(value = 1, message = "{validation.price.missing}")
	private double price;
	@Min(value = 1, message = "{validation.plane.missing}")
	private long planeId;

	public void copyFlightRequestValuesToBean(FlightRequest flightRequest) {

		if (flightRequest.getOffer() != null) {
			setPrice(flightRequest.getOffer().getPrice());
			if (flightRequest.getOffer().getPlane() != null) {
				setPlaneId(flightRequest.getOffer().getPlane().getId());
			}
		}
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public long getPlaneId() {
		return planeId;
	}

	public void setPlaneId(long planeId) {
		this.planeId = planeId;
	}

}
