package se.plushogskolan.jetbroker;

import static org.junit.Assert.fail;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.joda.time.DateTime;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.Offer;
import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.domain.Status;

public class TestFixture {
	
	public static Plane getValidPlane(String code, String description) {
		Plane plane = new Plane();
		plane.setPlaneTypeCode(code);
		plane.setDescription(description);

		return plane;
	}

	public static Plane getValidPlane() {
		return getValidPlane("AIR", "Airbus");
	}
	
	public static PlaneType getValidPlaneType(String code, String name) {
		return getValidPlaneType(code,name,3333,1.1);
	}
	
	public static PlaneType getValidPlaneType(String code, String name, int maxSpeedKmH, double fuelConsumptionPerKm) {
		return new PlaneType(code,name,33,33333,maxSpeedKmH,fuelConsumptionPerKm);
	}
	
	public static PlaneType getValidPlaneType() {
		return getValidPlaneType("AIR", "Airbus");
	}
	
	public static Airport getValidAirport(String code, String name, double latitude, double longitude) {
		return new Airport(code, name, latitude, longitude);
	}
	
	public static Airport getValidAirport() {
		return getValidAirport("NAM", "Hello", 35, 35);
	}
	
	public static Offer getValidOffer() {
		return new Offer(4,4,44444);
	}
	
	public static Offer getValidOffer(double price) {
		return new Offer(5,5,price);
	}
	
	public static FlightRequest getValidFlightRequest() {
		return getValidFlightRequest(1,Status.NEW,"STM","GBG",DateTime.now(), "AIR", 5, 555555);
	}
	
	public static FlightRequest getValidFlightRequest(long agentId, Status status, String departureAirportCode,
			String arrivalAirportCode, DateTime date, String planeCode,
			int passengers, double offeredPrice) {
		return new FlightRequest(agentId,status, departureAirportCode, arrivalAirportCode, date, planeCode, passengers, offeredPrice);
	}
	
	/**
	 * Helper method to check if a certain property has reported errors
	 */
	public static void assertPropertyIsInvalid(String property,
			Set<? extends ConstraintViolation<?>> violations) {

		boolean errorFound = false;
		for (ConstraintViolation<?> constraintViolation : violations) {
			if (constraintViolation.getPropertyPath().toString()
					.equals(property)) {
				errorFound = true;
				break;
			}
		}

		if (!errorFound) {
			fail("Expected validation error for '" + property
					+ "', but no such error exists");
		}
	}

	public static Validator getValidator() {
		return Validation.buildDefaultValidatorFactory().getValidator();
	}

	public static Archive<?> createIntegrationTestArchive() {

		MavenDependencyResolver mvnResolver = DependencyResolvers.use(MavenDependencyResolver.class)
				.loadMetadataFromPom("pom.xml");

		WebArchive war = ShrinkWrap.create(WebArchive.class, "order_test.war").addPackages(true, "se.plushogskolan")
				.addAsWebInfResource("beans.xml").addAsResource("META-INF/persistence.xml");

		war.addAsLibraries(mvnResolver.artifact("org.easymock:easymock:3.2").resolveAsFiles());
		war.addAsLibraries(mvnResolver.artifact("joda-time:joda-time:2.2").resolveAsFiles());
		war.addAsLibraries(mvnResolver.artifact("org.jadira.usertype:usertype.core:3.1.0.CR8").resolveAsFiles());

		return war;
	}
}
