package se.plushogskolan.jetbroker.plane.repository.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import se.plushogskolan.jee.utils.repository.jpa.JpaRepository;
import se.plushogskolan.jetbroker.plane.domain.PlaneType;
import se.plushogskolan.jetbroker.plane.repository.PlaneTypeRepository;

public class JpaPlaneTypeRepository extends JpaRepository<PlaneType> implements PlaneTypeRepository {

	@PersistenceContext
	protected EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public List<PlaneType> getAllPlaneTypes() {
		return em.createQuery("select pt from PlaneType pt").getResultList();
	}

}
