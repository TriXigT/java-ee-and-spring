package se.plushogskolan.jetbroker.agent.integration.order.mock.mdb;

import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Mock;
import se.plushogskolan.jetbroker.agent.annotations.FlightRequestChanged;
import se.plushogskolan.jetbroker.agent.annotations.FlightRequestChanged.ChangeType;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.agent.events.FlightRequestChangedEvent;
import se.plushogskolan.jetbroker.agent.services.FlightRequestService;

@ApplicationScoped
public class MockFlightRequestConfirmationMDB {

	private static Logger log = Logger.getLogger(MockFlightRequestConfirmationMDB.class.getName());

	private static long confirmationId = 1;

	@Inject
	private FlightRequestService flightRequestService;

	public void handleEvent(@Observes @FlightRequestChanged(ChangeType.CONFIRMED) @Mock FlightRequestChangedEvent event) {

		long confId = confirmationId++;
		log.info("FlightRequest is confirmed in mock. ConfirmationID=" + confId);
		FlightRequestConfirmation confirmation = new FlightRequestConfirmation(event.getFlightRequestId(), confId);

		getFlightRequestService().handleFlightRequestConfirmation(confirmation);
	}

	public FlightRequestService getFlightRequestService() {
		return flightRequestService;
	}

	public void setFlightRequestService(FlightRequestService flightRequestService) {
		this.flightRequestService = flightRequestService;
	}

}
