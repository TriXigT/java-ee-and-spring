<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/" >
	<html>
	<body>
		<h1>
			<xsl:text>Flight request - </xsl:text>
			<xsl:value-of select="/flightrequest/@id"></xsl:value-of>
		</h1>
		<p>
			<xsl:text>Departure airport code: </xsl:text>
			<xsl:value-of select="/flightrequest/departureAirportCode"/></p>
		<p>
			<xsl:text>arrivalAirportCode: </xsl:text>
			<xsl:value-of select="/flightrequest/arrivalAirportCode"/>
		</p>
		<p>
			<xsl:text>No of passengers: </xsl:text>
			<xsl:value-of select="/flightrequest/noOfPassengers"/>
		</p>
		<p>
			<xsl:text>Status: </xsl:text>
			<xsl:value-of select="/flightrequest/status"/>
		</p>
	</body>
	</html>
	</xsl:template>
</xsl:stylesheet> 