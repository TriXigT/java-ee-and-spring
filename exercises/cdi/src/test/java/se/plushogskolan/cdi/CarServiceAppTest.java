package se.plushogskolan.cdi;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.cdi.interceptors.InvocationCounterInterceptor;
import se.plushogskolan.cdi.model.Car;
import se.plushogskolan.cdi.services.CarService;

public class CarServiceAppTest {

	private Weld weld;

	private CarServiceApp app;

	@Before
	public void setup() {
		// Start Weld - this initiates the CDI container
		weld = new Weld();
		WeldContainer container = weld.initialize();
		app = container.instance().select(CarServiceApp.class).get();
	}

	@After
	public void shutDown() {
		weld.shutdown();
	}

	@Test
	public void testTodaysDate() {

		String expectedDate = new DateTime().toString("dd-MM-yyyy");
		assertEquals("Todays date", expectedDate, app.todaysDate);
	}

	@Test
	public void testYesterdaysDate() {
		DateTime date = new DateTime();
		date = date.minusDays(1);
		String expectedDate = date.toString("dd-MM-yyyy");
		assertEquals("Yesterays date", expectedDate, app.yesterdaysDate);
	}

	@Test
	public void testNormalCarService() {
		CarService carService = app.normalCarService;
		assertNotNull("NormalCarService is injected", carService);

		Car car = carService.getCar();
		assertEquals("Car is corrrect", "Mocked normal car", car.getName());
		assertEquals("Car owner is correct", "Hans Olsson", car.getOwner()
				.getName());
	}

	@Test
	public void testSportCarService() {
		CarService carService = app.sportCarService;
		assertNotNull("SportCarService is injected", carService);

		Car car = carService.getCar();
		assertNotNull("Car is retrieved", car);
		assertEquals("Car owner is correct", "Hans von Sparre", car.getOwner()
				.getName());
	}

	@Test
	public void testUsers() {
		assertNotSame("Normal user is not same", app.normalUser1.getId(),
				app.normalUser2.getId());
		assertSame("Rich user is same", app.richUser1.getId(),
				app.richUser2.getId());

	}

	@Test
	public void testSaveCar() {
		Car car = new Car("New car");

		// NormalCarService uses mock
		app.normalCarService.saveCar(car);
		assertEquals("No updated car for mock", 0, app.carUpdatedCount);

		// SportCarService uses prod DB that throws events
		app.sportCarService.saveCar(car);
		assertEquals("Updated car for DB", 1, app.carUpdatedCount);
	}

	@Test
	public void testDecoratedSportsCar() {

		Car car = app.sportCarService.getCar();
		assertEquals("Decorated sports car name",
				"DB sports car with some decorated wrom wrom", car.getName());
	}

	@Test
	public void testInterceptor() {

		InvocationCounterInterceptor.reset();

		// Trigger som methods. Every method call should be counted by the
		// interceptor.
		app.sportCarService.getCar();
		app.sportCarService.getCar();
		app.normalCarService.getCar();

		assertEquals("getNormalCar", 3,
				InvocationCounterInterceptor
						.getInvocationCountForMethod("getCar"));
		assertEquals("getSportsCar", 2,
				InvocationCounterInterceptor
						.getInvocationCountForMethod("getSportsCar"));
		assertEquals("getNormalCar", 1,
				InvocationCounterInterceptor
						.getInvocationCountForMethod("getNormalCar"));
	}

}
