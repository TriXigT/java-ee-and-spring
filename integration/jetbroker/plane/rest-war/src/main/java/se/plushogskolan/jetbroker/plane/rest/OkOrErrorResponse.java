package se.plushogskolan.jetbroker.plane.rest;

public class OkOrErrorResponse {

	public enum OkOrErrorStatus {

		OK, ERROR;
	}

	private OkOrErrorStatus status = OkOrErrorStatus.OK;
	private String errorMessage = "";

	public static OkOrErrorResponse getOkResponse() {
		return new OkOrErrorResponse(OkOrErrorStatus.OK);
	}

	public static OkOrErrorResponse getErrorResponse(String error) {
		OkOrErrorResponse response = new OkOrErrorResponse(OkOrErrorStatus.ERROR);
		response.setErrorMessage(error);
		return response;
	}

	private OkOrErrorResponse(OkOrErrorStatus status) {
		setStatus(status);
	}

	public OkOrErrorStatus getStatus() {
		return status;
	}

	public void setStatus(OkOrErrorStatus status) {
		this.status = status;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
