package se.plushogskolan.jetbroker.plane.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.plane.domain.PlaneType;
import se.plushogskolan.jetbroker.plane.repository.PlaneTypeRepository;

@Stateless
public class PlaneTypeServiceImpl implements PlaneTypeService {

	@Inject
	private PlaneTypeRepository planeRepository;

	@Override
	public PlaneType getPlaneType(long id) {
		return getPlaneRepository().getPlaneType(id);
	}

	@Override
	public PlaneType createPlaneType(PlaneType planeType) {
		long id = getPlaneRepository().createPlaneType(planeType);
		return getPlaneType(id);
	}

	@Override
	public void updatePlaneType(PlaneType planeType) {
		getPlaneRepository().updatePlaneType(planeType);
	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {
		return getPlaneRepository().getAllPlaneTypes();
	}

	public PlaneTypeRepository getPlaneRepository() {
		return planeRepository;
	}

	public void setPlaneRepository(PlaneTypeRepository planeRepository) {
		this.planeRepository = planeRepository;
	}

}
