package webservices.helloworld;

import static org.junit.Assert.assertEquals;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Endpoint;
import javax.xml.ws.Service;

import org.junit.After;
import org.junit.Test;

/**
 * This is a very basic test. We create a web service using an Endpoint. Once the web service is up and running we
 * connect to it using JAX-WS classes.
 * 
 */
public class HelloWorldClientTest {

	Endpoint endpoint = null;

	@After
	public void cleanup() {

		if (endpoint != null) {
			endpoint.stop();
		}
	}

	/**
	 * Test a basic web service
	 */
	@Test
	public void test() throws Exception {

		endpoint = startEndpoint();
		HelloWorld helloWorld = getHelloWorld();
		assertEquals("HelloWorld: ", "Hello world", helloWorld.helloWorld());

	}

	/**
	 * Publish the HelloWorld web service as a web service using localhost:8180.
	 */
	public Endpoint startEndpoint() {
		return Endpoint.publish("http://localhost:8180/helloworld", new HelloWorldImpl());
	}

	/**
	 * Using JAX-WS classes, return the port that is used to access the web service.
	 */
	public HelloWorld getHelloWorld() throws Exception {
		URL url = new URL("http://localhost:8180/helloworld?wsdl");
		QName qName = new QName("http://helloworld.webservices/", "HelloWorldImplService");
		Service service = Service.create(url, qName);
		
		return service.getPort(HelloWorld.class);

	}

}
