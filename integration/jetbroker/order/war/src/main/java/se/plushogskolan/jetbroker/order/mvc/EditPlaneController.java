package se.plushogskolan.jetbroker.order.mvc;

import java.util.logging.Logger;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.service.PlaneService;

@Controller
@RequestMapping("/editPlane/{id}.html")
public class EditPlaneController {

	Logger log = Logger.getLogger(EditPlaneController.class.getName());

	@Inject
	private PlaneService planeService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView index(@PathVariable long id) {

		log.fine("editPlane, id=" + id);

		EditPlaneBean bean = new EditPlaneBean();
		boolean isNew = id <= 0;

		if (!isNew) {
			Plane plane = getPlaneService().getPlane(id);
			bean.fillBeanWithPlaneValues(plane);
		}

		ModelAndView mav = new ModelAndView("editPlane");
		mav.addObject("editPlaneBean", bean);
		addReferenceDataToModel(mav, id);
		return mav;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView handleSubmit(@Valid EditPlaneBean bean, BindingResult errors) {

		log.fine("handleSubmit: ID=" + bean.getPlaneId());

		if (errors.hasErrors()) {
			ModelAndView mav = new ModelAndView("editPlane");
			mav.addObject("editPlaneBean", bean);
			addReferenceDataToModel(mav, bean.getPlaneId());
			return mav;
		}

		if (bean.getPlaneId() > 0) {
			Plane plane = getPlaneService().getPlane(bean.getPlaneId());
			bean.fillPlaneWithBeanValues(plane);
			getPlaneService().updatePlane(plane);
		} else {
			Plane plane = new Plane();
			bean.fillPlaneWithBeanValues(plane);
			plane = getPlaneService().createPlane(plane);
		}

		return new ModelAndView("redirect:/index.html");
	}

	protected void addReferenceDataToModel(ModelAndView mav, long planeId) {
		mav.addObject("isNew", planeId <= 0);
		mav.addObject("planeTypes", getPlaneService().getAllPlaneTypes());
	}

	public PlaneService getPlaneService() {
		return planeService;
	}

	public void setPlaneService(PlaneService planeService) {
		this.planeService = planeService;
	}
}
