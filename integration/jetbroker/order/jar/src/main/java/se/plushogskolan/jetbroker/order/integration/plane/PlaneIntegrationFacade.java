package se.plushogskolan.jetbroker.order.integration.plane;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.PlaneType;

@Local
public interface PlaneIntegrationFacade {

	List<Airport> getAllAirports();

	List<PlaneType> getAllPlaneTypes();

}
