package se.plushogskolan.jetbroker.order.repository;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.order.domain.Airport;

@Local
public interface AirportRepository {
	Airport getAirport(String code);
	List<Airport> getAllAirports();
}
