package se.plushogskolan.jetbroker.order.service;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.order.domain.Plane;

@Local
public interface PlaneService {
	Plane getPlane(long id);
	Plane createPlane(Plane plane);
	void updatePlane(Plane plane);
	void removePlane(Plane plane);
	List<Plane> getAllPlanes();
}
