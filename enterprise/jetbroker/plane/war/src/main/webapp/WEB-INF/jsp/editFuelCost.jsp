<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
<head>
<title><spring:message code="editFuelCost.title"/></title>
<link href="<%=request.getContextPath()%>/style/common.css" type="text/css" rel="stylesheet" />
</head>
<body>
	<jsp:include page="header.jsp" />
	<h2 class="underline"><img src="<%=request.getContextPath()%>/images/gas.png">
		<spring:message code="editFuelCost.title"/>
	</h2>

	<form:form commandName="editFuelCostBean">
		<table class="formTable">
			<tr>
				<th><spring:message code="global.fuelPrice"/></th>
				<td><form:input path="fuelCost"/></td>
				<td><form:errors path="fuelCost" cssClass="errors" /></td>
			<tr>
				<th></th>
				<td>
					<c:set var="submitText">
						<spring:message code="global.submit" />
					</c:set>
				<input type="submit" value="${submitText}" /> <a href="<%=request.getContextPath()%>/index.html"><spring:message code="global.cancel"/></a></td>
				<td></td>
			</tr>
		</table>
	</form:form>
</body>
</html>