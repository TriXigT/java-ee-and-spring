package webservices.wsgen;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface GreetingWebService {
	
	@WebMethod
	public String greetSomeone(String name);

}
