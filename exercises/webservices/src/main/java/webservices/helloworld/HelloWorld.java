package webservices.helloworld;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * Add annotations to make this a web service
 */
@WebService
public interface HelloWorld {

	@WebMethod
	public String helloWorld();

}
