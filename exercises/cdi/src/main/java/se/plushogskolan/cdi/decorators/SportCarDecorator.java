package se.plushogskolan.cdi.decorators;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.enterprise.inject.Any;
import javax.inject.Inject;

import se.plushogskolan.cdi.annotations.Income;
import se.plushogskolan.cdi.annotations.Income.IncomeType;
import se.plushogskolan.cdi.model.Car;
import se.plushogskolan.cdi.services.CarService;

/**
 * A decorator that intercepts method calls to a CarService mapped with income =
 * HIGH. Will change the name when the getCar() method is called.
 * 
 */
@Decorator
public abstract class SportCarDecorator implements CarService {

	@Inject @Delegate @Income(IncomeType.HIGH)
	CarService carService;

	@Income(IncomeType.HIGH)
	public Car getCar() {
		System.out.println("SportCarDecorator.getCar() is invoked");
		Car car = carService.getCar();
		car.setName(car.getName() + " with some decorated wrom wrom");
		return car;
	}

}
