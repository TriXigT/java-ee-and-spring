package se.plushogskolan.jetbroker.agent.domain;

public class PlaneType {

	private String code;
	private String name;
	private int maxNoOfPassengers;
	private int maxRangeKm;
	private int maxSpeedKmH;
	private double fuelConsumptionPerKm;

	public PlaneType() { }
	
	public PlaneType(String code, String name, int maxNoOfPassengers,
			int maxRangeKm, int maxSpeedKmH, double fuelConsumptionPerKm) {
		super();
		this.code = code;
		this.name = name;
		this.maxNoOfPassengers = maxNoOfPassengers;
		this.maxRangeKm = maxRangeKm;
		this.maxSpeedKmH = maxSpeedKmH;
		this.fuelConsumptionPerKm = fuelConsumptionPerKm;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMaxNoOfPassengers() {
		return maxNoOfPassengers;
	}

	public void setMaxNoOfPassengers(int maxNoOfPassengers) {
		this.maxNoOfPassengers = maxNoOfPassengers;
	}

	public int getMaxRangeKm() {
		return maxRangeKm;
	}

	public void setMaxRangeKm(int maxRangeKm) {
		this.maxRangeKm = maxRangeKm;
	}

	public int getMaxSpeedKmH() {
		return maxSpeedKmH;
	}

	public void setMaxSpeedKmH(int maxSpeedKmH) {
		this.maxSpeedKmH = maxSpeedKmH;
	}

	public double getFuelConsumptionPerKm() {
		return fuelConsumptionPerKm;
	}

	public void setFuelConsumptionPerKm(double fuelConsumptionPerKm) {
		this.fuelConsumptionPerKm = fuelConsumptionPerKm;
	}

	@Override
	public String toString() {
		return "PlaneType [code=" + code + ", name=" + name + "]";
	}

}
