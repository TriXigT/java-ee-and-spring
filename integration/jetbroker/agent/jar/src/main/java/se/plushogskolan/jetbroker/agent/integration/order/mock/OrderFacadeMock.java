package se.plushogskolan.jetbroker.agent.integration.order.mock;

import java.util.logging.Logger;

import se.plushogskolan.jee.utils.cdi.Mock;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.integration.order.OrderFacade;

@Mock
public class OrderFacadeMock implements OrderFacade {

	private static Logger log = Logger.getLogger(OrderFacadeMock.class.getName());

	public void sendFlightRequest(FlightRequest request) throws Exception {

		log.info("sendFlightRequest in OrderFacade MOCK: " + request);
	}

}
