package se.plushogskolan.jetbroker.agent.services;

import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.event.Event;
import javax.enterprise.inject.Any;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.agent.annotations.FlightRequestChanged;
import se.plushogskolan.jetbroker.agent.annotations.FlightRequestChanged.ChangeType;
import se.plushogskolan.jetbroker.agent.domain.request.FlightOffer;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestStatus;
import se.plushogskolan.jetbroker.agent.events.FlightRequestChangedEvent;
import se.plushogskolan.jetbroker.agent.integration.order.OrderFacade;
import se.plushogskolan.jetbroker.agent.repository.FlightRequestRepository;

@Stateless
public class FlightRequestServiceImp implements FlightRequestService {

	@Inject
	private Logger log;
	
	@Inject
	private FlightRequestRepository repository;

	@Inject
	@Prod
	private OrderFacade orderFacade;

	@Resource
	private SessionContext context;

	@Inject
	@Any
	@FlightRequestChanged(ChangeType.CONFIRMED)
	@Prod
	Event<FlightRequestChangedEvent> flightRequestConfirmationEvent;

	@Inject
	@Any
	@FlightRequestChanged(ChangeType.OFFER_RECEIVED)
	@Prod
	Event<FlightRequestChangedEvent> flightRequestOfferRecievedEvent;

	@Inject
	@Any
	@FlightRequestChanged(ChangeType.REJECTED)
	@Prod
	Event<FlightRequestChangedEvent> flightRequestRejectedEvent;

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public FlightRequest getFlightRequest(long id) {
		log.fine(String.format("get flight request with id [%d]", id));
		
		return getRepository().findById(id);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public List<FlightRequest> getAllFlightRequests() {
		log.fine("get all flightrequests");
		
		return getRepository().getAllFlightRequests();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED) 
	public FlightRequest createFlightRequest(FlightRequest request) {
		log.fine(String.format("create flightrequest: %s", request));
		
		request.setRequestStatus(FlightRequestStatus.CREATED);
		long id = repository.persist(request);
		
		try {
			orderFacade.sendFlightRequest(request);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return repository.findById(id);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void handleFlightRequestConfirmation(FlightRequestConfirmation response) {
		log.fine(String.format("handle confirmation of request: %s", response.toString()));
		
		FlightRequest flightRequest = repository.findById(response.getAgentRequestId());
		
		flightRequest.setConfirmationId(response.getOrderRequestId());
		flightRequest.setRequestStatus(FlightRequestStatus.REQUEST_CONFIRMED);
		
		repository.update(flightRequest);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void handleFlightRequestRejection(long flightRequestId) {

		// Implement

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void handleFlightOffer(FlightOffer offer, long flightRequestId) {

		// Implement
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateFlightRequest(FlightRequest request) {

		log.fine("updateFlightRequest: " + request);

		getRepository().update(request);

		if (request.getNoOfPassengers() == 13) {
			log.info("Rollbacking transaction since passengers = 13");
			context.setRollbackOnly();
		}
	}

	@Override
	public List<FlightRequest> getFlightRequestsForCustomer(long id) {
		return getRepository().getFlightRequestsForCustomer(id);
	}

	@Override
	public void deleteFlightRequest(long id) {
		log.fine("deleteFlightRequest, id=" + id);
		FlightRequest flightRequest = getFlightRequest(id);
		getRepository().remove(flightRequest);

	}

	public FlightRequestRepository getRepository() {
		return repository;
	}

	public void setRepository(FlightRequestRepository repository) {
		this.repository = repository;
	}

	public OrderFacade getOrderFacade() {
		return orderFacade;
	}

	public void setOrderFacade(OrderFacade orderFacade) {
		this.orderFacade = orderFacade;
	}

	protected void setLog(Logger log) {
		this.log = log;
	}

}
