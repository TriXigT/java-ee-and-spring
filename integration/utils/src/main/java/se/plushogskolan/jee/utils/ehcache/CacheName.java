package se.plushogskolan.jee.utils.ehcache;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.enterprise.util.Nonbinding;
import javax.inject.Qualifier;

/**
 * Qualifier used with CacheFactory to specifiy which net.sf.ehcache.Cache instance to get from net.sf.ehcache.CacheManager
 * @author coolio
 *
 */
@Qualifier 
@Retention(RetentionPolicy.RUNTIME) 
@Target({METHOD, FIELD, PARAMETER, TYPE})  
public @interface CacheName {  
	@Nonbinding String cacheName() default "unknown";  
} 
