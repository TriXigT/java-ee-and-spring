package se.plushogskolan.jetbroker.order.integration.facade;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.Offer;

@Local
public interface IntegrationFacade {
	void sendFlightRequest(FlightRequest flightRequest);
	void sendOffer(Offer offer);
}
