package se.plushogskolan.jetbroker.order.repository.jpa;

import static org.junit.Assert.assertTrue;

import javax.inject.Inject;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.order.domain.Offer;
import se.plushogskolan.jetbroker.order.repository.AbstractRepositoryTest;
import se.plushogskolan.jetbroker.order.repository.OfferRepository;

@RunWith(Arquillian.class)
@Transactional(TransactionMode.ROLLBACK)
public class JpaOfferRepositoryIntegrationTest extends AbstractRepositoryTest<Offer, OfferRepository>{

	@Inject
	OfferRepository repo;

	@Test
	public void testGetAllOffers() {
		repo.persist(getEntity1());
		repo.persist(getEntity2());
		
		assertTrue("No. of offers", repo.getAllOffers().size() == 2);
	}
	
	@Test(expected = Exception.class)
	public void testValidationIsActive() {
		Offer offer = getEntity1();
		offer.setPrice(-5000);
		repo.persist(offer);
	}

	@Override
	protected OfferRepository getRepository() {
		return repo;
	}

	@Override
	protected Offer getEntity1() {
		return TestFixture.getValidOffer(4444);
	}

	@Override
	protected Offer getEntity2() {
		return TestFixture.getValidOffer(22);
	}

}