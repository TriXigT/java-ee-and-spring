package se.plushogskolan.jetbroker.order.domain;

public enum Status {
    NEW("New request"), OFFER_SENT("Offer sent"), REJECTED("Request rejected");
    
    String niceName;
    
    Status(String niceName) {
    	this.niceName = niceName;
    }
    
    public String getNiceName() {
		return niceName;
	}
}
