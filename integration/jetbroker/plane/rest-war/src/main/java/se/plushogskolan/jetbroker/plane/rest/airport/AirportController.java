package se.plushogskolan.jetbroker.plane.rest.airport;

import java.util.logging.Logger;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import se.plushogskolan.jetbroker.plane.domain.Airport;
import se.plushogskolan.jetbroker.plane.rest.OkOrErrorResponse;
import se.plushogskolan.jetbroker.plane.rest.OkOrErrorResponse.OkOrErrorStatus;
import se.plushogskolan.jetbroker.plane.rest.airport.model.CreateAirportRequest;
import se.plushogskolan.jetbroker.plane.rest.airport.model.CreateAirportResponse;
import se.plushogskolan.jetbroker.plane.rest.airport.model.GetAirportResponse;
import se.plushogskolan.jetbroker.plane.service.AirportService;
import se.plushogskolan.jetbroker.plane.service.FuelPriceService;

@Controller
public class AirportController {

	Logger log = Logger.getLogger(AirportController.class.getName());
	
	@Inject
	AirportService airportService;
	
	@Inject
	FuelPriceService fuelPriceService;

	@RequestMapping(value = "/createAirport", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public CreateAirportResponse createAirport(@RequestBody CreateAirportRequest request) throws Exception {
		log.fine("createPlaneType: " + request);
		Airport airport = airportService.createAirport(request.buildAirport());
		return new CreateAirportResponse(airport.getId());
	}

	@RequestMapping(value = "/getAirport/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public GetAirportResponse getAirport(@PathVariable long id) {
		log.fine(String.format("getAirport id:%d", id));
		return new GetAirportResponse(airportService.getAirport(id));
	}

	@RequestMapping(value = "/deleteAirport/{id}", method = RequestMethod.DELETE, produces = "application/json")
	@ResponseBody
	public OkOrErrorResponse deleteAirport(@PathVariable long id) {
		log.fine(String.format("deleteAirport id:%d", id));
		try {
			airportService.deleteAirport(id);
		} catch(Exception e) {
			return OkOrErrorResponse.getErrorResponse(e.getMessage());
		}
		
		return OkOrErrorResponse.getOkResponse();
	}

	@RequestMapping(value = "/updateFuelPrice/{fuelPrice}", method = RequestMethod.PUT, produces = "application/json")
	@ResponseBody
	public OkOrErrorResponse updateFuelPrice(@PathVariable double fuelPrice) {
		log.fine(String.format("updateFuelPrice from %f to %f", fuelPriceService.getFuelPrice(), fuelPrice));
		try {
			fuelPriceService.updateFuelPrice(fuelPrice);
		} catch(Exception e) {
			return OkOrErrorResponse.getErrorResponse(e.getMessage());
		}
		
		return OkOrErrorResponse.getOkResponse();
	}

}
