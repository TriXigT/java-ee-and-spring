package se.plushogskolan.jetbroker.order.integration.simulator;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.annotation.Mock;
import se.plushogskolan.jetbroker.order.integration.event.NewFlightRequestEvent;

@Stateless
public class SystemSimulatorImpl implements SystemSimulator{
	
	@Inject 
	@Mock
	Event<NewFlightRequestEvent> newFlightRequestEvent;
	
	@Override
	public void receiveFlightRequest(long agentId) {
		newFlightRequestEvent.fire(new NewFlightRequestEvent(agentId));
	}
}
