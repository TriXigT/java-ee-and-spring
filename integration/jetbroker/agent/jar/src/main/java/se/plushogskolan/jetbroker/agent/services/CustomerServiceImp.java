package se.plushogskolan.jetbroker.agent.services;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.repository.CustomerRepository;

@Stateless
public class CustomerServiceImp implements CustomerService {

	@Inject
	private Logger log;

	@Inject
	private CustomerRepository customerRepository;

	@Inject
	private FlightRequestService flightRequestService;

	@Override
	public void updateCustomer(Customer customer) { // Some dummy code to test transactions

		log.fine("updateCustomer: " + customer);
		getCustomerRepository().update(customer);

	}

	@Override
	public Customer createCustomer(Customer customer) {

		log.fine("createCustomer: " + customer);
		long id = getCustomerRepository().persist(customer);
		return getCustomerRepository().findById(id);

	}

	@Override
	public void deleteCustomer(long id) {

		log.fine("deleteCustomer, id=" + id);
		List<FlightRequest> flightRequests = getFlightRequestService().getFlightRequestsForCustomer(id);
		for (FlightRequest flightRequest : flightRequests) {
			getFlightRequestService().deleteFlightRequest(flightRequest.getId());
		}

		getCustomerRepository().remove(getCustomer(id));
	}

	@Override
	public List<Customer> getAllCustomers() {
		return getCustomerRepository().getAllCustomers();
	}

	@Override
	public Customer getCustomer(long id) {
		return getCustomerRepository().findById(id);
	}

	public CustomerRepository getCustomerRepository() {
		return customerRepository;
	}

	public void setCustomerRepository(CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}

	public FlightRequestService getFlightRequestService() {
		return flightRequestService;
	}

	public void setFlightRequestService(FlightRequestService flightRequestService) {
		this.flightRequestService = flightRequestService;
	}
}
