package webservices.helloworld;

import javax.jws.WebService;
import javax.xml.ws.Endpoint;

/**
 * Implement the HelloWorld web service
 */
@WebService(endpointInterface="webservices.helloworld.HelloWorld")
public class HelloWorldImpl implements HelloWorld {
	
	/**
	 * Empty constructor for JAX WS
	 */
	public HelloWorldImpl() { }
	
	public static void main(String[] args) {
		Endpoint.publish("http://localhost:8180/helloworld", HelloWorld.class);
	}

	public String helloWorld() {
		return "Hello world";
	}

}
