<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
<head>
<link href="<%=request.getContextPath()%>/style/common.css"
	type="text/css" rel="stylesheet" />

</head>
<body>
	<jsp:include page="header.jsp" />


	<h2 class="underline">
		<img src="<%=request.getContextPath()%>/images/flightrequest.png">
		Flight requests
	</h2>

	<c:choose>
		<c:when test="${empty flightRequestWrappers }">
			<p>There are no flight requests to handle.
		</c:when>
		<c:otherwise>
			<table class="dataTable">
				<tr>
					<th>ID</th>
					<th>Status</th>
					<th>Departure airport</th>
					<th>Arrival airport</th>
					<th>Date</th>
					<th>No of passengers</th>
					<th>Offered price</th>
					<th>Offered plane</th>
					<th>Edit</th>
				</tr>
				<c:forEach items="${flightRequestWrappers}" var="wrapper">
					<tr>
						<td>${wrapper.flightRequest.id}</td>
						<td>${wrapper.flightRequest.status.niceName}</td>
						<td>${wrapper.departureAirport.name} (${wrapper.departureAirport.code})</td>
						<td>${wrapper.arrivalAirport.name} (${wrapper.arrivalAirport.code})</td>
						<td><fmt:formatDate type="both" timeStyle="short"
								value="${wrapper.flightRequest.departureTimeAsDate}" /></td>
						<td>${wrapper.flightRequest.noOfPassengers}</td>
						<td>
							<c:if test="${not empty wrapper.flightRequest.offer}">
								<fmt:formatNumber value="${wrapper.flightRequest.offer.price}" maxFractionDigits="0" /> SEK</td>
							</c:if>
						<td>
							<c:if test="${not empty wrapper.planeType}">
								${wrapper.planeType.name} (${wrapper.planeType.maxNoOfPassengers} seats)
							</c:if>
						</td>
						<td><a
							href="<%=request.getContextPath()%>/editFlightRequest/${wrapper.flightRequest.id}.html"><img src="images/edit.png" ></a></td>
					</tr>
				</c:forEach>
			</table>
		</c:otherwise>
	</c:choose>



	<h2 class="underline">
		<img src="<%=request.getContextPath()%>/images/plane.png">
		Our planes
	</h2>

	<p>
		<a class="button" href="<%=request.getContextPath()%>/editPlane/0.html">Create
			new plane</a>
	</p>
	<table class="dataTable">
		<tr>
			<th>ID</th>
			<th>Code</th>
			<th>Name</th>
			<th>Max passengers</th>
			<th>Range (km)</th>
			<th>Max speed (km/h)</th>
			<th>Description</th>
			<th>Edit</th>
		</tr>
		<c:forEach items="${planeWrappers}" var="wrapper">
			<tr>
				<td>${wrapper.plane.id}</td>
				<td>${wrapper.planeType.code}</td>
				<td>${wrapper.planeType.name}</td>
				<td>${wrapper.planeType.maxNoOfPassengers}</td>
				<td>${wrapper.planeType.maxRangeKm}</td>
				<td>${wrapper.planeType.maxSpeedKmH}</td>
				<td>${wrapper.plane.description}</td>
				<td><a
					href="<%=request.getContextPath()%>/editPlane/${wrapper.plane.id}.html"><img src="images/edit.png"></a></td>
			</tr>
		</c:forEach>
	</table>

<h2 class="underline">
		<img src="<%=request.getContextPath()%>/images/gas.png">
		Fuel price
	</h2>
	
	<p>Current fuel cost is <fmt:formatNumber value="${fuelCost }" maxFractionDigits="2" minFractionDigits="2"/> SEK. </p>


</body>

</html>