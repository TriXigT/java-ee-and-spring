package se.plushogskolan.jetbroker.order.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.order.domain.Offer;
import se.plushogskolan.jetbroker.order.integration.facade.IntegrationFacade;
import se.plushogskolan.jetbroker.order.repository.OfferRepository;
import se.plushogskolan.jetbroker.order.service.OfferService;
@Stateless
public class OfferServiceImpl implements OfferService{

	@Inject
	OfferRepository repo;
	@Inject
	IntegrationFacade facade;
	
	@Override
	public Offer getOffer(long id) {
		return repo.findById(id);
	}

	@Override
	public Offer createOffer(Offer offer) {
		facade.sendOffer(repo.findById(repo.persist(offer)));
		return offer;
	}

	@Override
	public void updateOffer(Offer offer) {
		repo.update(offer);
		
	}
	
	@Override
	public void removeOffer(Offer offer) {
		repo.remove(offer);
	}

	@Override
	public List<Offer> getAllOffers() {
		return repo.getAllOffers();
	}

	protected OfferRepository getRepository() {
		return repo;
	}

	protected void setRepository(OfferRepository repo) {
		this.repo = repo;
	}

	protected IntegrationFacade getFacade() {
		return facade;
	}

	protected void setFacade(IntegrationFacade facade) {
		this.facade = facade;
	}
	
	
	
}
