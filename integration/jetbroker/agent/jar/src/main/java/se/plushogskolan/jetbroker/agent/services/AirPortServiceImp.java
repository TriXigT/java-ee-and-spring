package se.plushogskolan.jetbroker.agent.services;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.agent.domain.AirPort;
import se.plushogskolan.jetbroker.agent.events.AirportsChangedEvent;
import se.plushogskolan.jetbroker.agent.repository.AirPortRepository;

@Stateless
public class AirPortServiceImp implements AirPortService {

	@Inject
	private Logger log;
	
	@Inject
	private AirPortRepository airPortRepository;

	@Override
	public List<AirPort> getAllAirPorts() {
		log.fine("get all airports");
		
		return airPortRepository.getAllAirPorts();
	}

	@Override
	public AirPort getAirPort(String code) {
		log.fine(String.format("get airport by code [%s]", code));
		
		return airPortRepository.getAirPort(code);
	}

	@Override
	public void handleAirportsChangedEvent(@Observes AirportsChangedEvent event) {
		log.fine("handle received AirportChangedEvent");
		
		airPortRepository.handleAirportsChangedEvent();
	}
}
