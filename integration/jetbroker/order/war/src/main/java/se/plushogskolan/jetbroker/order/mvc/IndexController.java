package se.plushogskolan.jetbroker.order.mvc;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.service.FlightRequestService;
import se.plushogskolan.jetbroker.order.service.FuelPriceService;
import se.plushogskolan.jetbroker.order.service.PlaneService;

@Controller
public class IndexController {

	@Inject
	private FlightRequestService flightRequestService;

	@Inject
	private PlaneService planeService;
	
	@Inject
	private FuelPriceService fuelPriceService;

	@RequestMapping("/index.html")
	public ModelAndView index() {

		// Get all flightRequests and wrap them in a FlightRequestWrapper
		List<FlightRequest> flightRequests = getFlightRequestService().getAllFlightRequests();
		List<FlightRequestWrapper> flightRequestWrappers = createFlightRequestWrappers(flightRequests);

		// Get all planes and wrap them in a PlaneWrapper class
		List<Plane> planes = getPlaneService().getAllPlanes();
		List<PlaneWrapper> wrappers = createPlaneWrappers(planes);

		ModelAndView mav = new ModelAndView("index");
		mav.addObject("flightRequestWrappers", flightRequestWrappers);
		mav.addObject("planeWrappers", wrappers);
		mav.addObject("fuelCost", fuelPriceService.getFuelPrice());
		return mav;
	}

	private List<PlaneWrapper> createPlaneWrappers(List<Plane> planes) {
		ArrayList<PlaneWrapper> wrappers = new ArrayList<PlaneWrapper>();
		for (Plane plane : planes) {
			PlaneType planeType = getPlaneService().getPlaneType(plane.getPlaneTypeCode());
			PlaneWrapper wrapper = new PlaneWrapper(plane, planeType);
			wrappers.add(wrapper);
		}
		return wrappers;
	}

	private List<FlightRequestWrapper> createFlightRequestWrappers(List<FlightRequest> flightRequests) {
		List<FlightRequestWrapper> wrappers = new ArrayList<FlightRequestWrapper>();
		for (FlightRequest flightRequest : flightRequests) {
			Airport arrivalAirport = getPlaneService().getAirport(flightRequest.getArrivalAirportCode());
			Airport departureAirport = getPlaneService().getAirport(flightRequest.getDepartureAirportCode());
			PlaneType planeType = null;
			if (flightRequest.getOffer() != null) {
				if (flightRequest.getOffer().getPlane() != null) {
					planeType = getPlaneService().getPlaneType(flightRequest.getOffer().getPlane().getPlaneTypeCode());
				}
			}
			wrappers.add(new FlightRequestWrapper(flightRequest, arrivalAirport, departureAirport, planeType));

		}
		return wrappers;
	}

	public FlightRequestService getFlightRequestService() {
		return flightRequestService;
	}

	public void setFlightRequestService(FlightRequestService service) {
		this.flightRequestService = service;
	}

	public PlaneService getPlaneService() {
		return planeService;
	}

	public void setPlaneService(PlaneService planeService) {
		this.planeService = planeService;
	}

	public static class FlightRequestWrapper {

		private FlightRequest flightRequest;
		private Airport arrivalAirport;
		private Airport departureAirport;
		private PlaneType planeType;

		public FlightRequestWrapper(FlightRequest req, Airport arrivalAirport, Airport departureAirport,
				PlaneType planeType) {
			setFlightRequest(req);
			setArrivalAirport(arrivalAirport);
			setDepartureAirport(departureAirport);
			setPlaneType(planeType);
		}

		public FlightRequest getFlightRequest() {
			return flightRequest;
		}

		public void setFlightRequest(FlightRequest flightRequest) {
			this.flightRequest = flightRequest;
		}

		public Airport getArrivalAirport() {
			return arrivalAirport;
		}

		public void setArrivalAirport(Airport arrivalAirport) {
			this.arrivalAirport = arrivalAirport;
		}

		public Airport getDepartureAirport() {
			return departureAirport;
		}

		public void setDepartureAirport(Airport departureAirport) {
			this.departureAirport = departureAirport;
		}

		public PlaneType getPlaneType() {
			return planeType;
		}

		public void setPlaneType(PlaneType planeType) {
			this.planeType = planeType;
		}

	}

	public static class PlaneWrapper {

		private Plane plane;
		private PlaneType planeType;

		public PlaneWrapper(Plane plane, PlaneType planeType) {

			setPlane(plane);
			setPlaneType(planeType);
		}

		public Plane getPlane() {
			return plane;
		}

		public void setPlane(Plane plane) {
			this.plane = plane;
		}

		public PlaneType getPlaneType() {
			return planeType;
		}

		public void setPlaneType(PlaneType planeType) {
			this.planeType = planeType;
		}

	}

}
