package se.plushogskolan.jetbroker.agent.repository;

import java.util.List;

import se.plushogskolan.jee.utils.repository.BaseRepository;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.Status;

public interface FlightRequestRepository  extends BaseRepository<FlightRequest>{

	/**
	 * Retrieve all FlightRequests from the datasource
	 * @return List of FlightRequests
	 */
	List<FlightRequest> getAllFlightRequests();
	
	/**
	 * Get FlightRequests having a status matching supplied Status
	 * @return List of FlightRequests mathing supplied status
	 */
	List<FlightRequest> getFlightRequestsByStatus(Status status);
}
