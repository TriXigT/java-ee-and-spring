package se.plushogskolan.cdi.interceptors;

import java.util.HashMap;
import java.util.Map;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import se.plushogskolan.cdi.annotations.InvocationCounter;

/**
 * An interceptor that counts each method that is invoked. This interceptor is
 * called every time a method is invoked that is annotated with
 * @InvocationCounter.
 * 
 */
@Interceptor @InvocationCounter
public class InvocationCounterInterceptor {

	private static Map<String, Integer> counts = new HashMap<String, Integer>();

	public static void reset() {
		counts.clear();
	}

	public static int getInvocationCountForMethod(String method) {
		Integer count = counts.get(method);
		return count == null ? 0 : count;
	}

	/**
	 * This method is invoked every time we call a method that this interceptor
	 * is configured for. We can perform our own logic, and by using
	 * ctx.process() we call the original method.
	 */
	@AroundInvoke
	public Object countMethodInvocations(InvocationContext ctx)
			throws Exception {
		String key = getKey(ctx);
		Integer count = increaseCount(key);
		System.out.println("Method invocations: " + count + " for " + key);
		return ctx.proceed();
	}

	private Integer increaseCount(String key) {
		if (!counts.containsKey(key)) {
			counts.put(key, 0);
		}

		Integer count = counts.get(key);
		count = count + 1;
		counts.put(key, count);

		return count;
	}

	private String getKey(InvocationContext ctx) {
		return ctx.getMethod().getName();
	}

}
