package se.plushogskolan.jee.utils.domain;

/**
 * An interface for all entities that can return a long ID. This is supposed to
 * be done by every JPA entity.
 * 
 * By having this simple interface we can reuse functionality for all JPA
 * entities. If a method returns an object of type IdHolder, we can cast that to
 * whatever object we expect.
 * 
 */
public interface IdHolder {

	long getId();
	void setId(long id);
}
