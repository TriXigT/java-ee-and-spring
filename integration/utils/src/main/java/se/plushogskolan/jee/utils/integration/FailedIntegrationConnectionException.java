package se.plushogskolan.jee.utils.integration;

import javax.ejb.ApplicationException;

/**
 * An exception that is thrown when we cannot establish a connection to an
 * integration point.
 * 
 */
@ApplicationException(rollback = true)
public class FailedIntegrationConnectionException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public FailedIntegrationConnectionException() {
	}

	public FailedIntegrationConnectionException(String msg) {
		super(msg);
	}

	public FailedIntegrationConnectionException(String msg, Throwable t) {
		super(msg, t);
	}

}
