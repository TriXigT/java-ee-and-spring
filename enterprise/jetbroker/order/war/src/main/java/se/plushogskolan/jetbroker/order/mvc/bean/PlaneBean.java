package se.plushogskolan.jetbroker.order.mvc.bean;

import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.domain.PlaneType;

public class PlaneBean {
	private PlaneType planeType;
	private Plane plane;
	
	private int fuelCost;
	private int travelTime;
	
	public PlaneBean(Plane plane, PlaneType planeType) {
		this(plane, planeType, 0, 0);
	}
	
	public PlaneBean(Plane plane, PlaneType planeType, int fuelCost,
			int travelTime) {
		this.plane = plane;
		this.planeType = planeType;
		this.fuelCost = fuelCost;
		this.travelTime = travelTime;
	}
	
	public Plane getPlane() {
		return plane;
	}
	public void setPlane(Plane plane) {
		this.plane = plane;
	}
	public PlaneType getPlaneType() {
		return planeType;
	}
	public void setPlaneType(PlaneType planeType) {
		this.planeType = planeType;
	}
	public int getFuelCost() {
		return fuelCost;
	}
	public void setFuelCost(int fuelCost) {
		this.fuelCost = fuelCost;
	}
	public int getTravelTime() {
		return travelTime;
	}
	public void setTravelTime(int travelTime) {
		this.travelTime = travelTime;
	}

}
