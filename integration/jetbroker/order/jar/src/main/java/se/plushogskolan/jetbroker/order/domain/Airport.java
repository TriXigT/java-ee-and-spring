package se.plushogskolan.jetbroker.order.domain;

public class Airport {

	private String code;
	private String name;
	private double latitude;
	private double longitude;

	public Airport() {

	}

	public Airport(String code, String name) {
		this(code, name, 0, 0);
	}

	public Airport(String code, String name, double lat, double lng) {
		setCode(code);
		setName(name);
		setLatitude(lat);
		setLongitude(lng);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

}
