package se.plushogskolan.jee.exercises.localization;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.Locale;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

/**
 * This class is for testing some formatting that can be done using Locales,
 * ResourceBundles etc. Create the class LocaleExercise and implement the needed
 * methods.
 * 
 * Create two property files in src/main/resources. The default property file
 * should be called localizationexercise.properties. The second one has the same
 * name but is for the Swedish language.
 * 
 * LocaleExercise should take all the text from the properties files. Formatting
 * is done by the classes in java.text. No hardcoded text or custom formatting
 * logic is necessary in LocaleExercise.
 * 
 * "If it is hard you haven´t found the right solution".
 * 
 */
public class LocaleExerciseTest {

	private LocaleExercise exercise;
	private Locale swedishLocale;

	@Before
	public void setup() {

		 exercise = new LocaleExercise();
		 swedishLocale = new Locale("sv", "se");

		/*
		 * Different developers have different default Locale. If default Locale
		 * is Swedish, the ResourceBundle behaves like this:
		 * 
		 * 1. Ok, there is no English file. Search for an alternative.
		 * 
		 * 2. I only find the default language file. But wait, wouldn´t it be
		 * better to use the Locale from the System instead. That must be
		 * better...
		 * 
		 * 3. The Swedish file is therefore selected, not the default one.
		 * 
		 * So we hardcode the default to use English so that the behavior is
		 * predictable
		 */
		Locale.setDefault(Locale.ENGLISH);

	}

	/**
	 * sayHello simply returns hello in different languages. Use ResourceBundle
	 * to get the text from the language files
	 */
	@Test
	public void testSayHello() {
		 assertEquals("sayHello en", "Hello",
		 exercise.sayHello(Locale.ENGLISH));
		 assertEquals("sayHello swe", "Hej",
		 exercise.sayHello(swedishLocale));
	}

	/**
	 * Insert the name into the returned String using java.text.MessageFormat
	 */
	@Test
	public void testSayName() {
		 assertEquals("sayName en", "Hello Charles",
		 exercise.sayName("Charles", Locale.ENGLISH));
		 assertEquals("sayName swe", "Hej Kalle",
		 exercise.sayName("Kalle", swedishLocale));
	}

	/**
	 * Only provide the text in the default language file. Swedish should
	 * default back to English.
	 */
	@Test
	public void testFallback() {
		 String expectedReply = "This can only be said in English";
		 assertEquals("saySomethingEnglish en", expectedReply,
		 exercise.saySomethingEnglish(Locale.ENGLISH));
		 assertEquals("saySomethingEnglish swe", expectedReply,
		 exercise.saySomethingEnglish(swedishLocale));
	}

	/**
	 * A little longer introduction, also using java.text.MessageFormat.
	 */
	@Test
	public void testIntroduction() {
		 assertEquals("saySomethingEnglish en",
		 "My name is Charles, I am 23 years old and live in London.",
		 exercise.introduceYourself(23, "London", "Charles",
		 Locale.ENGLISH));
		 assertEquals("saySomethingEnglish swe",
		 "Jag heter Kalle, jag är 34 år gammal och bor i Stockholm.",
		 exercise.introduceYourself(34, "Stockholm", "Kalle",
		 swedishLocale));
	}

	/**
	 * This test is a bit tricky. Use MessageFormat, but you have to instantiate
	 * it using a Locale. Look at the MessageFormat API to see how to use
	 * formats, subformats etc in text strings. Then try using different formats
	 * until you get the expected result.
	 */
	@Test
	public void testSayApointmentDate() {
		 Date date = new DateTime(2013, 5, 12, 23, 23, 0, 0).toDate();
		 assertEquals("sayApointmentDate en",
		 "Lets meet on May 12, 2013 at 11:23 PM.",
		 exercise.sayApointmentDate(date, Locale.ENGLISH));
		 assertEquals("sayApointmentDate sv",
		 "Vi träffas den 12 maj 2013 klockan 23:23.",
		 exercise.sayApointmentDate(date, swedishLocale));
	}

	/**
	 * Print currency. Look in NumberFormat (currency instance)
	 */
	@Test
	public void testSayPrice() {
		 double price = 11.24;
		 assertEquals("sayPrice us", "$11.24",
		 exercise.sayPrice(price, Locale.US));
		 assertEquals("sayPrice sv", "11,24 kr",
		 exercise.sayPrice(price, swedishLocale));
	}

	/**
	 * Limit the number of fraction digits. Use NumberFormat (number instance)
	 */
	@Test
	public void printFractionDigits() {
		 double no = 1.123456789;
		 assertEquals("sayFractionDigits", "1.123",
		 exercise.sayFractionDigits(no));
	}

}
