package se.plushogskolan.jetbroker.agent.integration.plane.mdb;


import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.MessageListener;

import net.sf.ehcache.Cache;
import se.plushogskolan.jee.utils.ehcache.CacheName;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jetbroker.agent.events.AirportsChangedEvent;

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = JmsConstants.DESTINATION_TYPE_TOPIC),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = JmsConstants.TOPIC_PLANE_BROADCAST),
		@ActivationConfigProperty(propertyName = "messageSelector",
		propertyValue = "messageType = '" + JmsConstants.MSGTYPE_PLANEBROADCAST_AIRPORTSCHANGED + "'")})
public class AirportMDB implements MessageListener{

	@Inject
	private Logger log;

	@Inject
	@CacheName(cacheName = "airportCache")
	private Cache cache;

	@Inject
	private Event<AirportsChangedEvent> event;
	
	@Override
	public void onMessage(Message message) {
		log.fine("Received messeage");

		event.fire(new AirportsChangedEvent());
	}

}
