package se.plushogskolan.jetbroker.order.domain;

public class FlightRequestConfirmation {

	long agentRequestId;
	long orderRequestId;

	public FlightRequestConfirmation(long agentRequestId, long orderRequestId) {
		setAgentRequestId(agentRequestId);
		setOrderRequestId(orderRequestId);
	}

	public long getAgentRequestId() {
		return agentRequestId;
	}

	public void setAgentRequestId(long agentRequestId) {
		this.agentRequestId = agentRequestId;
	}

	public long getOrderRequestId() {
		return orderRequestId;
	}

	public void setOrderRequestId(long confirmationId) {
		this.orderRequestId = confirmationId;
	}

}
