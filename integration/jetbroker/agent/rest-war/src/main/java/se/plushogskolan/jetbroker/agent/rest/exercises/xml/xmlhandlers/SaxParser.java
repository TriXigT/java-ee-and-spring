package se.plushogskolan.jetbroker.agent.rest.exercises.xml.xmlhandlers;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestStatus;

public class SaxParser {

	public static FlightRequest readFlightRequestFromXmlUsingSax(String file) throws Exception {
		SAXParserFactory factory = SAXParserFactory.newInstance();

		SAXParser parser = factory.newSAXParser();
		Handler handler = new Handler();
		parser.parse(SaxParser.class.getResourceAsStream(file), handler);
		
		return handler.getFlightRequest();
	}

	private static class Handler extends DefaultHandler {
		
		private FlightRequest flightRequest;
		private String currentTextValue;
		
		public FlightRequest getFlightRequest() {
			return flightRequest;
		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException { 
			if(qName.equals("flightrequest")) {
				flightRequest = new FlightRequest();
				flightRequest.setId(Long.parseLong(attributes.getValue("id")));
			}
		}

		@Override
		public void endElement(String uri, String localName, String qName)
				throws SAXException {
			
			if(qName.equals("arrivalAirportCode")) {
				flightRequest.setArrivalAirportCode(currentTextValue);
			} else if(qName.equals("departureAirportCode")) {
				flightRequest.setDepartureAirportCode(currentTextValue);
			} else if(qName.equals("noOfPassengers")) {
				flightRequest.setNoOfPassengers(Integer.parseInt(currentTextValue));
			} else if(qName.equals("status")) {
				flightRequest.setRequestStatus(Enum.valueOf(FlightRequestStatus.class, currentTextValue));
			}
		}

		@Override
		public void characters(char[] ch, int start, int length)
				throws SAXException {
			currentTextValue = new String(ch, start, length);
		}
	}
}
