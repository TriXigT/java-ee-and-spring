package se.plushogskolan.jetbroker.order.integration.plane.mdb;

import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.BytesMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

import se.plushogskolan.jee.utils.jms.AbstractMDB;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jetbroker.order.service.FuelPriceService;

@MessageDriven(activationConfig = { 
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = JmsConstants.DESTINATION_TYPE_TOPIC),
		@ActivationConfigProperty(propertyName="destination", propertyValue=JmsConstants.TOPIC_PLANE_BROADCAST),
		@ActivationConfigProperty(propertyName="messageSelector", propertyValue="messageType = '"+ JmsConstants.MSGTYPE_PLANEBROADCAST_FUELPRICECHANGED + "'")})
public class FuelPriceMDB extends AbstractMDB implements MessageListener{
	Logger log = Logger.getLogger(FuelPriceMDB.class.getName());
	
	@Inject
	FuelPriceService fuelPriceService;
	
	@Override
	public void onMessage(Message message) {
		log.info("received message");
		
		try {
			BytesMessage msg = (BytesMessage) message;
			fuelPriceService.setFuelPrice(msg.readDouble());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}
	
}
