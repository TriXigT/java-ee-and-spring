package se.plushogskolan.jetbroker.order.service.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class FuelPriceServiceImplTest {

	FuelPriceServiceImpl fuelPriceService;
	
	@Before
	public void setup() {
		fuelPriceService = new FuelPriceServiceImpl();
	}
	
	@Test
	public void testGetDefaultFuelPrice() {
		assertEquals("get fuel price", 1, fuelPriceService.getFuelPrice(), 0);
	}
	
	@Test
	public void testSetFuelPrice() {
		fuelPriceService.setFuelPrice(2.4);
		assertEquals("set fuel price", 2.4, fuelPriceService.getFuelPrice(), 0);
	}

}
