package se.plushogskolan.jetbroker.order.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.repository.PlaneRepository;
import se.plushogskolan.jetbroker.order.service.PlaneService;
@Stateless
public class PlaneServiceImpl implements PlaneService{

	@Inject
	PlaneRepository repo;
	
	@Override
	public Plane getPlane(long id) {
		return repo.findById(id);
	}

	@Override
	public Plane createPlane(Plane plane) {
		return repo.findById(repo.persist(plane));
	}

	@Override
	public void updatePlane(Plane plane) {
		repo.update(plane);
		
	}
	
	@Override
	public void removePlane(Plane plane) {
		repo.remove(plane);
	}

	@Override
	public List<Plane> getAllPlanes() {
		return repo.getAllPlanes();
	}

	protected PlaneRepository getRepository() {
		return repo;
	}

	protected void setRepository(PlaneRepository repo) {
		this.repo = repo;
	}
	
	
	
}
