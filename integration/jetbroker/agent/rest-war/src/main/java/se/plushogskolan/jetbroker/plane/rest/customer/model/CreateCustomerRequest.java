package se.plushogskolan.jetbroker.plane.rest.customer.model;

import se.plushogskolan.jetbroker.agent.domain.Customer;

public class CreateCustomerRequest {
	
	private String firstName;
	private String lastName;
	private String email;
	private String company;
	
	public Customer buildCustomer() {
		Customer customer = new Customer();
		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		customer.setCompany(company);
		customer.setEmail(email);
		
		return customer;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

}
