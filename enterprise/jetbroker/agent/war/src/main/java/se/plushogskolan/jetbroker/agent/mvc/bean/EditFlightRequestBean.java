package se.plushogskolan.jetbroker.agent.mvc.bean;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

import se.plushogskolan.jee.utils.validation.AirportCode;
import se.plushogskolan.jee.utils.validation.ArrivalAndDepartureAirport;
import se.plushogskolan.jee.utils.validation.ArrivalAndDepartureAirportHolder;

@ArrivalAndDepartureAirport
public class EditFlightRequestBean implements ArrivalAndDepartureAirportHolder{

	private long id;
	
	@NotBlank
	@AirportCode(message="{validation.airport.code.invalid}")
	private String departureAirportCode;
	@NotBlank
	@AirportCode(message="{validation.airport.code.invalid}")
	private String arrivalAirportCode;	
	@NotBlank
	private String date;
	@Email(message="{validation.customer.missing}")
	private String customerEmail;
	@Range(min=1, max=500)
	private Integer numberOfPassengers;
	@Range(min=0,max=23)
	private Integer hour;
	@Range(min=0,max=59)
	private Integer minute;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		
		this.id = id;
	}
	
	@Override
	public String getDepartureAirportCode() {
		return departureAirportCode;
	}

	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}

	@Override
	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}

	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public Integer getNumberOfPassengers() {
		return numberOfPassengers;
	}

	public void setNumberOfPassengers(Integer numberOfPassengers) {
		this.numberOfPassengers = numberOfPassengers;
	}

	public Integer getHour() {
		return hour;
	}

	public void setHour(Integer hour) {
		this.hour = hour;
	}

	public Integer getMinute() {
		return minute;
	}

	public void setMinute(Integer minute) {
		this.minute = minute;
	}

}
