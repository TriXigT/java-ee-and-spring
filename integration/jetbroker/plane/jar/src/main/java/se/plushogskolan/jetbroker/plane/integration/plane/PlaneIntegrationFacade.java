package se.plushogskolan.jetbroker.plane.integration.plane;

import javax.ejb.Local;

@Local
public interface PlaneIntegrationFacade {

	public void broadcastNewFuelPrice(double fuelPrice);

	public void broadcastAirportsChanged();

	public void broadcastPlaneTypesChanged();

}
