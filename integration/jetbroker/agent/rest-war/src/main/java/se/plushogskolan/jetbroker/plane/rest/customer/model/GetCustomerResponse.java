package se.plushogskolan.jetbroker.plane.rest.customer.model;

import se.plushogskolan.jetbroker.agent.domain.Customer;

public class GetCustomerResponse {
	private long id;
	private String firstName;
	private String lastName;
	private String email;
	private String company;
	
	public GetCustomerResponse(Customer customer) {
		setId(customer.getId());
		setFirstName(customer.getFirstName());
		setLastName(customer.getLastName());
		setEmail(customer.getEmail());
		setCompany(customer.getCompany());
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}
	
	
}
