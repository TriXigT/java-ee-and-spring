package se.plushogskolan.jetbroker.order.integration.agent;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSession;
import javax.jms.Session;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jee.utils.jms.JmsHelper;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.FlightRequestConfirmation;

@Prod
@Stateless
public class AgentIntegrationFacadeImpl implements AgentIntegrationFacade{

	@Resource(mappedName=JmsConstants.JNDI_CONNECTIONFACTORY)
	private QueueConnectionFactory connectionFactory;
	@Resource(mappedName=JmsConstants.QUEUE_FLIGHTREQUEST_RESPONSE)
	private Queue queue;

	private QueueConnection connection;
	private QueueSession session;

	@PostConstruct
	private void init() {
		try {
			connection = connectionFactory.createQueueConnection();
			session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			connection.start();
		} catch (JMSException e) {
			throw new RuntimeException(e);
		}
	}

	@PreDestroy
	private void cleanUp() {
		JmsHelper.closeConnectionAndSession(connection, session);
	}

	@Override
	public void sendUpdatedOfferMessage(FlightRequest flightRequest) {

	}

	@Override
	public void sendFlightRequestRejectedMessage(FlightRequest flightRequest) {

	}

	@Override
	public void sendFlightRequestConfirmation(FlightRequestConfirmation response) {
		try {
			BytesMessage message = session.createBytesMessage();
			message.setLongProperty("agentRequestId", response.getAgentRequestId());
			message.setLongProperty("orderRequestId", response.getOrderRequestId());
			message.setStringProperty("messageType", JmsConstants.MSGTYPE_FLIGHTREQUEST_RESPONSE_CONFIRMATION);

			session.createProducer(queue).send(message);
		} catch (JMSException e) {
			throw new RuntimeException(e);
		}
	}

}
