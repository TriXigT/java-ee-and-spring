package se.plushogskolan.jee.utils.jms;

import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.MessageDrivenContext;
import javax.jms.JMSException;
import javax.jms.MessageListener;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;

public abstract class AbstractMDB implements MessageListener {

	private static Logger log = Logger.getLogger(AbstractMDB.class.getName());

	@Resource(mappedName = JmsConstants.JNDI_CONNECTIONFACTORY)
	private QueueConnectionFactory connectionFactory;

	@Resource
	protected MessageDrivenContext context;

	protected QueueConnection connection = null;

	protected void logErrorAndRollback(Exception e) {
		log.warning("Exception in " + getClass().getCanonicalName() + ": Will perform rollback. Error message: "
				+ e.getMessage());
		e.printStackTrace();
		context.setRollbackOnly();
	}

	@PostConstruct
	public void init() {

		try {
			connection = connectionFactory.createQueueConnection();
			connection.start();
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	@PreDestroy
	public void cleanUp() {

		try {
			connection.close();
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

}
