package se.plushogskolan.jetbroker.plane.service;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.plane.domain.PlaneType;
import se.plushogskolan.jetbroker.plane.repository.PlaneTypeRepository;

@Stateless
public class PlaneServiceImpl implements PlaneService {

	@Inject
	private Logger log;
	
	@Inject
	private PlaneTypeRepository planeRepository;

	@Override
	public PlaneType getPlaneType(long id) {
		log.fine(String.format("get planetype by id [%d]", id));
		
		return getPlaneRepository().findById(id);
	}

	@Override
	public PlaneType createPlaneType(PlaneType planeType) {
		log.fine(String.format("create planetype: %s", planeType.toString()));
		
		long id = getPlaneRepository().persist(planeType);
		return getPlaneType(id);
	}

	@Override
	public void updatePlaneType(PlaneType planeType) {
		log.fine(String.format("update planetype: %s", planeType.toString()));
		
		getPlaneRepository().update(planeType);
	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {
		log.fine("get all planetypes");
		
		return getPlaneRepository().getAllPlaneTypes();
	}

	@Override
	public void deletePlaneType(long id) {
		PlaneType planeType = getPlaneType(id);
		
		log.fine(String.format("delete planetype: %s", planeType.toString()));
		
		getPlaneRepository().remove(planeType);
	}

	public PlaneTypeRepository getPlaneRepository() {
		return planeRepository;
	}

	public void setPlaneRepository(PlaneTypeRepository planeRepository) {
		this.planeRepository = planeRepository;
	}

}
