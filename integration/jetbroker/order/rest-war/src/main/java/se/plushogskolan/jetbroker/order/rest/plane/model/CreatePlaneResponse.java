package se.plushogskolan.jetbroker.order.rest.plane.model;

public class CreatePlaneResponse {
	private long id;

	public CreatePlaneResponse(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
