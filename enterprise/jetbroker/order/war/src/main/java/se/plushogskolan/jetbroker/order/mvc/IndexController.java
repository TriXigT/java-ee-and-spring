package se.plushogskolan.jetbroker.order.mvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.mvc.bean.FlightRequestBean;
import se.plushogskolan.jetbroker.order.mvc.bean.PlaneBean;
import se.plushogskolan.jetbroker.order.service.AirportService;
import se.plushogskolan.jetbroker.order.service.FlightRequestService;
import se.plushogskolan.jetbroker.order.service.FuelPriceService;
import se.plushogskolan.jetbroker.order.service.PlaneService;
import se.plushogskolan.jetbroker.order.service.PlaneTypeService;

@Controller
public class IndexController {
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(IndexController.class.getName());

	@Inject
	private PlaneTypeService planeTypeService;
	@Inject
	private PlaneService planeService;
	@Inject
	private FuelPriceService fuelPriceService;
	@Inject
	private FlightRequestService flightRequestService;
	@Inject
	private AirportService airportService;
	
	@RequestMapping("/index.html")
	public ModelAndView index(Locale locale) {
		ModelAndView mav = new ModelAndView("index");
		
		mav.addObject("fuelPrice", fuelPriceService.getFuelPrice());
		mav.addObject("planeBeans", getAssembledPlaneBeans());
		mav.addObject("flightRequestBeans", getAssembledFlightRequestBeans());
		return mav;
	}
	
	/**
	 * Utility method for assembling PlaneBeans which are wrappers holding a Plane and
	 * a PlaneType used to show a Plane
	 * 
	 * @return a List of IndexPlaneBeans
	 */
	private List<PlaneBean> getAssembledPlaneBeans() {
		List<PlaneBean> indexPlaneBeans = new ArrayList<PlaneBean>();
		
		for(Plane plane : planeService.getAllPlanes()) {
			PlaneBean bean = new PlaneBean(plane, planeTypeService.getPlaneTypeByCode(plane.getPlaneTypeCode()));
			indexPlaneBeans.add(bean);
		}
		
		return indexPlaneBeans;
	}
	
	/**
	 * Utility method for assembling FlightRequestBeans which are wrappers holding a FlightRequest,
	 * a PlaneType, Airports etc used to show a FlightRequest
	 * 
	 * @return a List of FlightRequestBeans
	 */
	private List<FlightRequestBean> getAssembledFlightRequestBeans() {
		List<FlightRequestBean> indexPlaneBeans = new ArrayList<FlightRequestBean>();
		
		for(FlightRequest flightRequest : flightRequestService.getAllFlightRequests()) {
			FlightRequestBean bean = new FlightRequestBean(flightRequest, 
					planeTypeService.getPlaneTypeByCode(flightRequest.getPlaneTypeCode()),
					airportService.getAirportByCode(flightRequest.getDepartureAirportCode()),
					airportService.getAirportByCode(flightRequest.getArrivalAirportCode()));
			indexPlaneBeans.add(bean);
		}
		
		return indexPlaneBeans;
	}

}
